# Void safety

## Summary

Void safety is a static analysis approach that guarantees absence of `NullPointerException`/`NullReferenceException`/`VOID_TARGET`/etc. at run-time. It is briefly explained at [Stackoverflow](http://stackoverflow.com/tags/void-safety/info).

This is a machine-checkable proof of void safety soundness formalized in Isabelle/HOL.

## Installation

1. The theories are checked against [Isabelle 2016](https://isabelle.in.tum.de/installation.html).
2. Source code can be downloaded or checked out depending on your needs
    * Latest stable version: branch `stable`
    * Specific stable version: specific tag, such as `1.0.0`
3. All theories can be built with _Isabelle_ using session `VoidSafety` specified in a file `ROOT` in the root directory of the repository.

## For developers

Source tree follows [Anti-gitflow](http://endoflineblog.com/gitflow-considered-harmful) pattern when all changes go to a branch `master`. If you need a stable version, checkout or download a specific tag, not `HEAD` from `master`. Alternatively you can checkout or download the latest stable version using a branch `stable`. As a consequence of this policy you should not checkout `master` for evaluation or any other use, it is meant only for development and may contain broken code.

## Revision history

1.2.5
	Removed explicit rule for infinite loops.

1.2.4
	Supported infinite loops as a way to indicate unreachable code.

1.2.3
	Added right commutativity lemmas for addition and removal in topset.
	Used simpler definition of transfer function for object tests.
	Simplified proofs by using lattice properties of attachment types.
	Replaced SMT-based proofs by Isar and apply scripts.

1.2.2
	Changed variable names to be more consistent.

1.2.1
	Used Attached and Detachable for attachment types. Simplified lemmas in terms of presentation.

1.2.0
	Used better notation for expression validity.

1.1.1
	Moved definitions of expression validity and state validity to the corresponding theories.

1.1.0
	Used better notation for state validity.

1.0.1
	Added README.

1.0.0
	Supported local variables and object test locals, exceptions (also used to model "design mode"), control structures with positive and negative scopes, loops and actual argument processing.
