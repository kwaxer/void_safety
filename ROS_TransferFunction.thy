section "Transfer function for read-only scopes"

theory ROS_TransferFunction imports         
	ValueAttachment LoopOperator Environment Expression
begin

subsection "Attachment scope"

fun
  scope_true :: "('b, 't) expression \<Rightarrow> vname topset" and
  scope_false :: "('b, 't) expression \<Rightarrow> vname topset"
where
  scope_true_attached_2: "scope_true (attached t (\<V> n') as n) = {n', n}" |
  scope_true_attached_1: "scope_true (attached t e as n) = {n}" |
  scope_true_if: "scope_true (if b then e\<^sub>1 else e\<^sub>2 end) =
    (if is_false e\<^sub>1 then scope_false b \<squnion> scope_true e\<^sub>2 else
    (if is_false e\<^sub>2 then scope_true b \<squnion> scope_true e\<^sub>1 else
    \<lceil>{}\<rceil>))" |
  scope_true_other': "scope_true _ = \<lceil>{}\<rceil>" |

  scope_false_if: "scope_false (if b then e\<^sub>1 else e\<^sub>2 end) =
    (if is_true e\<^sub>1 then scope_false b \<squnion> scope_false e\<^sub>2 else
    (if is_true e\<^sub>2 then scope_true b \<squnion> scope_false e\<^sub>1 else
    \<lceil>{}\<rceil>))" |
  scope_false_other': "scope_false _ = \<lceil>{}\<rceil>"

abbreviation scope_true_rep ("+[_]" 73) where "+[c] \<equiv> scope_true c"
abbreviation scope_false_rep ("-[_]" 73) where "-[c] \<equiv> scope_false c"

(*<*)
lemma scope_true_attached_1': "\<lbrakk>case e of \<V> _ \<Rightarrow> False\<rbrakk> \<Longrightarrow> scope_true (attached t e as n) = {n}"
  by (cases e) simp_all
lemma scope_true_other:
    "\<lbrakk>case c of
      (attached _ _ as _) \<Rightarrow> False |
      (if _ then _ else _ end) \<Rightarrow> False\<rbrakk> \<Longrightarrow>
      scope_true c = (\<emptyset>:: vname set)"
  by (cases c) simp_all
lemma scope_false_other:
  "\<lbrakk>case c of
    (if _ then _ else _ end) \<Rightarrow> False
   \<rbrakk> \<Longrightarrow>scope_false c = (\<emptyset>:: vname set)"
  by (cases c) simp_all
(*>*)

lemma
  scope_true_is_limited: "+[c] \<noteq> \<top>" and
  scope_false_is_limited: "-[c] \<noteq> \<top>"
proof (induction c)
  case (Test t e n)
  then show "+[attached t e as n] \<noteq> \<top>" by (force elim: "scope_true.elims")
  case (If b c1 c2) then show "+[if b then c1 else c2 end] \<noteq> \<top>"
    by (simp add: topset_union_topI)
  case (If b c1 c2) then show "-[if b then c1 else c2 end] \<noteq> \<top>"
    by (simp add: topset_union_topI)
qed simp_all

subsection "Attachment status transfer function"

text {* Function that computes attachment status of variables for an expression
given attachment stratus of variables before it. *}

fun
  \<A> :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>s :: "('b, 't) expression list \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>\<T> :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> bool"
where

-- "Access"

	\<A>_Value: "\<A> (\<C> v) A = A" |
	\<A>_Local: "\<A> (\<V> n) A = A" |

-- "Reattachment"

	\<A>_Assign: "\<A> (n ::= e) A =	(if \<A>\<T> e A then \<A> e A \<oplus> n else \<A> e A \<ominus> n)" |
	\<A>_Create: "\<A> (create n) A = A \<oplus> n" |
	\<A>_Call: "\<A> (e \<^bsub>\<bullet>\<^esub> f (a)) A = \<A>s a (\<A> e A)" |

-- "Compound instructions"

  \<A>_Seq: "\<A> (c\<^sub>1;; c\<^sub>2) A = \<A> c\<^sub>2 (\<A> c\<^sub>1 A)" |
	\<A>_If: "\<A> (if b then c\<^sub>1 else c\<^sub>2 end) A =
	  \<A> c\<^sub>1 (\<A> b A \<squnion> +[b]) \<sqinter> \<A> c\<^sub>2 (\<A> b A \<squnion> -[b])" |
	\<A>_Loop: "\<A> (until e loop c end) A = \<A> e (loop_operator (\<lambda> B. \<A> c (\<A> e B)) A)" |
	\<A>_Exception: "\<A> Exception A = \<top>" |

-- "Boolean expressions"

	\<A>_Test: "\<A> (attached t e as n) A = \<A> e A" |

-- "List of expressions"

  \<A>s_Nil: "\<A>s [] A = A" |
  \<A>s_Cons: "\<A>s (e # es) A = \<A>s es (\<A> e A)" |

-- "Is value attached?"

	\<A>\<T>_Value: "\<A>\<T> (\<C> v) A \<longleftrightarrow> is_attached_type (\<T>\<^sub>c v)" |
	\<A>\<T>_Local: "\<A>\<T> (\<V> n) A \<longleftrightarrow> n \<in>\<^sup>\<top> A" |
	\<A>\<T>_If: "\<A>\<T> (if b then c\<^sub>1 else c\<^sub>2 end) A \<longleftrightarrow>
	  \<A>\<T> c\<^sub>1 (\<A> b A \<squnion> +[b]) \<and> \<A>\<T> c\<^sub>2 (\<A> b A \<squnion> -[b])" |

-- "Fallback"

	\<A>\<T>_Other: "\<A>\<T> _ A \<longleftrightarrow> True"

lemmas \<A>_\<A>\<T>_induct = \<A>_\<A>s_\<A>\<T>.induct[split_format(complete)]

fun At Af where
  "At (if b then e\<^sub>1 else e\<^sub>2 end) A =
    (if is_false e\<^sub>1 then At e\<^sub>2 (Af b A) else
    (if is_false e\<^sub>2 then At e\<^sub>1 (At b A) else
    \<A> (if b then e\<^sub>1 else e\<^sub>2 end) A))" |
  "At c A = \<A> c A \<squnion> +[c]" |
  "Af (if b then e\<^sub>1 else e\<^sub>2 end) A =
    (if is_true e\<^sub>1 then Af e\<^sub>2 (Af b A) else
    (if is_true e\<^sub>2 then Af e\<^sub>1 (At b A) else
    \<A> (if b then e\<^sub>1 else e\<^sub>2 end) A))" |
  "Af c A = \<A> c A \<squnion> -[c]"

abbreviation \<A>_rep (infixl "\<rhd>" 72) where "\<A>_rep A c \<equiv> \<A> c A"
abbreviation \<A>_true_rep (infixl "\<rhd>+" 72) where "A \<rhd>+ b \<equiv> At b A"
abbreviation \<A>_false_rep (infixl "\<rhd>-" 72) where "A \<rhd>- b \<equiv> Af b A"
abbreviation \<A>s_rep (infixl "\<rhd>\<rhd>" 72) where "\<A>s_rep A es \<equiv> \<A>s es A"
abbreviation \<A>\<T>_rep (infixl "\<hookrightarrow>" 71) where "A \<hookrightarrow> c \<equiv> \<A>\<T> c A"

definition loop_step_def [simp]: "loop_step e c A = A \<rhd> e \<rhd> c"
definition loop_computation_def [simp]: "loop_computation e c A = loop_operator (\<lambda>X. X \<rhd> e \<rhd> c) A"
abbreviation loop_computation_rep ("_ \<rhd>\<^sup>* '(_ \<rhd>\<^sup>- _')" [55, 55, 56]) where "A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<equiv> loop_computation e c A"

lemma "\<A>s es A = fold \<A> es A"
	by (induction es arbitrary: A) simp_all

lemma
  fixes c :: "('b, 't) expression" and es :: "('b, 't) expression list" and e :: "('b, 't) expression"
  shows
    \<A>_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<rhd> c \<le> Y \<rhd> c" and
    \<A>s_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<rhd>\<rhd> es \<le> Y \<rhd>\<rhd> es" and
    \<A>\<T>_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> e \<longrightarrow> Y \<hookrightarrow> e"
proof (induction (no_simp) rule: \<A>_\<A>\<T>_induct)
  fix v :: "'b value" and A and X Y :: "vname topset"
  show "X \<hookrightarrow> (\<C> v) \<longrightarrow> Y \<hookrightarrow> \<C> v" by simp
  assume "X \<le> Y"
  then show "X \<rhd> (\<C> v) \<le> Y \<rhd> \<C> v" by simp
next
  fix n A and X Y :: "vname topset"
  assume "X \<le> Y"
  then show "X \<rhd> (\<V> n) \<le> Y \<rhd> \<V> n" by simp
  then show "X \<hookrightarrow> (\<V> n) \<longrightarrow> Y \<hookrightarrow> \<V> n" by (auto simp add: topset_subsetD)
next
  fix n and e :: "('b, 't) expression" and A X Y :: "vname topset"
  show "X \<hookrightarrow> n ::= e \<longrightarrow> Y \<hookrightarrow> n ::= e" by simp
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> e \<longrightarrow> Y \<hookrightarrow> e"
    "\<And>X Y. \<lbrakk>A \<hookrightarrow> e; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "\<And>X Y. \<lbrakk>\<not> A \<hookrightarrow> e; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "X \<le> Y"
  then show "X \<rhd> (n ::= e) \<le> Y \<rhd> (n ::= e)"
    by (metis (no_types) \<A>_Assign topset_add_mono topset_add_rem_mono topset_rem_mono)
next
  fix c1 c2 :: "('b, 't) expression" and A X Y :: "vname topset"
  show "X \<hookrightarrow> c1 ;; c2 \<longrightarrow> Y \<hookrightarrow> c1 ;; c2" by simp
  assume
    "(\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c1 \<le> Y \<rhd> c1)"
    "(\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c2 \<le> Y \<rhd> c2)"
    "X \<le> Y"
  then show "X \<rhd> c1 ;; c2 \<le> Y \<rhd> c1 ;; c2" by simp
next
  fix n and A X Y :: "vname topset"
  show "X \<hookrightarrow> create n \<longrightarrow> Y \<hookrightarrow> create n" by simp
  assume
    "X \<le> Y"
  then show "X \<rhd> create n \<le> Y \<rhd> create n" by (simp add: topset_add_mono)
next
  fix e :: "('b, 't) expression" and f and a :: "('b, 't) expression list" and A X Y :: "vname topset"
  assume
    "X \<le> Y"
  show "X \<hookrightarrow> e \<^bsub>\<bullet>\<^esub> f (a) \<longrightarrow> Y \<hookrightarrow> e \<^bsub>\<bullet>\<^esub> f (a)" by simp
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>\<rhd> a \<le> Y \<rhd>\<rhd> a"
    "X \<le> Y"
  then show "X \<rhd> e \<^bsub>\<bullet>\<^esub> f (a) \<le> Y \<rhd> e \<^bsub>\<bullet>\<^esub> f (a)" by simp
next
  fix b c1 c2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> b \<le> Y \<rhd> b"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c1 \<le> Y \<rhd> c1"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> b \<le> Y \<rhd> b"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c2 \<le> Y \<rhd> c2"
    "X \<le> Y"
  then show "X \<rhd> if b then c1 else c2 end \<le> Y \<rhd> if b then c1 else c2 end" by simp
next
  fix b c1 c2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> b \<le> Y \<rhd> b" and
    lHT1: "\<And>X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> c1 \<longrightarrow> Y \<hookrightarrow> c1" and
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> b \<le> Y \<rhd> b" and
    lHT2: "\<And>X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> c2 \<longrightarrow> Y \<hookrightarrow> c2" and
    "X \<le> Y"
  then have
    "X \<rhd> b \<squnion> +[b] \<le> Y \<rhd> b \<squnion> +[b]" and
    "X \<rhd> b \<squnion> -[b] \<le> Y \<rhd> b \<squnion> -[b]" by simp_all
  then show "X \<hookrightarrow> if b then c1 else c2 end \<longrightarrow> Y \<hookrightarrow> if b then c1 else c2 end"
    by (simp add: lHT1 lHT2)
next
  fix e c :: "('b, 't) expression" and A X Y :: "vname topset"
  show "X \<hookrightarrow> until e loop c end \<longrightarrow> Y \<hookrightarrow> until e loop c end" by simp
  assume
    lHe: "\<And>x X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e" and
    "\<And>x X Y. X \<le> Y \<Longrightarrow> X \<rhd> c \<le> Y \<rhd> c"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "X \<le> Y"
  then have "mono (\<lambda>x. x \<rhd> e \<rhd> c)" by (simp add: monoI)
  with \<open>X \<le> Y\<close> have "X \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<le> Y \<rhd>\<^sup>* (e \<rhd>\<^sup>- c)" using loop_operator_mono' by simp
  then have "X \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd> e \<le> Y \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd> e" by (simp add: lHe)
  then show "X \<rhd> until e loop c end \<le> Y \<rhd> until e loop c end" by simp
next
  fix t and e :: "('b, 't) expression" and n and A X Y :: "vname topset"
  assume
    "X \<le> Y"
  then show "X \<hookrightarrow> attached t e as n \<longrightarrow> Y \<hookrightarrow> attached t e as n" by simp
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "X \<le> Y"
  then show "X \<rhd> attached t e as n \<le> Y \<rhd> attached t e as n" by simp
next
  fix A X Y :: "vname topset"
  assume
    "X \<le> Y"
  then show "X \<rhd> Exception \<le> Y \<rhd> Exception" using topset_member_top by simp
  then show "X \<hookrightarrow> Exception \<longrightarrow> Y \<hookrightarrow> Exception" by simp
next
  fix A X Y :: "vname topset"
  assume
    "X \<le> Y"
  then show "X \<rhd>\<rhd> [] \<le> Y \<rhd>\<rhd> []" by simp
next
  fix e :: "('b, 't) expression" and es :: "('b, 't) expression list" and A X Y :: "vname topset"
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>\<rhd> es \<le> Y \<rhd>\<rhd> es"
    "X \<le> Y"
  then show "X \<rhd>\<rhd> (e # es) \<le> Y \<rhd>\<rhd> (e # es)" by simp
qed
  

lemma
  \<A>_mono: "mono (\<lambda> X. X \<rhd> c)" and
  \<A>\<T>_mono: "mono (\<lambda> X. X \<hookrightarrow> c)"
proof
  show "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c \<le> Y \<rhd> c" by (simp add: \<A>_mono')
next
  have "\<And>X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> c \<le> Y \<hookrightarrow> c" using \<A>\<T>_mono' by simp
  then show "mono (\<lambda> X. X \<hookrightarrow> c)"
    by (simp add: monoI)
qed

lemma loop_computation_le0:
  fixes e c :: "('b, 't) expression"
  shows "A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<le> A"
    by (metis gfp_least loop_computation_def loop_function_def loop_operator_def topset_inter_subset_iff)

lemma loop_step_mono: "mono (loop_step e c)"
  by (simp add: \<A>_mono monoD monoI)

lemma loop_step_mono' [simp, intro]: "mono (\<lambda> X. X \<rhd> e \<rhd> c)"
  by (simp add: \<A>_mono' monoI)

lemma loop_computation_mono: "mono (loop_computation e c)"
  by (metis loop_computation_def loop_operator_mono' monoI)

lemma loop_computation_le1:
  fixes e c :: "('b, 't) expression"
  shows "A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<le> A \<rhd> e \<rhd> c"
proof -
  let ?f = "(\<lambda>X. X \<rhd> e \<rhd> c)"
  have "mono ?f" by simp
  then have "loop_operator ?f A = loop_function ?f A (loop_operator ?f A)"
    by (rule loop_operator_unfold)
  also have "\<dots> \<le> loop_function ?f A A"
    using \<A>_mono' topset_inter_mono2
      by (metis \<open>mono ?f\<close> loop_function_def loop_operator_le0)
  finally show ?thesis by (simp add: topset_inter_subset_iff)
qed

lemma loop_application1: "A \<rhd> until e loop c end \<le> A \<rhd> e \<rhd> c \<rhd> until e loop c end"
proof -
  let ?f = "(\<lambda>X. X \<rhd> e \<rhd> c)"
  have "mono ?f" by simp
  then have "loop_operator ?f A \<le> loop_operator ?f (?f A)" by (rule loop_operator_le1)
  then show ?thesis by (simp add: \<A>_mono')
qed

lemma \<A>_to_all [simp]: "\<A> c \<top> = \<top>"
proof (induction c)
  case (Loop b c)
  then show ?case by (simp_all add: topset_Union_def gfp_def)
  case (Call e f a)
  then show ?case by (induction a,  simp_all)
qed simp_all

lemma reachability:
  assumes
    HR: "B \<rhd> c \<noteq> \<top>" and
    HS: "A \<le> B"
  shows
    "A \<rhd> c \<noteq> \<top>"
proof
  assume
    lH: "A \<rhd> c = \<top>"
  from HS have "A \<rhd> c \<le> B \<rhd> c" by (rule \<A>_mono')
  with lH have "\<top> \<le> B \<rhd> c" by simp
  then have "B \<rhd> c = \<top>" using topset_top_le by simp
  with HR show False by simp
qed

lemma attachment_loop_condition: "A \<rhd> until e loop c end \<le> A \<rhd> e"
proof -
  let ?F = "\<lambda> B. B \<rhd> e \<rhd> c"
  have "mono ?F" by (simp add: \<A>_mono' monoD monoI topset_inter_mono_arg2)
  then have "A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<le> loop_function ?F A (A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c))" using loop_operator_unfold
    by (metis loop_computation_def order_refl)
  then have "A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<le> A" by (simp add: topset_inter_subset_iff)
  then show ?thesis by (simp add: \<A>_mono')
qed

subsection "Attachment validity rules and type checks"

text {* A predicate that tells if an expression is valid with respect to attachment rules
and what is the expected type of the expression. *}

type_synonym attachment_environment = "attachment_mark environment"

definition declared_attachment_type_of_local :: "attachment_environment \<Rightarrow> vname \<Rightarrow> attachment_type option" ("\<T>\<^sub>l") where
	"\<T>\<^sub>l \<Gamma> n = (case \<Gamma> n of None \<Rightarrow> None | Some T \<Rightarrow> Some (type_attachment (\<lambda> x. False) T))"
	  -- {* TODO: Provide implementation for @{term is_expanded} *}
declare declared_attachment_type_of_local_def [simp]

fun declared_attachment_type_of :: "attachment_environment \<Rightarrow> ('b, 't) expression \<Rightarrow> attachment_type" ("\<T>d") where
	"\<T>d _ (\<C> v) = \<T>\<^sub>c v" |
	"\<T>d \<Gamma> (\<V> n) = (case \<T>\<^sub>l \<Gamma> n of None \<Rightarrow> Detachable | Some T \<Rightarrow> T)" |
	"\<T>d \<Gamma> (if b then c1 else c2 end) = upper_bound (\<T>d \<Gamma> c1) (\<T>d \<Gamma> c2)" |
	"\<T>d _ _ = Attached"

inductive
	AT :: "attachment_environment \<Rightarrow> vname topset \<Rightarrow> ('b, 't) expression \<Rightarrow> attachment_type \<Rightarrow> bool" ("_, _ \<turnstile> _ : _" [60, 54, 0, 60] 60)
where
	AT_Value: "\<Gamma>, A \<turnstile> \<C> v: \<T>\<^sub>c v" |
	AT_Local1: "n \<in>\<^sup>\<top> A \<Longrightarrow> \<Gamma>, A \<turnstile> \<V> n: Attached" |
	AT_Local2: "\<not> n \<in>\<^sup>\<top> A \<Longrightarrow> \<Gamma>, A \<turnstile> \<V> n: Detachable" |
	AT_Seq: "\<lbrakk>\<Gamma>, A \<turnstile> c\<^sub>1: Attached; \<Gamma>, \<A> c\<^sub>1 A \<turnstile> c\<^sub>2: Attached\<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> (c\<^sub>1;; c\<^sub>2): Attached" |
	AT_Assign: "\<Gamma>, A \<turnstile> e: T \<Longrightarrow> \<Gamma>, A \<turnstile> (n ::= e): Attached" |
	AT_Create: "\<Gamma>, A \<turnstile> (create n): Attached" |
	AT_Call: "\<Gamma>, A \<turnstile> e: Attached \<Longrightarrow> \<Gamma>, A \<turnstile> (e \<^bsub>\<bullet>\<^esub> f (a)): Attached" |
	AT_If: "\<lbrakk>
	    \<Gamma>, A \<turnstile> b: Attached;
	    \<Gamma>, \<A> b A \<squnion> +[b] \<turnstile> c\<^sub>1: T\<^sub>1;
	    \<Gamma>, \<A> b A \<squnion> -[b] \<turnstile> c\<^sub>2: T\<^sub>2
	  \<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> if b then c\<^sub>1 else c\<^sub>2 end: upper_bound T\<^sub>1 T\<^sub>2" |
	AT_Loop: "\<lbrakk>
      \<Gamma>, A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<turnstile> e: Attached;
      \<Gamma>, A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd> e \<turnstile> c: Attached
    \<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> until e loop c end: Attached" |
	AT_Test: "\<Gamma>, A \<turnstile> e: T \<Longrightarrow> \<Gamma>, A \<turnstile> (attached t e as n): Attached" |
	AT_Exception: "\<Gamma>, A \<turnstile> Exception: Attached"

paragraph "Type checks inductive cases"

lemmas at_induct = AT.induct[split_format(complete)]
inductive_cases ValueE[elim!]: "\<Gamma>, A \<turnstile> \<C> v: T"
inductive_cases LocalE[elim!]: "\<Gamma>, A \<turnstile> \<V> n: T"
inductive_cases SeqE[elim!]: "\<Gamma>, A \<turnstile> c1 ;; c2: T"
inductive_cases AssignE[elim!]: "\<Gamma>, A \<turnstile> n ::= e: T"
inductive_cases CreateE[elim!]: "\<Gamma>, A \<turnstile> create n: T"
inductive_cases CallE[elim!]: "\<Gamma>, A \<turnstile> n \<^bsub>\<bullet>\<^esub> f (a): T"
inductive_cases IfE[elim!]: "\<Gamma>, A \<turnstile> if b then c1 else c2 end: T"
inductive_cases LoopE[elim!]: "\<Gamma>, A \<turnstile> until e loop c end: T"
inductive_cases TestE[elim!]: "\<Gamma>, A \<turnstile> attached t e as x: T"
inductive_cases ExceptionE[elim!]: "\<Gamma>, A \<turnstile> Exception: T"

paragraph "Type checks properties"

lemma checked_attached_is_attached':
  assumes
    HA: "\<Gamma>, A \<turnstile> e: T" and
    HT: "T = Attached"
  shows
    "A \<hookrightarrow> e"
using assms
proof (induction rule: "AT.induct")
  case (AT_If \<Gamma> A b c\<^sub>1 T\<^sub>1 c\<^sub>2 T\<^sub>2)
  moreover then have "T\<^sub>1 = Attached" and "T\<^sub>2 = Attached"
    using upper_bound_bot_left' upper_bound_bot_right' by blast+
  ultimately show ?case by simp
qed simp_all

lemma checked_attached_is_attached: "\<Gamma>, A \<turnstile> e: Attached \<Longrightarrow> A \<hookrightarrow> e"
  by (simp add: checked_attached_is_attached')

lemma AT_loop: "\<Gamma>, A \<turnstile> until e loop c end : T \<Longrightarrow> \<Gamma>, A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<turnstile> until e loop c end : T"
proof -
  assume
    lH: "\<Gamma>, A \<turnstile> until e loop c end : T"
  then have
    lHe: "\<Gamma>, A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<turnstile> e: Attached" and
    lHc: "\<Gamma>, A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd> e \<turnstile> c: Attached" and
    lHt: "T = Attached" by auto
  have "mono (\<lambda>X. X \<rhd> e \<rhd> c)" by (simp add: \<A>_mono' monoI)
  then have "A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) = A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c)"
    using loop_operator_idem loop_computation_def by auto
  with lHe lHc have
    "\<Gamma>, A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<turnstile> e: Attached" and
    "\<Gamma>, A \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd>\<^sup>* (e \<rhd>\<^sup>- c) \<rhd> e \<turnstile> c: Attached" by simp_all
  with lHt show ?thesis by (simp add: AT_Loop)
qed

end