section "Attachment status of instances"

theory MemoryAttachment imports Memory ValueAttachment begin

lemma instance_is_attached: "instance m t = \<lfloor>(m', v)\<rfloor> \<Longrightarrow> is_attached_value v"
proof -
  assume "instance m t = \<lfloor>(m', v)\<rfloor>"
  then have
    HA: "Value.is_attached v" by (auto simp add: instance_def)
  then show ?thesis
  proof (cases v)
    case (Reference x)
    then show ?thesis by (cases x, insert Reference HA, simp_all)
  qed simp_all
qed

lemma instance_is_not_void: "instance m t = \<lfloor>(m', v)\<rfloor> \<Longrightarrow> v \<noteq> Void\<^sub>v"
  using instance_is_attached using attached_value_is_not_void by simp 

lemma instance_is_value: "instance m t = \<lfloor>(m', v)\<rfloor> \<Longrightarrow> is_value v"
proof -
  assume "instance m t = \<lfloor>(m', v)\<rfloor>"
  moreover hence "is_attached_value v" by (rule instance_is_attached)
  ultimately show ?thesis
    by (simp add: basic_memory.direct_instance instance_def is_value_def)
qed

end