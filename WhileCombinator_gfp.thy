theory WhileCombinator_gfp
imports "~~/Src/HOL/Library/While_Combinator"
begin

subsection "Kleene iteration"

lemma Kleene_iter_gpfp:
assumes "mono f" and "p \<le> f p" shows "p \<le> (f^^k) (top::'a::order_top)"
proof(induction k)
  case 0 show ?case by simp
next
  case Suc
  from monoD[OF assms(1) Suc] assms(2)
  show ?case by simp
qed

subsection \<open>Total version\<close>

text\<open>Proving termination:\<close>

text "Kleene iteration starting from some finite bounding maximum set:"

lemma while_option_finite_subset_Some_on_max: fixes C :: "'a set"
  assumes "mono f" and "!!X. X \<subseteq> C \<Longrightarrow> f X \<subseteq> C" and "finite C"
  shows "\<exists>P. while_option (\<lambda>A. f A \<noteq> A) f C = Some P"
proof(rule measure_while_option_Some[where
    f= "%A::'a set. card A" and P= "%A. A \<subseteq> C \<and> f A \<subseteq> A" and s= "C"])
  fix A assume A: "A \<subseteq> C \<and> f A \<subseteq> A" "f A \<noteq> A"
  show "(f A \<subseteq> C \<and> f (f A) \<subseteq> f A) \<and> card (f A) < card A"
    (is "?L \<and> ?R")
  proof
    show ?L by(metis A(1) assms(2) monoD[OF \<open>mono f\<close>])
    show ?R by (metis A assms(3) card_seteq linorder_le_less_linear rev_finite_subset)
  qed
qed (simp add: assms(2))


lemma gfp_the_while_option:
  assumes "mono f" and "!!X. f X \<subseteq> C" and "finite C"
  shows "gfp f = the(while_option (\<lambda>A. f A \<noteq> A) f C)"
proof-
  obtain P where
    Hw: "while_option (\<lambda>A. f A \<noteq> A) f C = Some P"
      using while_option_finite_subset_Some_on_max[OF assms] by blast
  from while_option_stop2[OF this] obtain n where
    H1: "f P = P" and
    H2: "(f ^^ n) C = P" by auto
  with assms(1) have
    "\<And>n. P \<subseteq> (f ^^ n) UNIV" by (simp add: Kleene_iter_gpfp)
  moreover from H2 assms(1) have
    "\<And>A. \<not> A \<subseteq> C \<or> (f ^^ n) A \<subseteq> P" by (metis funpow_mono)
  ultimately have
    "(f ^^ Suc n) UNIV = P" by (metis assms(2) funpow_simps_right(2) o_apply subset_antisym)
  then have
    "gfp f \<subseteq> P" by (metis Kleene_iter_gpfp assms(1) gfp_lemma2)
  with Hw H1 show ?thesis by (metis gfp_upperbound option.sel order_refl subset_antisym)
qed

lemma gfp_while:
  assumes "mono f" and "!!X. f X \<subseteq> C" and "finite C"
  shows "gfp f = while (\<lambda>A. f A \<noteq> A) f C"
unfolding while_def using assms by (rule gfp_the_while_option)

end
