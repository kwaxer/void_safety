theory ValueAttachment imports
	TypeAttachment
	Value
begin

subsection "Attachment type of simple expressions"

definition declared_attachment_type_of_constant :: "'a value \<Rightarrow> attachment_type" ("\<T>\<^sub>c") where
	"\<T>\<^sub>c v = (case v of
				Unit \<Rightarrow> Attached |
				Object _ \<Rightarrow> Attached |
				Reference x \<Rightarrow> (case x of AttachedObject _ \<Rightarrow> Attached | _ \<Rightarrow> Detachable))"
declare declared_attachment_type_of_constant_def [simp]

lemma expanded_attachment: "is_expanded v \<Longrightarrow> \<T>\<^sub>c v = Attached"
  by (cases v, simp_all)

lemma reference_attachment: "is_reference v \<Longrightarrow> \<T>\<^sub>c v = Attached \<or> \<T>\<^sub>c v = Detachable"
proof (cases v)
  case (Reference x) thus ?thesis by (cases x, simp_all)
qed simp_all

definition is_attached_value :: "'a value \<Rightarrow> bool" where
	"is_attached_value v = is_attached_type (\<T>\<^sub>c v)"
declare is_attached_value_def [simp]

lemma attached_value_is_attached_constant [iff]: "is_attached v \<longleftrightarrow> is_attached_value v"
proof (cases v)
  case (Reference x)
  then show ?thesis by (cases x) (simp_all add: is_attached_type_def)
qed simp_all

lemma attached_value_is_not_void [iff]: "is_attached_value v \<longleftrightarrow> v \<noteq> Void\<^sub>v"
	using is_attached_if_not_void by fastforce

lemma attached_typed_value_is_not_void [iff]: "\<T>\<^sub>c v = Attached \<longleftrightarrow> v \<noteq> Void\<^sub>v"
	using attached_value_is_not_void is_attached_type_def by auto

lemma detachable_typed_value_is_void [iff]: "\<T>\<^sub>c v = Detachable \<longleftrightarrow> v = Void\<^sub>v"
proof (cases v)
	case (Reference x) then show ?thesis by (cases x) (simp_all add: is_attached_type_def)
qed simp_all

end