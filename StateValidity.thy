theory StateValidity imports
	EnvironmentAttachment
	State
	TopSet
	Value
begin

type_synonym 'local_type local_state = "'local_type State.local_state"
type_synonym ('local_type, 'memory) state = "('local_type, 'memory) State.state"

subsection "Run-time attachment status"

text {* Function @{term valid_local_values} tells if a given state of local variables is valid. *}

definition valid_local_values::
  "'local_type local_state \<Rightarrow> bool"(*<*)("\<turnstile> _" [60] 60)(*>*)
				-- {* Notation: @{term "\<turnstile> local_state"} *}
where
	"\<turnstile> loc \<longleftrightarrow> (\<forall> name value. loc name = \<lfloor>value\<rfloor> \<longrightarrow> value \<noteq> Unit)"

definition local_has_value:: "'local_type local_state \<Rightarrow> vname \<Rightarrow> bool" where
  "local_has_value l name \<longleftrightarrow> (\<exists> v. l name = \<lfloor>v\<rfloor>)"

definition valid_local_state::
		"attachment_environment \<Rightarrow> 'local_type local_state \<Rightarrow> bool"
where
	"valid_local_state \<Gamma> l \<longleftrightarrow>
	    (\<forall> name T. \<Gamma> name = \<lfloor>T\<rfloor> \<longrightarrow> local_has_value l name)"

lemma local_has_value': "\<lbrakk>valid_local_state \<Gamma> l; \<Gamma> n = \<lfloor>T\<rfloor>\<rbrakk> \<Longrightarrow> \<exists> v. l n = \<lfloor>v\<rfloor>"
  by (simp add: valid_local_state_def local_has_value_def)

lemma local_state_upd:
  "\<lbrakk>valid_local_state \<Gamma> l\<rbrakk> \<Longrightarrow>
    valid_local_state \<Gamma> (l (name \<mapsto> value))"
using assms
  by (simp add: valid_local_state_def local_has_value_def)

definition valid_local_attachment_state::
		"vname topset \<Rightarrow> 'local_type local_state \<Rightarrow> bool"
where
	"valid_local_attachment_state A l \<longleftrightarrow>
	    A \<noteq> \<top> \<longrightarrow> (\<forall> name. name \<in>\<^sup>\<top> A \<longrightarrow>
					(\<exists> value. l name = \<lfloor>value\<rfloor> \<and> value \<noteq> Void\<^sub>v))"

lemma local_attachment_state_top [simp]: "valid_local_attachment_state \<top> l"
  by (simp add: valid_local_attachment_state_def)

lemma local_attachment_state_bottom [simp]: "valid_local_attachment_state \<lceil>\<emptyset>\<rceil> l"
  by (simp add: valid_local_attachment_state_def)

lemma local_attachment_state_union:
  "\<lbrakk>valid_local_attachment_state A l; valid_local_attachment_state B l\<rbrakk> \<Longrightarrow>
    valid_local_attachment_state (A \<squnion> B) l"
proof (cases A, simp, cases B, simp)
  fix a b
  assume
    HA: "A = \<lceil>a\<rceil>" and
    HB: "B = \<lceil>b\<rceil>" and
    VA: "valid_local_attachment_state A l" and
    VB: "valid_local_attachment_state B l"
  then have
    HAB: "A \<squnion> B = \<lceil>a \<union> b\<rceil>" using topset_union_def by (metis topset.simps(5))
  {
    fix name
    assume
      *: "name \<in>\<^sup>\<top> A \<squnion> B"
    with HAB have "name \<in> a \<union> b" by simp 
    moreover from * HA VA have "\<exists> value. l name = \<lfloor>value\<rfloor> \<and> value \<noteq> Void\<^sub>v" if "name \<in>\<^sup>\<top> A"
      using that valid_local_attachment_state_def by blast
    moreover from * HB VB have "\<exists> value. l name = \<lfloor>value\<rfloor> \<and> value \<noteq> Void\<^sub>v" if "name \<in>\<^sup>\<top> B"
      using that valid_local_attachment_state_def by blast
    ultimately have "\<exists> value. l name = \<lfloor>value\<rfloor> \<and> value \<noteq> Void\<^sub>v" using HA HB by blast
  }
  then have "\<forall> name. name \<in>\<^sup>\<top> A \<squnion> B \<longrightarrow>
					(\<exists> value. l name = \<lfloor>value\<rfloor> \<and> value \<noteq> Void\<^sub>v)" by simp
  then show "valid_local_attachment_state (A \<squnion> B) l" using valid_local_attachment_state_def by blast
qed

lemma local_attachment_state_exc:                   
  assumes
    "valid_local_attachment_state A l" and
    "\<not> name \<in>\<^sup>\<top> A"
  shows "valid_local_attachment_state A (l (name \<mapsto> value))"
using assms valid_local_attachment_state_def by (metis (mono_tags) fun_upd_other)

lemma local_attachment_state_anti_mono:
  assumes
    "B \<le> A" and
    "A \<noteq> \<top>"
  shows "valid_local_attachment_state A l \<Longrightarrow> valid_local_attachment_state B l"
using assms by (simp add: topset_subsetD valid_local_attachment_state_def)

lemma local_attachment_state_sub:
  "valid_local_attachment_state A l \<Longrightarrow>
  valid_local_attachment_state (A \<ominus> name) (l (name \<mapsto> value))"
  apply (cases A)
  apply simp
  apply (simp add: topset_member_def valid_local_attachment_state_def topset_rem_def)
done 

lemma local_attachment_state_add:
  assumes
    HA: "value \<noteq> Void\<^sub>v" and
    HV: "valid_local_attachment_state A l"
  shows "valid_local_attachment_state (A \<oplus> name) (l (name \<mapsto> value))"
proof (cases A)
  case Top then show ?thesis by (simp add: valid_local_attachment_state_def)
next
  case (Set a)
  with HV have "\<forall> n. n \<in>\<^sup>\<top> A \<longrightarrow> (\<exists> v. l n = \<lfloor>v\<rfloor> \<and> v \<noteq> Void\<^sub>v)"
    using valid_local_attachment_state_def by auto
  with Set HA have "\<forall> n. n \<in> (a \<union> {name}) \<longrightarrow> (\<exists> v. (l (name \<mapsto> value)) n = \<lfloor>v\<rfloor> \<and> v \<noteq> Void\<^sub>v)"
    by (simp add: topset_member_def)
  with Set have "\<forall> n. n \<in>\<^sup>\<top> (A \<oplus> name) \<longrightarrow> (\<exists> v. (l (name \<mapsto> value)) n = \<lfloor>v\<rfloor> \<and> v \<noteq> Void\<^sub>v)"
    by (simp add: topset_add_def topset_member_def)
  with Set show ?thesis using valid_local_attachment_state_def by blast
qed

lemma local_attachment_state_upd:
  assumes
    HA: "value \<noteq> Void\<^sub>v" and
    HV: "valid_local_attachment_state A l"
  shows "valid_local_attachment_state A (l (name \<mapsto> value))"
using assms local_attachment_state_add local_attachment_state_exc
	topset_add_insert topset_insert_absorb by metis

text {* Function @{term valid_state} tells if a given run-time state conforms to
a specified attachment status of variables. *}

primrec valid_state::
		"attachment_environment \<Rightarrow> vname topset \<Rightarrow> ('l, 'm) state \<Rightarrow> bool"(*<*)("_, _ \<turnstile> _ \<surd>\<^sub>s" [60, 60, 60] 60)(*>*)
				-- {* Notation: @{term "environment, attached_vars \<turnstile> state \<surd>\<^sub>s"} *}
where
	"\<Gamma>, A \<turnstile> (loc, mem) \<surd>\<^sub>s \<longleftrightarrow>
	  valid_local_state \<Gamma> loc \<and>
	  valid_local_attachment_state A loc"

lemma valid_stateI [intro]: "\<lbrakk>valid_local_state \<Gamma> loc; valid_local_attachment_state A loc\<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> (loc, mem) \<surd>\<^sub>s"
  by simp

lemma valid_stateD1 [dest?]: "\<Gamma>, A \<turnstile> (loc, mem) \<surd>\<^sub>s \<Longrightarrow> valid_local_state \<Gamma> loc"
  by simp

lemma valid_stateD2 [dest?]: "\<Gamma>, A \<turnstile> (loc, mem) \<surd>\<^sub>s \<Longrightarrow> valid_local_attachment_state A loc"
  by simp

lemma valid_state_anti_mono:
  assumes
    "B \<le> A" and
    "A \<noteq> \<top>"
  shows "\<Gamma>, A \<turnstile> s \<surd>\<^sub>s \<Longrightarrow> \<Gamma>, B \<turnstile> s \<surd>\<^sub>s"
proof -
  obtain l m where
    *: "s = (l, m)" by fastforce
  moreover assume
    "\<Gamma>, A \<turnstile> s \<surd>\<^sub>s"
  ultimately have
    "valid_local_state \<Gamma> l" and
    "valid_local_attachment_state A l"
      by auto
  then have
    "valid_local_state \<Gamma> l" and
    "valid_local_attachment_state B l" using assms local_attachment_state_anti_mono by auto
  with * show ?thesis by simp
qed

lemma valid_state_union: "\<lbrakk>\<Gamma>, A \<turnstile> (loc, mem) \<surd>\<^sub>s; \<Gamma>, B \<turnstile> (loc, mem) \<surd>\<^sub>s\<rbrakk> \<Longrightarrow>  \<Gamma>, A \<squnion> B \<turnstile> (loc, mem) \<surd>\<^sub>s"
  by (simp add: local_attachment_state_union)

paragraph {* Run-time state decomposition. *}

lemma local_has_value: "\<lbrakk>\<Gamma>, A \<turnstile> s \<surd>\<^sub>s; \<Gamma> n = \<lfloor>T\<rfloor>\<rbrakk> \<Longrightarrow> \<exists> v. fst s n = \<lfloor>v\<rfloor>"
proof -
  assume "\<Gamma>, A \<turnstile> s \<surd>\<^sub>s" and "\<Gamma> n = \<lfloor>T\<rfloor>"
  moreover obtain l m where "l = fst s" and "m = snd s" by simp
  moreover have "\<lbrakk>\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s; \<Gamma> n = \<lfloor>T\<rfloor>\<rbrakk> \<Longrightarrow> \<exists> v. l n = \<lfloor>v\<rfloor>"
    by (auto simp add: local_has_value')
  ultimately show ?thesis by simp
qed

lemma local_is_attached: "\<lbrakk>\<Gamma>, A \<turnstile> s \<surd>\<^sub>s; A \<noteq> \<top>; n \<in>\<^sup>\<top> A\<rbrakk> \<Longrightarrow> \<exists> v. fst s n = \<lfloor>v\<rfloor> \<and> v \<noteq> Void\<^sub>v"
proof -
  assume "\<Gamma>, A \<turnstile> s \<surd>\<^sub>s" and "A \<noteq> \<top>" and "n \<in>\<^sup>\<top> A"
  moreover obtain l m where "l = fst s" and "m = snd s" by simp
  moreover have "\<lbrakk>\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s; A \<noteq> \<top>; n \<in>\<^sup>\<top> A\<rbrakk> \<Longrightarrow> \<exists> v. l n = \<lfloor>v\<rfloor> \<and> v \<noteq> Void\<^sub>v"
    by (auto simp add: valid_local_attachment_state_def)
  ultimately show ?thesis by simp
qed

paragraph {* Run-time state updates. *}

lemma attachment_set_sub:
  assumes "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
  shows "\<Gamma>, A \<ominus> name \<turnstile> (l (name \<mapsto> value), m) \<surd>\<^sub>s"
using assms by (simp add: local_state_upd local_attachment_state_sub)

lemma attachment_set_add:
  assumes
    "value \<noteq> Void\<^sub>v" and
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
  shows "\<Gamma>, A \<oplus> name \<turnstile> (l (name \<mapsto> value), m) \<surd>\<^sub>s"
using assms local_attachment_state_add local_state_upd by auto

lemma attachment_set_int1:
  assumes
    HV: "\<Gamma>, A1 \<turnstile> (l1, m1) \<surd>\<^sub>s" and
    HD1: "A1 \<noteq> \<top>" and
    HD2: "A2 \<noteq> \<top>"
  shows
    "\<Gamma>, A1 \<sqinter> A2 \<turnstile> (l1, m1) \<surd>\<^sub>s"
proof -
    from HD1 HD2 have "A1 \<sqinter> A2 \<noteq> \<top>" using topset_inter_topD1 by auto
    moreover have "A1 \<sqinter> A2 \<le> A1" by (rule topset_inter_lower1)
    with HV HD1 HD2 show ?thesis using valid_state_anti_mono by blast
qed

lemma attachment_set_int2:
  assumes
    HV: "\<Gamma>, A2 \<turnstile> (l2, m2) \<surd>\<^sub>s" and
    HD2: "A2 \<noteq> \<top>" and
    HD1: "A1 \<noteq> \<top>"
  shows
    "\<Gamma>, A1 \<sqinter> A2 \<turnstile> (l2, m2) \<surd>\<^sub>s"
proof -
  from assms have "\<Gamma>, A2 \<sqinter> A1 \<turnstile> (l2, m2) \<surd>\<^sub>s" by (rule attachment_set_int1)
  then show ?thesis by (simp add: topset_inter_commute)
qed

definition attachment_valid_state ("_ \<turnstile> _ \<surd>\<^sub>s") where
  "attachment_valid_state \<Gamma> s = \<Gamma>, \<emptyset> \<turnstile> s \<surd>\<^sub>s"

end