section "Attachment correctness with big-step semantics"

theory BigStepSafety imports
	BigStep MemoryAttachment ExpressionValidity StateValidity
begin

no_notation floor ("\<lfloor>_\<rfloor>")

lemma
	assumes
		HT: "\<Gamma>, A \<turnstile> \<V> n : Attached" and
		HV: "\<Gamma>, A \<turnstile> s \<surd>\<^sub>s" and
		HD: "A \<noteq> \<top>"
	shows "\<exists> v. \<Gamma> \<turnstile> \<langle>\<V> n, s\<rangle> \<Rightarrow> \<langle>\<C> v, s\<rangle> \<and> is_attached_value v"
proof -
  from HT have "n \<in>\<^sup>\<top> A" by auto
  moreover from HV have "valid_local_attachment_state A (fst s)"
    by (metis prod.collapse valid_state.simps)
  ultimately obtain v where "(fst s) n = \<lfloor>v\<rfloor>" and "is_attached_value v"
    using HD HV local_is_attached by blast
  moreover then have "\<Gamma> \<turnstile> \<langle>\<V> n, s\<rangle> \<Rightarrow> \<langle>\<C> v, s\<rangle>"
    by (metis Local prod.collapse)
  ultimately show ?thesis by auto
qed

subsection "Preservation of valid run-time state"

lemma valid_state_preservation_by_value_assignment:
	assumes
		HS: "\<Gamma> \<turnstile> \<langle>n ::= \<C> v, (l, m)\<rangle> \<Rightarrow> \<langle>c', (l', m')\<rangle>" and
		VS: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
	shows "\<Gamma>, \<A> (n ::= \<C> v) A \<turnstile> (l', m') \<surd>\<^sub>s"
using assms
proof (cases "A")
	case (Set a)
	show ?thesis
	proof (cases "v \<noteq> Void\<^sub>v")
		case True
    moreover with Set have "\<A> (n ::= \<C> v) A = A \<oplus> n" by simp
    moreover from VS have "\<Gamma>, A \<turnstile> (l, m') \<surd>\<^sub>s" by auto
    with True have "\<Gamma>, A \<oplus> n \<turnstile> (l (n \<mapsto> v), m') \<surd>\<^sub>s" using attachment_set_add by fastforce
    moreover from HS have *: "l' = l (n \<mapsto> v)" by auto
    ultimately show ?thesis by auto
	next
		case False
    moreover from False Set have "\<A> (n ::= \<C> v) A = A \<ominus> n" by simp
    moreover from VS have "\<Gamma>, A \<turnstile> (l, m') \<surd>\<^sub>s" by auto
    then have "\<Gamma>, A \<ominus> n \<turnstile> (l (n \<mapsto> v), m') \<surd>\<^sub>s" by (rule attachment_set_sub)
    moreover from HS have *: "l' = l (n \<mapsto> v)" by auto
    ultimately show ?thesis by auto
  qed
next
  case Top
  with assms \<A>_to_all show ?thesis using local_state_upd by fastforce
qed

lemma valid_state_preservation_by_creation:
	assumes
		HS: "\<Gamma> \<turnstile> \<langle>create n, (l, m)\<rangle> \<Rightarrow> \<langle>c', (l', m')\<rangle>" and
		HT: "\<Gamma>, A \<turnstile> create n : T" and
		VS: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
		HE: "c' \<noteq> Exception"
	shows "\<Gamma>, \<A> (create n) A \<turnstile> (l', m') \<surd>\<^sub>s"
proof -
	from HS HE obtain Tn v where
	  "\<Gamma> n = \<lfloor>Tn\<rfloor>" and
		"instance m Tn = Some (m', v)" and
		*: "l'= l (n \<mapsto> v)" by auto
	hence "v \<noteq> Void\<^sub>v" by (auto simp add: instance_def)
	moreover from VS have "\<Gamma>, A \<turnstile> (l, m') \<surd>\<^sub>s" by auto
	ultimately have "\<Gamma>, A \<oplus> n \<turnstile> (l (n \<mapsto> v), m') \<surd>\<^sub>s" by (rule attachment_set_add)
	with * show ?thesis by auto
qed

lemma valid_local_state_preservation:
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
  assumes
		"valid_local_state \<Gamma> (fst s)"
	shows
		valid_local_state_preservation_e: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow> valid_local_state \<Gamma> (fst s')" and
		valid_local_state_preservation_es: "\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle> \<Longrightarrow> valid_local_state \<Gamma> (fst s')"
using assms
  by (induction rule: "big_step_big_steps.inducts")
     (simp_all add: instance_is_value local_state_upd "option.case_eq_if")

lemma valid_local_state_preservation':
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
  assumes
		"valid_local_state \<Gamma> l"
	shows
		valid_local_state_preservation_e': "\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow> valid_local_state \<Gamma> l'" and
		valid_local_state_preservation_es': "\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow> valid_local_state \<Gamma> l'"
using assms valid_local_state_preservation by auto

lemma indefinedness_preservation_e:
	assumes
		"\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle>"
		"\<Gamma>, \<top> \<turnstile> s \<surd>\<^sub>s"
	shows
		"\<Gamma>, \<top> \<turnstile> s' \<surd>\<^sub>s"
using assms local_attachment_state_top valid_local_state_preservation_e valid_stateD1 valid_stateI
	by (metis prod.collapse)

lemma indefinedness_preservation_es:
	assumes
		"\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle>"
		"\<Gamma>, \<top> \<turnstile> s \<surd>\<^sub>s"
	shows
		"\<Gamma>, \<top> \<turnstile> s' \<surd>\<^sub>s"
using assms local_attachment_state_top valid_local_state_preservation_es valid_stateD1 valid_stateI
	by (metis prod.collapse)

lemma
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	shows
		definedness_preservation_e: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow> \<Gamma>, \<lceil>a\<rceil> \<turnstile> s \<surd>\<^sub>s \<Longrightarrow> \<exists> b. \<Gamma>, \<lceil>b\<rceil> \<turnstile> s' \<surd>\<^sub>s" and
		definedness_preservation_es: "\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle> \<Longrightarrow> \<Gamma>, \<lceil>a\<rceil> \<turnstile> s \<surd>\<^sub>s \<Longrightarrow> \<exists> b. \<Gamma>, \<lceil>b\<rceil> \<turnstile> s' \<surd>\<^sub>s"
using assms
proof (induction arbitrary: a and a rule: "big_step_big_steps.inducts")
  case (Value v s) then show ?case by auto
next
  case (Local l n v m) then show ?case by auto
next
  case (Seq c1 s v s' c2 c2' s'') then show ?case by auto
next
  case (Assign e s v l m n a)
  then obtain b::"vname set" where "\<Gamma>, \<lceil>b\<rceil> \<turnstile> (l, m) \<surd>\<^sub>s" by blast 
  then have "\<Gamma>, \<lceil>b\<rceil> \<ominus> n \<turnstile> (l(n \<mapsto> v), m) \<surd>\<^sub>s" by (rule attachment_set_sub)
  thus ?case
    by (metis topset.exhaust topset_inter_rem topset_inter_rem_distrib topset_inter_top_right)
next
  case (Create n T m m' v l a)
  moreover with "Create" obtain Tn and v :: "'b value" where "\<Gamma> n = \<lfloor>Tn\<rfloor>" "instance m Tn = \<lfloor>(m', v)\<rfloor>"
  	by simp
  moreover hence "instance m Tn = \<lfloor>(m', v)\<rfloor>" by simp
    hence "is_attached_value v" by (rule instance_is_attached)
  ultimately show ?case	using local_state_upd
  	valid_local_attachment_state_def attached_value_is_not_void by fastforce
next
	case ("Create\<^bsub>fail\<^esub>") then show ?case by auto
next
  case (Call n f s) then show ?case by auto
next
  case ("If\<^bsub>true\<^esub>" b c1 c2) then show ?case by auto
next
  case ("If\<^bsub>false\<^esub>" b c1 c2) then show ?case by auto
next
  case ("Loop\<^bsub>true\<^esub>" e c) then show ?case by auto
next
  case ("Loop\<^bsub>false\<^esub>" e c) then show ?case by meson
next
  case ("Test\<^bsub>true\<^esub>" e) then show ?case
    by (metis attachment_set_sub topset.collapse topset_rem_absorb) 
next
  case ("Test\<^bsub>false\<^esub>" e) then show ?case by simp
next
	case Nil then show ?case by auto
next
	case Cons then show ?case by auto
qed auto

lemma result_true: "\<lbrakk>is_true e; \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>c, s'\<rangle>\<rbrakk> \<Longrightarrow> c = True\<^sub>c \<or> c = Exception"
  using is_true.elims(2) by blast

lemma result_false: "\<lbrakk>is_false e; \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>c, s'\<rangle>\<rbrakk> \<Longrightarrow> c = False\<^sub>c \<or> c = Exception"
  using is_false.elims(2) by blast

lemma
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	assumes
		"A \<noteq> \<top>"
	shows
	  reachability_preservation: "
  		\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
  		e' \<noteq> Exception \<Longrightarrow>
  		A \<rhd> e \<noteq> \<top>" and
  	reachability_preservation_scope_true: "
  		\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
  		e' = True\<^sub>c \<Longrightarrow>
  		A \<rhd>+ e \<noteq> \<top>" and
  	reachability_preservation_scope_false: "
  		\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
  		e' = False\<^sub>c \<Longrightarrow>
  		A \<rhd>- e \<noteq> \<top>" and
	  reachability_preservation_es: "
  		\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
  		Exception \<notin> set es' \<Longrightarrow>
  		A \<rhd>\<rhd> es \<noteq> \<top>" and
	  "\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow> True" and
	  "\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow> True"
using assms
proof (induction arbitrary: A T and A Ts rule: big_step_induct)
  fix A :: "vname topset" and T
  case (Value v l m)
  assume "A \<noteq> \<top>"
  then show "A \<rhd> (\<C> v) \<noteq> \<top>" by simp
  then show "A \<rhd>+ (\<C> v) \<noteq> \<top>" by simp
  then show "A \<rhd>- (\<C> v) \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T
  case (Local l n v m)
  assume "A \<noteq> \<top>"
  then show "A \<rhd> (\<V> n) \<noteq> \<top>" by simp
  then show "A \<rhd>+ (\<V> n) \<noteq> \<top>" by simp
  then show "A \<rhd>- (\<V> n) \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T
  case (Seq c1 l m l1 m1 c2 c2' l' m')
  moreover assume
    "c2' \<noteq> Exception"
    "\<Gamma>, A \<turnstile> c1 ;; c2 : T"
    "A \<noteq> \<top>"
  ultimately show "A \<rhd> c1 ;; c2 \<noteq> \<top>" by auto
next
  fix A :: "vname topset" and T
  case (Seq c1 l m l1 m1 c2 c2' l' m')
  moreover assume
    "c2' = True\<^sub>c"
    "\<Gamma>, A \<turnstile> c1 ;; c2 : T"
    "A \<noteq> \<top>"
  ultimately show "A \<rhd>+ c1 ;; c2 \<noteq> \<top>"
  	using result_false by (cases "is_false (c1 ;; c2)") fastforce+
next
  fix A :: "vname topset" and T
  case (Seq c1 l m l1 m1 c2 c2' l' m')
  moreover assume
    "c2' = False\<^sub>c"
    "\<Gamma>, A \<turnstile> c1 ;; c2 : T"
    "A \<noteq> \<top>"
  ultimately show "A \<rhd>- c1 ;; c2 \<noteq> \<top>"
  	using result_true by (cases "is_true (c1 ;; c2)") fastforce+
next
  fix A :: "vname topset" and T n
  case (Assign e l m v l' m')
  moreover assume
    "\<Gamma>, A \<turnstile> n ::= e : T"
    "A \<noteq> \<top>"
  ultimately show "A \<rhd> n ::= e \<noteq> \<top>" by auto
  then show "A \<rhd>+ n ::= e \<noteq> \<top>" by simp
  then show "A \<rhd>- n ::= e \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T
  case (Create n Tn m m' v)
  assume
    "A \<noteq> \<top>"
  then show "A \<rhd> create n \<noteq> \<top>" by simp
  then show "A \<rhd>+ create n \<noteq> \<top>" by simp
  then show "A \<rhd>- create n \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T
  case ("Create\<^bsub>fail\<^esub>" n Tn m)
  assume
    "A \<noteq> \<top>"
  then show "A \<rhd> create n \<noteq> \<top>" by simp
  then show "A \<rhd>+ create n \<noteq> \<top>" by simp
  then show "A \<rhd>- create n \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T f
  case (Call e l m v le me es vs l' m')
  moreover assume
    lHc: "\<Gamma>, A \<turnstile> e \<^bsub>\<bullet>\<^esub> f (es) : T" and
    lHD: "A \<noteq> \<top>"
  moreover with Call.IH have "A \<rhd> e \<noteq> \<top>" by auto
  moreover have "Exception \<notin> set (map Value vs)" by auto
  moreover from lHc obtain Ts where "\<Gamma>, A \<rhd> e \<turnstile> es [:] Ts" by blast
  ultimately have "A \<rhd> e \<rhd>\<rhd> es \<noteq> \<top>" by blast
  then show "A \<rhd> e \<^bsub>\<bullet>\<^esub> f (es) \<noteq> \<top>" by auto
  then show "A \<rhd>+ e \<^bsub>\<bullet>\<^esub> f (es) \<noteq> \<top>" by simp
  then show "A \<rhd>- e \<^bsub>\<bullet>\<^esub> f (es) \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T c2
  case ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l1 m1)
  moreover assume
    lHc1: "c1' \<noteq> Exception" and
    lHc: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately have "A \<rhd>+ b \<noteq> \<top>" by auto
  moreover from lHc obtain Tc where "\<Gamma>, A \<rhd>+ b \<turnstile> c1 : Tc" by auto
  moreover from lHc1 have "A \<rhd>+ b \<rhd> c1 \<noteq> \<top>" using "If\<^bsub>true\<^esub>.IH"(4) calculation by blast
  ultimately show "A \<rhd> if b then c1 else c2 end \<noteq> \<top>"using topset_inter_topD1 by auto
next
  fix A :: "vname topset" and T c2
  case ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l1 m1)
  moreover assume
    lHc1: "c1' = True\<^sub>c" and
    lHc: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately have "A \<rhd>+ b \<noteq> \<top>" by auto
  moreover from lHc obtain Tc where "\<Gamma>, A \<rhd>+ b \<turnstile> c1 : Tc" by auto
  ultimately show "A \<rhd>+ if b then c1 else c2 end \<noteq> \<top>"
    using lHc1 "If\<^bsub>true\<^esub>" topset_inter_topD1 \<A>_If value_neq_exception result_false \<A>t_If 
    by (metis expression.inject(1) object_value.inject(2) value.inject(1))
next
  fix A :: "vname topset" and T c2
  case ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l1 m1)
  moreover assume
    lHc1: "c1' = False\<^sub>c" and
    lHc: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately have "A \<rhd>+ b \<noteq> \<top>" by auto
  moreover from lHc obtain Tc where "\<Gamma>, A \<rhd>+ b \<turnstile> c1 : Tc" by auto
  ultimately show "A \<rhd>- if b then c1 else c2 end \<noteq> \<top>"
    using lHc1 "If\<^bsub>true\<^esub>" topset_inter_topD1 \<A>_If value_neq_exception result_true \<A>f_If
    by (metis expression.inject(1) object_value.inject(2) value.inject(1))
next
  fix A :: "vname topset" and T c1
  case ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l2 m2)
  moreover assume
    lHc1: "c2' \<noteq> Exception" and
    lHc: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately have "A \<rhd>- b \<noteq> \<top>" by auto
  moreover from lHc obtain Tc where "\<Gamma>, A \<rhd>- b \<turnstile> c2 : Tc" by auto
  moreover from lHc1 have "A \<rhd>- b \<rhd> c2 \<noteq> \<top>" using "If\<^bsub>false\<^esub>.IH"(4) calculation by blast
  ultimately show "A \<rhd> if b then c1 else c2 end \<noteq> \<top>"using topset_inter_topD2 by auto
next
  fix A :: "vname topset" and T c1
  case ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l2 m2)
  moreover assume
    lHc2: "c2' = True\<^sub>c" and
    lHc: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately have "A \<rhd>- b \<noteq> \<top>" by auto
  moreover from lHc obtain Tc where "\<Gamma>, A \<rhd>- b \<turnstile> c2 : Tc" by auto
  ultimately show "A \<rhd>+ if b then c1 else c2 end \<noteq> \<top>"
    using lHc2 "If\<^bsub>false\<^esub>" topset_inter_topD2 \<A>_If value_neq_exception result_false \<A>t_If 
    by (metis expression.inject(1) object_value.inject(2) value.inject(1))
next
  fix A :: "vname topset" and T c1
  case ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l2 m2)
  moreover assume
    lHc2: "c2' = False\<^sub>c" and
    lHc: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately have "A \<rhd>- b \<noteq> \<top>" by auto
  moreover from lHc obtain Tc where "\<Gamma>, A \<rhd>- b \<turnstile> c2 : Tc" by auto
  ultimately show "A \<rhd>- if b then c1 else c2 end \<noteq> \<top>"
    using lHc2 "If\<^bsub>false\<^esub>" topset_inter_topD2 \<A>_If value_neq_exception result_true \<A>f_If 
    by (metis expression.inject(1) object_value.inject(2) value.inject(1))
next
  fix A :: "vname topset" and T c
  case ("Loop\<^bsub>true\<^esub>" e l m)
  moreover assume
    lHc: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHA: "A \<noteq> \<top>"
  then have "\<Gamma>, gfp (\<lambda>B. A \<sqinter> (B \<rhd>- e \<rhd> c)) \<turnstile> e : Attached" by auto
  moreover have "gfp (\<lambda>B. A \<sqinter> (B \<rhd>- e \<rhd> c)) \<le> A" by (meson gfp_least topset_inter_subset_iff) 
  ultimately have "\<Gamma>, A \<turnstile> e : Attached" using AT_mono by blast
  moreover have "A \<rhd> until e loop c end \<le> A \<rhd>+ e" by (rule attachment_loop_condition)
  moreover from lHA have "A \<rhd>+ e \<noteq> \<top>" using "Loop\<^bsub>true\<^esub>" calculation by blast
  ultimately show "A \<rhd> until e loop c end \<noteq> \<top>"by auto
  then show "A \<rhd>+ until e loop c end \<noteq> \<top>" by simp
  then show "A \<rhd>- until e loop c end \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T
  case ("Loop\<^bsub>false\<^esub>" e l m le me c lc mc c' l' m')
  moreover assume
    lHc': "c' \<noteq> Exception" and
    lHc: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately show "A \<rhd> until e loop c end \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T
  case ("Loop\<^bsub>false\<^esub>" e l m le me c lc mc c' l' m')
  moreover assume
    lHc': "c' = True\<^sub>c" and
    lHc: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately show "A \<rhd>+ until e loop c end \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T
  case ("Loop\<^bsub>false\<^esub>" e l m le me c lc mc c' l' m')
  moreover assume
    lHc': "c' = False\<^sub>c" and
    lHc: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHA: "A \<noteq> \<top>"
  ultimately show "A \<rhd>- until e loop c end \<noteq> \<top>" by simp
next
  fix A :: "vname topset" and T n
  case ("Test\<^bsub>true\<^esub>" e l m v l' m' t)
  moreover assume
    lHv: "v \<noteq> Void\<^sub>v \<and> v has_type t" and
    lHc: "\<Gamma>, A \<turnstile> attached t e as n : T" and
    lHA: "A \<noteq> \<top>"
  moreover then obtain Te where "\<Gamma>, A \<turnstile> e : Te" by blast
  ultimately have
    lHe: "A \<rhd> e \<noteq> \<top>" using value_neq_exception by blast
  from lHe show "A \<rhd> attached t e as n \<noteq> \<top>" by auto
  from lHe show "A \<rhd>+ attached t e as n \<noteq> \<top>" by (cases e) (simp_all add: topset_union_topI)
  from lHe show "A \<rhd>- attached t e as n \<noteq> \<top>" by (cases e) (simp_all add: topset_union_topI)
next
  fix A :: "vname topset" and T n
  case ("Test\<^bsub>false\<^esub>" e l m v l' m' t)
  moreover assume
    lHv: "\<not> (v \<noteq> Void\<^sub>v \<and> v has_type t)" and
    lHc: "\<Gamma>, A \<turnstile> attached t e as n : T" and
    lHA: "A \<noteq> \<top>"
  moreover then obtain Te where "\<Gamma>, A \<turnstile> e : Te" by blast
  ultimately have
    lHe: "A \<rhd> e \<noteq> \<top>" using value_neq_exception by blast
  from lHe show "A \<rhd> attached t e as n \<noteq> \<top>" by auto
  from lHe show "A \<rhd>+ attached t e as n \<noteq> \<top>" by (cases e) (simp_all add: topset_union_topI)
  from lHe show "A \<rhd>- attached t e as n \<noteq> \<top>" by (cases e) (simp_all add: topset_union_topI)
qed auto

lemma
	  reachability_preservation': "
  		\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
  		e' \<noteq> Exception \<Longrightarrow>
  		A \<noteq> \<top> \<Longrightarrow>
  		A \<rhd> e \<noteq> \<top>" and
  	reachability_preservation_scope_true': "
  		\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
  		e' = True\<^sub>c \<Longrightarrow>
  		A \<noteq> \<top> \<Longrightarrow>
  		A \<rhd>+ e \<noteq> \<top>" and
  	reachability_preservation_scope_false': "
  		\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
  		e' = False\<^sub>c \<Longrightarrow>
  		A \<noteq> \<top> \<Longrightarrow>
  		A \<rhd>- e \<noteq> \<top>" and
	  reachability_preservation_es': "
  		\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle> \<Longrightarrow>
  		\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
  		Exception \<notin> set es' \<Longrightarrow>
  		A \<noteq> \<top> \<Longrightarrow>
  		A \<rhd>\<rhd> es \<noteq> \<top>"
using reachability_preservation apply (metis "prod.collapse")
using reachability_preservation_scope_true apply (metis "prod.collapse")
using reachability_preservation_scope_false apply (metis "prod.collapse")
using reachability_preservation_es apply (metis "prod.collapse")
done

lemma exception_detection:
	assumes
		HS: "\<Gamma> \<turnstile> \<langle>c, (l, m)\<rangle> \<Rightarrow> \<langle>c', (l', m')\<rangle>" and
		HT: "\<Gamma>, A \<turnstile> c : T" and
    HD: "A \<noteq> \<top>" and
    HE: " A \<rhd> c = \<top>"
  shows
    "c' = Exception"
using reachability_preservation assms by fastforce

lemma local_attachment_state_if1:
  assumes
    HV: "valid_local_attachment_state (A \<rhd>+ c \<rhd> e\<^sub>1) l" and
    HD: "A \<rhd>+ c \<rhd> e\<^sub>1 \<noteq> \<top>"
  shows "valid_local_attachment_state (A \<rhd> if c then e\<^sub>1 else e\<^sub>2 end) l"
using assms topset_inter_lower1 by (metis \<A>_If local_attachment_state_anti_mono)

lemma local_attachment_state_if2:
  assumes
    HV: "valid_local_attachment_state (A \<rhd>- c \<rhd> e\<^sub>2) l" and
    HD: "A \<rhd>- c \<rhd> e\<^sub>2 \<noteq> \<top>"
  shows "valid_local_attachment_state (A \<rhd> if c then e\<^sub>1 else e\<^sub>2 end) l"
using assms topset_inter_lower2 by (metis \<A>_If local_attachment_state_anti_mono)

lemma valid_local_attachment_state_preservation'':
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	assumes
		HV: "valid_local_attachment_state A l" and
		HD: "A \<noteq> \<top>"
	shows
    "
	    \<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
    	\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
    	e' = \<C> w \<Longrightarrow>
    	T = Attached \<Longrightarrow>
    	w \<noteq> Void\<^sub>v" and
    "
	    \<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
    	\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
    	e' = \<C> w \<Longrightarrow>
	    valid_local_attachment_state (A \<rhd> e) l'" and
    "
	    \<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
    	\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
  	  e' = True\<^sub>c \<Longrightarrow> 
  	  valid_local_attachment_state (A \<rhd>+ e) l'" and
    "
	    \<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
    	\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
	    e' = False\<^sub>c \<Longrightarrow>
	    valid_local_attachment_state (A \<rhd>- e) l'" and
    "
	    \<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow>
    	\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
    	es' = map Value ws \<Longrightarrow>
    	Detachable \<notin> set Ts \<Longrightarrow>
    	Void\<^sub>v \<notin> set ws" and
    "
	    \<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow>
    	\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
    	es' = map Value ws \<Longrightarrow>
	    valid_local_attachment_state (A \<rhd>\<rhd> es) l'" and
    "\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow> True" and
    "\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow> True"
using assms
proof (induction arbitrary: A T w and A Ts ws rule: big_step_induct)
  fix A T w
  case (Value v l m)
  assume
    lHt: "\<Gamma>, A \<turnstile> \<C> v : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "(\<C> v) = (\<C> w)"
  from lHv show "valid_local_attachment_state (A \<rhd> \<C> v) l" by simp
  then show "valid_local_attachment_state (A \<rhd>+ \<C> v) l" by simp
  then show "valid_local_attachment_state (A \<rhd>- \<C> v) l" by simp
  assume
  	"T = Attached"
  with lHw lHt show "w \<noteq> Void\<^sub>v" by auto
next
  fix A T w
  case (Local l n v m)
  assume
    lHv: "l n = \<lfloor>v\<rfloor>" and
    lHT: "\<Gamma>, A \<turnstile> \<V> n : T" and
    lHV: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "(\<C> v) = (\<C> w)"
  then show "valid_local_attachment_state (A \<rhd> \<V> n) l" by simp
  then show "valid_local_attachment_state (A \<rhd>+ \<V> n) l" by simp
  then show "valid_local_attachment_state (A \<rhd>- \<V> n) l" by simp
  assume
  	"T = Attached"
  with lHA lHw lHT lHv lHV show "w \<noteq> Void\<^sub>v" using valid_local_attachment_state_def
	 	ExpressionValidity.LocalE
	 	by (metis attachment_type.distinct(1) expression.inject(1) option.inject )
next
  fix A T w
  case (Seq c\<^sub>1 l m l1 m1 c\<^sub>2 c\<^sub>2' l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> c\<^sub>1 ;; c\<^sub>2 : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c\<^sub>2' = \<C> w"
  from lHT lHv lHA lHw show
  	lHS: "valid_local_attachment_state (A \<rhd> c\<^sub>1 ;; c\<^sub>2) l'"
    	using Seq.IH(2) Seq.IH(6) Seq.hyps(1) value_neq_exception exception_detection by fastforce
  from lHS show "valid_local_attachment_state (A \<rhd>+ c\<^sub>1 ;; c\<^sub>2) l'" by simp
  from lHS show "valid_local_attachment_state (A \<rhd>- c\<^sub>1 ;; c\<^sub>2) l'" by simp
  assume
  	"T = Attached"
  with Seq.hyps Seq.IH lHT lHv lHA lHw show "w \<noteq> Void\<^sub>v"
    using exception_detection by fastforce
next
  fix A T and w :: "'b value" and  n
  case (Assign e l m v le me)
  assume
    lHT: "\<Gamma>, A \<turnstile> n ::= e : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "unit = \<C> w"  
  from lHT lHv lHA show "valid_local_attachment_state (A \<rhd> n ::= e) (le(n \<mapsto> v))"
    using attached_assignment_set detachable_assignment_set
      local_attachment_state_add local_attachment_state_sub
      by (metis (full_types) Assign.IH(1) Assign.IH(2) ExpressionValidity.AssignE
          attachment_type.exhaust)
  then show "valid_local_attachment_state (A \<rhd>+ n ::= e) (le(n \<mapsto> v))" by simp
  then show "valid_local_attachment_state (A \<rhd>- n ::= e) (le(n \<mapsto> v))" by simp
  assume
  	"T = Attached"
  from lHw show "w \<noteq> Void\<^sub>v" by simp
next
  fix A T and w :: "'b value" and l :: "'b local_state"
  case (Create n Tn m m' v)
  assume
    lHT: "\<Gamma>, A \<turnstile> create n : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "unit = \<C> w"
  from lHv Create.hyps show "valid_local_attachment_state (A \<rhd> create n) (l(n \<mapsto> v))"
    using local_attachment_state_add \<A>_Create instance_is_not_void by metis
  then show "valid_local_attachment_state (A \<rhd>+ create n) (l(n \<mapsto> v))" by simp
  then show "valid_local_attachment_state (A \<rhd>- create n) (l(n \<mapsto> v))" by simp
  assume
  	"T = Attached"
  from lHw show "w \<noteq> Void\<^sub>v" by simp
next
  fix A T and w :: "'b value" and l :: "'b local_state"
  case ("Create\<^bsub>fail\<^esub>" n Tn m)
  assume
    lHT: "\<Gamma>, A \<turnstile> create n : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "Exception = \<C> w"
  then show "w \<noteq> Void\<^sub>v" by simp
  from lHv lHw show " valid_local_attachment_state (A \<rhd> create n) l" by simp
  then show "valid_local_attachment_state (A \<rhd>+ create n) l" by simp
  then show "valid_local_attachment_state (A \<rhd>- create n) l" by simp
next
  fix A T and w :: "'b value" and  f
  case (Call e l m v le me es vs l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> e \<^bsub>\<bullet>\<^esub> f (es) : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "unit = \<C> w"
  then show "w \<noteq> Void\<^sub>v" by blast
  from lHT lHv lHA Call.IH show "valid_local_attachment_state (A \<rhd> e \<^bsub>\<bullet>\<^esub> f (es)) l'"
  	using \<A>_Call \<A>_to_all local_attachment_state_top by (metis ExpressionValidity.CallE)
  then show "valid_local_attachment_state (A \<rhd>+ e \<^bsub>\<bullet>\<^esub> f (es)) l'" by simp
  then show "valid_local_attachment_state (A \<rhd>- e \<^bsub>\<bullet>\<^esub> f (es)) l'" by simp
next
  fix A T w c2
  case H: ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l1 m1)
  assume
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c1' = \<C> w"
  from lHT lHv lHA lHw H.hyps H.IH show "valid_local_attachment_state (A \<rhd> if b then c1 else c2 end) l1"
    using value_neq_exception exception_detection
      local_attachment_state_if1 reachability_preservation_scope_true
      by (metis ExpressionValidity.IfE)
	assume
		"T = Attached"
	with H lHT lHv lHA lHw show "w \<noteq> Void\<^sub>v"
		using reachability_preservation_scope_true upper_bound_bot_left' by (metis ExpressionValidity.IfE) 
next
  fix A T w c2
  case ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l1 m1)
  moreover assume
    lH1: "c1' = True\<^sub>c" and
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c1' = \<C> w"
  then have "valid_local_attachment_state (A \<rhd> if b then c1 else c2 end) l1"
    using value_neq_exception exception_detection
      local_attachment_state_if1 reachability_preservation_scope_true
      by (metis "If\<^bsub>true\<^esub>.IH"(3) "If\<^bsub>true\<^esub>.IH"(6) "If\<^bsub>true\<^esub>.hyps"(1) "If\<^bsub>true\<^esub>.hyps"(2)
          ExpressionValidity.IfE)
  moreover from lH1 have "\<not> is_false c1" using "If\<^bsub>true\<^esub>.hyps" result_false by blast
  ultimately show "valid_local_attachment_state (A \<rhd>+ if b then c1 else c2 end) l1"
    using "If\<^bsub>true\<^esub>.IH" lH1 lHT lHv by fastforce
next
  fix A T w c2
  case ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l1 m1)
  moreover assume
    lH1: "c1' = False\<^sub>c" and
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c1' = \<C> w"
  then have "valid_local_attachment_state (A \<rhd> if b then c1 else c2 end) l1"
    using value_neq_exception exception_detection
      local_attachment_state_if1 reachability_preservation_scope_true
      by (metis "If\<^bsub>true\<^esub>.IH"(3) "If\<^bsub>true\<^esub>.IH"(6) "If\<^bsub>true\<^esub>.hyps"(1) "If\<^bsub>true\<^esub>.hyps"(2)
          ExpressionValidity.IfE)
  moreover from lH1 have "\<not> is_true c1" using "If\<^bsub>true\<^esub>.hyps" result_true by blast
  ultimately show "valid_local_attachment_state (A \<rhd>- if b then c1 else c2 end) l1"
    using "If\<^bsub>true\<^esub>.IH" lH1 lHT lHv by fastforce
next
  fix A T w c1
  case H: ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l2 m2)
  assume
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c2' = \<C> w"
  from lHT lHv lHA lHw H show "valid_local_attachment_state (A \<rhd> if b then c1 else c2 end) l2"
    using value_neq_exception exception_detection
      local_attachment_state_if2 reachability_preservation_scope_false
      by (metis ExpressionValidity.IfE)
	assume
		"T = Attached"
	with lHT lHv lHA lHw H show "w \<noteq> Void\<^sub>v"
    using reachability_preservation_scope_false upper_bound_bot_right'
    by (metis ExpressionValidity.IfE)
next
  fix A T w c1
  case ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l2 m2)
  moreover assume
    lH2: "c2' = True\<^sub>c" and
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c2' = \<C> w"
  from lHT lHv lHA lHw have "valid_local_attachment_state (A \<rhd> if b then c1 else c2 end) l2"
    using value_neq_exception exception_detection
      local_attachment_state_if2 reachability_preservation_scope_false
      by (metis "If\<^bsub>false\<^esub>.IH"(4) "If\<^bsub>false\<^esub>.IH"(6) "If\<^bsub>false\<^esub>.hyps"(1) "If\<^bsub>false\<^esub>.hyps"(2)
        ExpressionValidity.IfE)
  moreover from lH2 have "\<not> is_false c2" using "If\<^bsub>false\<^esub>.hyps" result_false by blast
  ultimately show "valid_local_attachment_state (A \<rhd>+ if b then c1 else c2 end) l2"
    using "If\<^bsub>false\<^esub>.IH" lH2 lHT lHv by fastforce
next
  fix A T w c1
  case ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l2 m2)
  moreover assume
    lH2: "c2' = False\<^sub>c" and
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c2' = \<C> w"
  from lHT lHv lHA lHw have "valid_local_attachment_state (A \<rhd> if b then c1 else c2 end) l2"
    using value_neq_exception exception_detection
      local_attachment_state_if2 reachability_preservation_scope_false
      by (metis "If\<^bsub>false\<^esub>.IH"(4) "If\<^bsub>false\<^esub>.IH"(6) "If\<^bsub>false\<^esub>.hyps"(1) "If\<^bsub>false\<^esub>.hyps"(2)
        ExpressionValidity.IfE)
  moreover from lH2 have "\<not> is_true c2" using "If\<^bsub>false\<^esub>.hyps" result_true by blast
  ultimately show "valid_local_attachment_state (A \<rhd>- if b then c1 else c2 end) l2"
    using "If\<^bsub>false\<^esub>.IH" lH2 lHT lHv by fastforce
next
  fix A T and w :: "'b value" and  c
  case ("Loop\<^bsub>true\<^esub>" e l m le me)
  assume
    lHT: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "unit = \<C> w"
	then show "w \<noteq> Void\<^sub>v" by simp
  from lHT have "\<Gamma>, gfp (\<lambda> B. A \<sqinter> B \<rhd>- e \<rhd> c) \<turnstile> e: Attached" by blast
  moreover have "gfp (\<lambda> B. A \<sqinter> B \<rhd>- e \<rhd> c) \<le> A" by (simp add: topset_gfp_inter_left)
  ultimately have "\<exists> Tb. \<Gamma>, A \<turnstile> e: Tb \<and> Tb \<rightarrow>\<^sub>a Attached" using AT_mono by metis
  then have
    lHe: "\<Gamma>, A \<turnstile> e: Attached" using conforms_to_attached by blast
  with "Loop\<^bsub>true\<^esub>.IH" lHA lHv have "valid_local_attachment_state (A \<rhd>+ e) le" by simp
  moreover from lHA "Loop\<^bsub>true\<^esub>.hyps" lHe have "A \<rhd>+ e \<noteq> \<top>"
    by (simp add: reachability_preservation_scope_true)
  moreover have "A \<rhd> until e loop c end \<le> A \<rhd>+ e" by (rule attachment_loop_condition)
  ultimately show "valid_local_attachment_state (A \<rhd> until e loop c end) le"
    by (simp add: local_attachment_state_anti_mono)
  then show "valid_local_attachment_state (A \<rhd>+ until e loop c end) le" by simp
  then show "valid_local_attachment_state (A \<rhd>- until e loop c end) le" by simp
next
  fix A T w
  case ("Loop\<^bsub>false\<^esub>" e l m le me c lc mc c' l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "c' = \<C> w"
  obtain sc s' f where
    "sc = (lc, mc)" and "s' = (l', m')" and
    lHf: "f = until e loop c end"
    by simp
  with "Loop\<^bsub>false\<^esub>.hyps" have
  	lHB: "\<Gamma> \<turnstile> \<langle>f, sc\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>" by simp
  from lHT have "\<Gamma>, gfp (\<lambda> B. A \<sqinter> B \<rhd>- e \<rhd> c) \<turnstile> e: Attached" by auto
  moreover have "gfp (\<lambda> B. A \<sqinter> B \<rhd>- e \<rhd> c) \<le> A" by (simp add: topset_gfp_inter_left)
  ultimately have "\<exists> Tb. \<Gamma>, A \<turnstile> e: Tb \<and> Tb \<rightarrow>\<^sub>a Attached" using AT_mono by metis
  then have
    lHTe: "\<Gamma>, A \<turnstile> e: Attached" using conforms_to_attached by blast
  with "Loop\<^bsub>false\<^esub>.IH" lHv lHA have
    lHVe: "valid_local_attachment_state (A \<rhd>- e) le" by simp
  moreover from lHA "Loop\<^bsub>false\<^esub>.hyps" lHTe have
    lHDe: "A \<rhd>- e \<noteq> \<top>" by (simp add: reachability_preservation_scope_false)
  from lHT have "\<Gamma>, (gfp (\<lambda>B. A \<sqinter> (B \<rhd>- e \<rhd> c)) \<rhd>- e) \<turnstile> c : Attached" by auto
  moreover have "(gfp (\<lambda>B. A \<sqinter> (B \<rhd>- e \<rhd> c)) \<rhd>- e) \<le> A \<rhd>- e"
    by (simp add: topset_gfp_inter_left \<A>f_mono')
  ultimately have "\<exists> Tb. \<Gamma>, (A \<rhd>- e) \<turnstile> c: Tb \<and> Tb \<rightarrow>\<^sub>a Attached" using AT_mono by metis
  then have
    lHTc: "\<Gamma>, (A \<rhd>- e) \<turnstile> c: Attached" using conforms_to_attached by blast
  with "Loop\<^bsub>false\<^esub>.IH" lHDe lHVe have "valid_local_attachment_state (A \<rhd>- e \<rhd> c) lc"
    by simp
  moreover from "Loop\<^bsub>false\<^esub>.hyps" lHDe lHTc have
    lHDc: "A \<rhd>- e \<rhd> c \<noteq> \<top>" by (simp add: reachability_preservation)
  moreover from lHT have "T = Attached" by auto
    with lHT have
      lHTc: "\<Gamma>, A \<rhd>- e \<rhd> c \<turnstile> until e loop c end: Attached"
        using AT_loop_step by metis
  ultimately have
    lHVc: "valid_local_attachment_state (A \<rhd>- e \<rhd> c \<rhd> until e loop c end) l'"
      using "Loop\<^bsub>false\<^esub>.IH" lHw by simp
  from lHw have "c' \<noteq> Exception" by simp
  from "Loop\<^bsub>false\<^esub>.hyps" lHTc lHDc this have
    lHDl: "A \<rhd>- e \<rhd> c \<rhd> until e loop c end \<noteq> \<top>" using reachability_preservation(1)
      by metis
  have "A \<rhd> until e loop c end \<le> A \<rhd>- e \<rhd> c \<rhd> until e loop c end" by (rule loop_application1)
  from this lHDl lHVc show "valid_local_attachment_state (A \<rhd> until e loop c end) l'"
    using local_attachment_state_anti_mono by blast
  then show "valid_local_attachment_state (A \<rhd>+ until e loop c end) l'" by simp
  then show "valid_local_attachment_state (A \<rhd>- until e loop c end) l'" by simp
  assume
  	"T = Attached"
 	moreover have
 		"\<Gamma> \<turnstile> \<langle>f, sc\<rangle> \<Rightarrow> \<langle>c', s'\<rangle> \<Longrightarrow> f = until e loop c end \<Longrightarrow> c' = \<C> w \<Longrightarrow> w = Unit"
 		"\<Gamma> \<turnstile> \<langle>es, sc\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle> \<Longrightarrow> True" by (induction rule: "big_step_big_steps.inducts") auto
  moreover note lHf lHw lHB
  ultimately have "w = Unit" by simp
  then show "w \<noteq> Void\<^sub>v" by simp
next
  fix A T and w :: "'b value" and n
  case ("Test\<^bsub>true\<^esub>" e l m v le me t)
  assume
    lHa: "v \<noteq> Void\<^sub>v \<and> v has_type t" and
    lHT: "\<Gamma>, A \<turnstile> attached t e as n : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "True\<^sub>c = \<C> w"
  then show "w \<noteq> Void\<^sub>v" by auto
  from  lHA lHT lHa lHv show
    lHVc: "valid_local_attachment_state (A \<rhd> attached t e as n) (le(n \<mapsto> v))"
      using "Test\<^bsub>true\<^esub>.IH" local_attachment_state_upd by fastforce
  then have
    lHVe: "valid_local_attachment_state (A \<rhd> e) le" using "Test\<^bsub>true\<^esub>.IH" lHA lHT lHv by auto 
  moreover from lHa have
    lHa': "is_attached v" by simp
  ultimately have
    lHVn: "valid_local_attachment_state (A \<rhd> e \<oplus> n) (le(n \<mapsto> v))"
      using local_attachment_state_add by auto
  show "valid_local_attachment_state (A \<rhd>+ attached t e as n) (le(n \<mapsto> v))"
  proof (cases "\<exists> x. e = \<V> x")
    case True
    then obtain x where "e = \<V> x" by blast
    then have "A \<rhd>+ attached t e as n = A \<rhd> e \<oplus> x \<oplus> n" by simp
    then have "A \<rhd>+ attached t e as n = A \<rhd> e \<oplus> n \<oplus> x" by simp
    moreover have "le x = \<lfloor>v\<rfloor>" using "Test\<^bsub>true\<^esub>.hyps"(1) \<open>e = \<V> x\<close> by blast 
    then have "(le (n \<mapsto> v)) x = \<lfloor>v\<rfloor>"by simp
    moreover from lHa' have "v \<noteq> Void\<^sub>v" by simp
    moreover note lHVn
    ultimately show ?thesis using local_attachment_state_add
      by (metis fun_upd_triv)
  next
    case False
    then have "A \<rhd>+ attached t e as n = A \<rhd> e \<oplus> n" by (cases e) simp_all
    then show ?thesis using lHVn by simp
  qed
  from lHVc show "valid_local_attachment_state (A \<rhd>- attached t e as n) (le(n \<mapsto> v))" by simp
next
  fix A T and w :: "'b value" and n
  case ("Test\<^bsub>false\<^esub>" e l m v le me t)
  assume
    lHa: "\<not> (v \<noteq> Void\<^sub>v \<and> v has_type t)" and
    lHT: "\<Gamma>, A \<turnstile> attached t e as n : T" and
    lHv: "valid_local_attachment_state A l" and
    lHA: "A \<noteq> \<top>" and
    lHw: "False\<^sub>c = \<C> w"
  then show "w \<noteq> Void\<^sub>v" by auto
  from lHA lHT lHv show
    lHVc: "valid_local_attachment_state (A \<rhd> attached t e as n) le"
      using "Test\<^bsub>false\<^esub>.IH"(2) by auto
  then have "valid_local_attachment_state (A \<rhd> e) le" by simp
  from lHVc show "valid_local_attachment_state (A \<rhd>- attached t e as n) le" by simp
  assume
    "False\<^sub>c = True\<^sub>c"
  then show "valid_local_attachment_state (A \<rhd>+ attached t e as n) le" by simp
next
  fix A Ts and ws :: "'b value list" 
	case (Nil l m)
	assume
		lHV: "valid_local_attachment_state A l" and
		lHA: "A \<noteq> \<top>"
	show True by simp
	show True by simp
	assume
		lHT: "\<Gamma>, A \<turnstile> [] [:] Ts" and
		lHw: "[] = map Value ws"
	from lHV show "valid_local_attachment_state (A \<rhd>\<rhd> []) l"  by simp
	assume
		"Detachable \<notin> set Ts"
	from lHw show "Void\<^sub>v \<notin> set ws" by simp
next
  fix A Ts ws
	case (Cons e l m v le me es es' l' m')
	assume
		lHV: "valid_local_attachment_state A l" and
		lHA: "A \<noteq> \<top>"
	show True by simp
	show True by simp
	assume
		lHT: "\<Gamma>, A \<turnstile> e # es [:] Ts" and
		lHw: "(Value v) # es' = map Value ws"
	from lHT obtain T' Ts' where
		lHeT: "\<Gamma>, A \<turnstile> e : T'" and
		lHesT:"\<Gamma>, A \<rhd> e \<turnstile> es [:] Ts'" and
		lHTc: "Ts = T' # Ts'"
		using list_attachment_validity_tail by blast
	moreover from lHw have
		lHesv: "es' = map Value (tl ws)" by auto
	moreover from Cons.IH lHA lHV lHeT have
		lHeV: "valid_local_attachment_state (A \<rhd> e) le" by auto
	moreover from Cons.hyps lHA lHeT have
		lHAe: "A \<rhd> e \<noteq> \<top>" using reachability_preservation by fastforce
	moreover note Cons.IH lHesT
	ultimately show "valid_local_attachment_state (A \<rhd>\<rhd> (e # es)) l'" by auto
	from lHw have
		"v = hd ws" by auto
	moreover assume
		lHTsD: "Detachable \<notin> set Ts"
	with lHT lHTc have
		lHeD: "T' = Attached" using "attachment_type.exhaust" by auto
	moreover note Cons.IH lHA lHV lHeT
	ultimately have
		lHwh: "hd ws \<noteq> Void\<^sub>v" by blast
	from lHTsD lHTc have
		lHesD: "Detachable \<notin> set Ts'" by simp
	with Cons.IH lHesT lHesv lHeV lHAe have "Void\<^sub>v \<notin> set (tl ws)" by simp
	with lHwh lHw show "Void\<^sub>v \<notin> set ws" using hd_Cons_tl Nil_is_map_conv by fastforce
qed auto


(*
lemma valid_local_attachment_state_preservation':
	assumes
		HS: "\<Gamma> \<turnstile> \<langle>c, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>" and
		HT: "\<Gamma>, A \<turnstile> c : T" and
    HV: "valid_local_attachment_state A l" and
    HD: "A \<noteq> \<top>"
  shows
    "(T = Attached \<longrightarrow> is_attached_value v) \<and> valid_local_attachment_state (\<A> c A) l'"
using assms
proof (induction c arbitrary: l m v l' m' A T)
  case (Value x)
  then show ?case by auto
next
  case (Local n l m v l' m' A)
  then have "l n = \<lfloor>v\<rfloor>" by auto
  from Local have "n \<in>\<^sup>\<top> A" if "T = Attached" using that by auto
  with Local have "l n = \<lfloor>v\<rfloor>" and "is_attached_value v" if "T = Attached" 
    using Local.prems valid_local_attachment_state_def that by auto
  moreover from Local.prems have "l = l'" by blast
  with Local.prems have "valid_local_attachment_state (A \<rhd> \<V> n) l'" by auto
  ultimately show ?case by simp
next
  case (Sequence c1 c2)
  then obtain l1 m1 where
    lHS1: "\<Gamma> \<turnstile> \<langle>c1, (l, m)\<rangle> \<Rightarrow> \<langle>unit, (l1, m1)\<rangle>" and
    lHS2: "\<Gamma> \<turnstile> \<langle>c2, (l1, m1)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>" by auto
  from Sequence have
    lHT1: "\<Gamma>, A \<turnstile> c1 : Attached" and
    lHT2: "\<Gamma>, (A \<rhd> c1) \<turnstile> c2 : Attached" by auto
  with lHS1 Sequence have
    lHD: "A \<rhd> c1 \<noteq> \<top>" using reachability_preservation by fastforce
  with lHS2 lHT2 HD Sequence have "is_attached_value v" using lHS1 lHT1 by blast
  moreover with lHS1 lHS2 lHT1 lHT2 lHD Sequence have "valid_local_attachment_state (A \<rhd> c1 \<rhd> c2) l'"
    by blast
  ultimately show ?case by simp
next
  case (Assignment n e l m v l' m' A T)
  then obtain ve le where
    lHS: "\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> ve, (le, m')\<rangle>" and
    lHU: "le (n \<mapsto> ve) = l'" by auto
  from Assignment obtain Te where
    lHT: "\<Gamma>, A \<turnstile> e : Te" by auto
  from lHS lHT Assignment have "valid_local_attachment_state (A \<rhd> e) le" by simp
  with lHS lHU Assignment have "valid_local_attachment_state (A \<rhd> n ::= e) l'"
    by (metis (full_types) AttachmentStatus.AssignE attached_assignment_set
      attachment_type.exhaust detachable_assignment_set local_attachment_state_add
      local_attachment_state_sub)
  moreover from Assignment have "is_attached_value v" by auto
  ultimately show ?case by simp
next
  case (Creation n l m v l' m' A T)
  then obtain Tn cv where
    "\<Gamma> n = \<lfloor>Tn\<rfloor>" and
    "BigStep.instance m Tn = \<lfloor>(m', cv)\<rfloor>" and
    lHU: "l' = l (n \<mapsto> cv)" by auto
  then have "is_attached_value cv" using instance_is_attached by (metis same_instance)
  with lHU Creation have "valid_local_attachment_state (A \<rhd> create n) l'"
    by (simp add: local_attachment_state_add)
  moreover from Creation have "is_attached_value v" by auto
  ultimately show ?case by simp
next
  case (Call e f l m v l' m' A T)
  then show ?case by auto
next
  case (If b c1 c2 l m v l' m' A T)
  then obtain lb mb where
    lHS: "\<Gamma> \<turnstile> \<langle>b, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> True, (lb, mb)\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c1, (lb, mb)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle> \<or>
          \<Gamma> \<turnstile> \<langle>b, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> False, (lb, mb)\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c2, (lb, mb)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>" by auto
  with If have
    lHV: "valid_local_attachment_state (A \<rhd> b) lb" by auto
  from If.prems have
    lHD: "A \<rhd> b \<noteq> \<top>" using reachability_preservation by fastforce
  from If obtain T1 T2 where
    lHB: "\<Gamma>, A \<turnstile> b: Attached" and
    lHT1: "\<Gamma>, \<A> b A \<turnstile> c1: T1" and
    lHT2: "\<Gamma>, \<A> b A \<turnstile> c2: T2" and
    lHT: "T = upper_bound T1 T2" by auto
  from lHS show ?case
  proof
    assume "\<Gamma> \<turnstile> \<langle>b, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> True, (lb, mb)\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c1, (lb, mb)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>"
    then have
      lHS1: "\<Gamma> \<turnstile> \<langle>c1, (lb, mb)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>" by rule
    with lHB lHT1 lHV lHD If.IH have
      "(T1 = Attached \<longrightarrow> is_attached_value v) \<and>
      valid_local_attachment_state (A \<rhd> b \<rhd> c1) l'" by auto
    with lHT have
      "(T = Attached \<longrightarrow> is_attached_value v) \<and>
      valid_local_attachment_state (A \<rhd> b \<rhd> c1) l'" using upper_bound_bot_left' by meson
    moreover from lHD lHS1 lHT1 have "A \<rhd> b \<rhd> c1 \<noteq> \<top>" using reachability_preservation by fastforce
    ultimately show ?thesis using local_attachment_state_if1 by blast
  next
    assume "\<Gamma> \<turnstile> \<langle>b, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> False, (lb, mb)\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c2, (lb, mb)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>"
    then have
      lHS1: "\<Gamma> \<turnstile> \<langle>c2, (lb, mb)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>" by rule
    with lHB lHT2 lHV lHD If.IH have
      "(T2 = Attached \<longrightarrow> is_attached_value v) \<and>
      valid_local_attachment_state (A \<rhd> b \<rhd> c2) l'" by auto
    with lHT have
      "(T = Attached \<longrightarrow> is_attached_value v) \<and>
      valid_local_attachment_state (A \<rhd> b \<rhd> c2) l'" using upper_bound_bot_right' by meson
    moreover from lHD lHS1 lHT2 have "A \<rhd> b \<rhd> c2 \<noteq> \<top>" using reachability_preservation by fastforce
    ultimately show ?thesis using local_attachment_state_if2 by blast
  qed
next
  case (Loop e c l m v l' m' A T)
  obtain s s' d f where
    "s = (l, m)" and "s' = (l', m')" and
    lHd: "d = \<C> v" and
    lHf: "f = until e loop c end"
    by simp
  with "Loop.prems" have "\<Gamma> \<turnstile> \<langle>f, s\<rangle> \<Rightarrow> \<langle>d, s'\<rangle>" by simp
  from this lHf lHd have "v = Unit"
    by (induction rule: big_step.induct) simp_all
  hence "is_attached_value v" by simp
  moreover have "valid_local_attachment_state (A \<rhd> until e loop c end) l'"
  ultimately show ?case by simp
next
  case Exception then show ?case by auto
qed
*)

lemma valid_local_attachment_state_preservation:
	assumes
		HS: "\<Gamma> \<turnstile> \<langle>c, (l, m)\<rangle> \<Rightarrow> \<langle>c', (l', m')\<rangle>" and
		HT: "\<Gamma>, A \<turnstile> c : T" and
    HV: "valid_local_attachment_state A l" and
    HE: "c' \<noteq> Exception"
  shows
    "valid_local_attachment_state (\<A> c A) l'"
proof (cases A)
  case Top thus ?thesis by (simp add: valid_local_attachment_state_def)
next
  case (Set a)
  from HS have "Final \<Gamma> c' (l', m')" by (simp add: big_step_final)
  then obtain v where "c' = (\<C> v) \<or> c' = Exception" by (auto simp add: Final_def)
  hence "c' = \<C> v" by (simp add: HE)
  with HS HT HV Set show ?thesis using valid_local_attachment_state_preservation''(2) by fastforce
qed

lemma local_attachment_type_preservation:
	assumes
		HS: "\<Gamma> \<turnstile> \<langle>c, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> v, (l', m')\<rangle>" and
		HT: "\<Gamma>, A \<turnstile> c : Attached" and
    HV: "valid_local_attachment_state A l" and
    HD: "A \<noteq> \<top>"
	shows "v \<noteq> Void\<^sub>v"
using assms valid_local_attachment_state_preservation''(1) by fastforce

lemma
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	assumes
		"\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
  shows
    valid_state_preservation: "
			\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
			\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
			e' = \<C> w \<Longrightarrow>
			\<Gamma>, (A \<rhd> e) \<turnstile> (l', m') \<surd>\<^sub>s" and
    valid_state_preservation_scope_true: "
			\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
			\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
    	e' = True\<^sub>c \<Longrightarrow>
    	\<Gamma>, (A \<rhd>+ e) \<turnstile> (l', m') \<surd>\<^sub>s" and
    valid_state_preservation_scope_false: "
			\<Gamma> \<turnstile> \<langle>e, (l, m)\<rangle> \<Rightarrow> \<langle>e', (l', m')\<rangle> \<Longrightarrow>
			\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
    	e' = False\<^sub>c \<Longrightarrow>
    	\<Gamma>, (A \<rhd>- e) \<turnstile> (l', m') \<surd>\<^sub>s" and
    valid_state_preservation_es: "
			\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow>
			\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
			es' = map Value ws \<Longrightarrow>
			\<Gamma>, A \<rhd>\<rhd> es \<turnstile> (l', m') \<surd>\<^sub>s" and
    "
			\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow>
			\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
			es' = map Value ws \<Longrightarrow>
			\<Gamma>, A \<rhd>\<rhd> es \<turnstile> (l', m') \<surd>\<^sub>s" and
    "
			\<Gamma> \<turnstile> \<langle>es, (l, m)\<rangle> [\<Rightarrow>] \<langle>es', (l', m')\<rangle> \<Longrightarrow>
			\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
			es' = map Value ws \<Longrightarrow>
			\<Gamma>, A \<rhd>\<rhd> es \<turnstile> (l', m') \<surd>\<^sub>s"
using assms
proof (induction arbitrary: A T w and A Ts ws rule: big_step_induct)
  fix A T w
  case (Value v l m)
  assume
    "\<Gamma>, A \<turnstile> \<C> v : T"
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
    "(\<C> v) = (\<C> w)"
  then show "\<Gamma>, A \<rhd> \<C> v \<turnstile> (l, m) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>+ \<C> v \<turnstile> (l, m) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- \<C> v \<turnstile> (l, m) \<surd>\<^sub>s" by simp
next
  fix A T w
  case (Local l n v m)
  assume
    "l n = \<lfloor>v\<rfloor>"
    "\<Gamma>, A \<turnstile> \<V> n : T"
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
    "(\<C> v) = (\<C> w)"
  then show "\<Gamma>, A \<rhd> \<V> n \<turnstile> (l, m) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>+ \<V> n \<turnstile> (l, m) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- \<V> n \<turnstile> (l, m) \<surd>\<^sub>s" by simp
next
  fix A T w
  case (Seq c1 l m l1 m1 c2 c2' l' m')
  assume
    "\<Gamma>, A \<turnstile> c1 ;; c2 : T"
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
    "c2' = \<C> w"
  then show
  	S: "\<Gamma>, A \<rhd> c1 ;; c2 \<turnstile> (l', m') \<surd>\<^sub>s" using TransferFunction.\<A>_Seq Seq.IH by auto 
  from S show "\<Gamma>, A \<rhd>+ c1 ;; c2 \<turnstile> (l', m') \<surd>\<^sub>s" by simp
  from S show "\<Gamma>, A \<rhd>- c1 ;; c2 \<turnstile> (l', m') \<surd>\<^sub>s" by simp
next
  fix A T and w :: "'b value" and n
  case (Assign e l m v le me)
  moreover assume
    "\<Gamma>, A \<turnstile> n ::= e : T"
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
    "unit = \<C> w"
  ultimately show "\<Gamma>, A \<rhd> n ::= e \<turnstile> (le(n \<mapsto> v), me) \<surd>\<^sub>s"
    using \<A>_to_all
      attached_assignment_set detachable_assignment_set
      attachment_set_add attachment_set_sub
      valid_stateD1 valid_stateD2 valid_stateI
      local_state_upd valid_local_attachment_state_def
      local_attachment_type_preservation
    by (metis (full_types) ExpressionValidity.AssignE attachment_type.exhaust) 
  then show "\<Gamma>, A \<rhd>+ n ::= e \<turnstile> (le(n \<mapsto> v), me) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- n ::= e \<turnstile> (le(n \<mapsto> v), me) \<surd>\<^sub>s" by simp
next
  fix A T and w :: "'b value" and l :: "'b local_state"
  case (Create n Tn m m' v)
  assume
    "\<Gamma>, A \<turnstile> create n : T"
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
    "unit = \<C> w"
  then show "\<Gamma>, A \<rhd> create n \<turnstile> (l(n \<mapsto> v), m') \<surd>\<^sub>s"
    using value_neq_exception valid_state_preservation_by_creation
    by (metis Create.hyps(1) Create.hyps(2) big_step_big_steps.Create) 
  then show "\<Gamma>, A \<rhd>+ create n \<turnstile> (l(n \<mapsto> v), m') \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- create n \<turnstile> (l(n \<mapsto> v), m') \<surd>\<^sub>s" by simp
next
  fix A T and w :: "'b value" and l :: "'b local_state"
  case ("Create\<^bsub>fail\<^esub>" n Tn m)
  assume
    "\<Gamma>, A \<turnstile> create n : T"
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
    "Exception = \<C> w"
  then show "\<Gamma>, A \<rhd> create n \<turnstile> (l, m) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>+ create n \<turnstile> (l, m) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- create n \<turnstile> (l, m) \<surd>\<^sub>s" by simp
next
  fix A T and w :: "'b value" and f
  case (Call e l m v le me es vs l' m')
  assume
    "\<Gamma>, A \<turnstile> e \<^bsub>\<bullet>\<^esub> f (es) : T"
    "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
    "unit = \<C> w"
  then show "\<Gamma>, A \<rhd> e \<^bsub>\<bullet>\<^esub> f (es) \<turnstile> (l', m') \<surd>\<^sub>s" using Call.IH by auto 
  then show "\<Gamma>, A \<rhd>+ e \<^bsub>\<bullet>\<^esub> f (es) \<turnstile> (l', m') \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- e \<^bsub>\<bullet>\<^esub> f (es) \<turnstile> (l', m') \<surd>\<^sub>s" by simp
next
  fix A T w c2
  case ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "c1' = \<C> w"
  then show
    lHVc: "\<Gamma>, A \<rhd> if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
      using \<A>_to_all indefinedness_preservation_e valid_local_attachment_state_preservation''(2)
        valid_local_state_preservation_e'
        valid_stateD1 valid_stateD2 valid_stateI
      by (metis "If\<^bsub>true\<^esub>.hyps"(1) "If\<^bsub>true\<^esub>.hyps"(2) "big_step_big_steps.If\<^bsub>true\<^esub>")
  assume
    "c1' = True\<^sub>c"
  moreover with "If\<^bsub>true\<^esub>.IH" lHT lHV have "\<Gamma>, A \<rhd>+ b \<rhd>+ c1 \<turnstile> (l', m') \<surd>\<^sub>s" by auto
  ultimately show "\<Gamma>, A \<rhd>+ if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
    using "If\<^bsub>true\<^esub>.hyps"(2) lHVc result_false by fastforce
next
  fix A T w c2
  case ("If\<^bsub>true\<^esub>" b l m lb mb c1 c1' l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "c1' = \<C> w"
  then have
    lHVc: "\<Gamma>, A \<rhd> if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
      using \<A>_to_all indefinedness_preservation_e valid_local_attachment_state_preservation''(2)
        valid_local_state_preservation_e'
        valid_stateD1 valid_stateD2 valid_stateI
      by (metis "If\<^bsub>true\<^esub>.hyps"(1) "If\<^bsub>true\<^esub>.hyps"(2) "big_step_big_steps.If\<^bsub>true\<^esub>")
  assume
    "c1' = False\<^sub>c"
  with lHT lHV lHVc show "\<Gamma>, A \<rhd>- if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
    using \<A>_to_all \<A>f_to_all 
      valid_local_attachment_state_preservation''(4)
      valid_stateD1 valid_stateD2 valid_stateI
    by (metis "If\<^bsub>true\<^esub>.hyps"(1) "If\<^bsub>true\<^esub>.hyps"(2) "big_step_big_steps.If\<^bsub>true\<^esub>")
next  
  fix A T w c1
  case ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "c2' = \<C> w"
  then show
    lHVc: "\<Gamma>, A \<rhd> if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
      using \<A>_to_all indefinedness_preservation_e
        valid_local_attachment_state_preservation''(2) valid_local_state_preservation_e'
        valid_stateD1 valid_stateD2 valid_stateI
      by (metis "If\<^bsub>false\<^esub>.hyps"(1) "If\<^bsub>false\<^esub>.hyps"(2) "big_step_big_steps.If\<^bsub>false\<^esub>")
  assume
     "c2' = True\<^sub>c"
  with lHT lHV lHVc show "\<Gamma>, A \<rhd>+ if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
    using \<A>_to_all \<A>t_to_all
      valid_local_attachment_state_preservation''(3)
      valid_stateD1 valid_stateD2 valid_stateI
    by (metis "If\<^bsub>false\<^esub>.hyps"(1) "If\<^bsub>false\<^esub>.hyps"(2) "big_step_big_steps.If\<^bsub>false\<^esub>")
next
  fix A T w c1
  case ("If\<^bsub>false\<^esub>" b l m lb mb c2 c2' l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "c2' = \<C> w"
  then have
    lHVc: "\<Gamma>, A \<rhd> if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
      using \<A>_to_all indefinedness_preservation_e
        valid_local_attachment_state_preservation''(2) valid_local_state_preservation_e'
        valid_stateD1 valid_stateD2 valid_stateI
      by (metis "If\<^bsub>false\<^esub>.hyps"(1) "If\<^bsub>false\<^esub>.hyps"(2) "big_step_big_steps.If\<^bsub>false\<^esub>")
  assume
     "c2' = False\<^sub>c"
  with lHT lHV lHVc show "\<Gamma>, A \<rhd>- if b then c1 else c2 end \<turnstile> (l', m') \<surd>\<^sub>s"
    using \<A>_to_all \<A>f_to_all
      valid_local_attachment_state_preservation''(4)
      valid_stateD1 valid_stateD2 valid_stateI
    by (metis "If\<^bsub>false\<^esub>.hyps"(1) "If\<^bsub>false\<^esub>.hyps"(2) "big_step_big_steps.If\<^bsub>false\<^esub>")
next
  fix A T and w :: "'b value" and c
  case ("Loop\<^bsub>true\<^esub>" e l m le me)
  assume
    lHT: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "unit = \<C> w"
  then have
    lHTe: "\<Gamma>, loop_computation e c A \<turnstile> e: Attached" by auto
  moreover have
    lHle: "loop_computation e c A \<le> A" by (rule loop_computation_le0)
  ultimately have "\<Gamma>, A \<turnstile> e: Attached" using AT_mono conforms_to_attached by blast
  with lHTe lHV lHle show "\<Gamma>, A \<rhd> until e loop c end \<turnstile> (le, me) \<surd>\<^sub>s"
    using \<A>_to_all
      valid_state_anti_mono "Loop\<^bsub>true\<^esub>.IH" "Loop\<^bsub>true\<^esub>.hyps" 
      attachment_loop_condition reachability_preservation_scope_true by metis
  then show "\<Gamma>, A \<rhd>+ until e loop c end \<turnstile> (le, me) \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- until e loop c end \<turnstile> (le, me) \<surd>\<^sub>s" by simp
next
  fix A T w
  case ("Loop\<^bsub>false\<^esub>" e l m le me c lc mc c' l' m')
  assume
    lHT: "\<Gamma>, A \<turnstile> until e loop c end : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "c' = \<C> w"
  then show "\<Gamma>, A \<rhd> until e loop c end \<turnstile> (l', m') \<surd>\<^sub>s"
    using indefinedness_preservation_e
      valid_local_attachment_state_preservation''(2) valid_local_state_preservation_e'
      valid_stateD1 valid_stateD2 valid_stateI
    by (metis "Loop\<^bsub>false\<^esub>.IH"(7) "Loop\<^bsub>false\<^esub>.hyps"(1) "Loop\<^bsub>false\<^esub>.hyps"(2) "Loop\<^bsub>false\<^esub>.hyps"(3)
        "big_step_big_steps.Loop\<^bsub>false\<^esub>")
  then show "\<Gamma>, A \<rhd>+ until e loop c end \<turnstile> (l', m') \<surd>\<^sub>s" by simp
  then show "\<Gamma>, A \<rhd>- until e loop c end \<turnstile> (l', m') \<surd>\<^sub>s" by simp
next
  fix A T and w :: "'b value" and n
  case ("Test\<^bsub>true\<^esub>" e l m v le me t)
  assume
    lHT: "\<Gamma>, A \<turnstile> attached t e as n : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "True\<^sub>c = \<C> w"
  then show
    lHVc: "\<Gamma>, A \<rhd> attached t e as n \<turnstile> (le(n \<mapsto> v), me) \<surd>\<^sub>s"
    using value_neq_exception local_attachment_state_upd local_state_upd
      valid_local_attachment_state_preservation valid_local_state_preservation_e'
      valid_stateD1 valid_stateD2 valid_stateI
    by (metis (no_types) "Test\<^bsub>true\<^esub>.hyps"(1) "Test\<^bsub>true\<^esub>.hyps"(2) ExpressionValidity.TestE
      TransferFunction.\<A>_Test)
  with lHT lHV show "\<Gamma>, A \<rhd>+ attached t e as n \<turnstile> (le(n \<mapsto> v), me) \<surd>\<^sub>s"
    using \<A>_to_all
      reachability_with_scope_true valid_local_attachment_state_preservation''(3)
      valid_stateD1 valid_stateD2 valid_stateI
    by (metis "Test\<^bsub>true\<^esub>.hyps"(1) "Test\<^bsub>true\<^esub>.hyps"(2) "big_step_big_steps.Test\<^bsub>true\<^esub>")
  from lHVc show "\<Gamma>, A \<rhd>- attached t e as n \<turnstile> (le(n \<mapsto> v), me) \<surd>\<^sub>s" by simp
next
  fix A T and w :: "'b value" and n
  case ("Test\<^bsub>false\<^esub>" e l m v le me t)
  assume
    lHT: "\<Gamma>, A \<turnstile> attached t e as n : T" and
    lHV: "\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s" and
    lHw: "False\<^sub>c = \<C> w"
  with "Test\<^bsub>false\<^esub>.hyps"(1) have
    "valid_local_state \<Gamma> le" by (meson valid_local_state_preservation_e' valid_stateD1)
  then show
    lHVc: "\<Gamma>, A \<rhd> attached t e as n \<turnstile> (le, me) \<surd>\<^sub>s"
      using "Test\<^bsub>false\<^esub>.IH"(1) lHT lHV by auto
  from lHVc show "\<Gamma>, A \<rhd>- attached t e as n \<turnstile> (le, me) \<surd>\<^sub>s" by simp
  assume
    "False\<^sub>c = True\<^sub>c"
  then show "\<Gamma>, A \<rhd>+ attached t e as n \<turnstile> (le, me) \<surd>\<^sub>s" by simp
next
	fix A Ts ws
	case (Cons e l m v le me es es' l' m')
	assume
		"\<Gamma>, A \<turnstile> e # es [:] Ts"
		"(Value v) # es' = map Value ws"
		"\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
	with Cons.IH show "\<Gamma>, A \<rhd>\<rhd> (e # es) \<turnstile> (l', m') \<surd>\<^sub>s" by auto
	then show "\<Gamma>, A \<rhd>\<rhd> (e # es) \<turnstile> (l', m') \<surd>\<^sub>s" .
	then show "\<Gamma>, A \<rhd>\<rhd> (e # es) \<turnstile> (l', m') \<surd>\<^sub>s" .
next
	fix A Ts ws
	case (ConsEx e l m l' m' es)
	assume
		"\<Gamma>, A \<turnstile> e # es [:] Ts"
		" Exception # es = map Value ws"
		"\<Gamma>, A \<turnstile> (l, m) \<surd>\<^sub>s"
	then show "\<Gamma>, A \<rhd>\<rhd> (e # es) \<turnstile> (l', m') \<surd>\<^sub>s" by blast
	then show "\<Gamma>, A \<rhd>\<rhd> (e # es) \<turnstile> (l', m') \<surd>\<^sub>s" .
	then show "\<Gamma>, A \<rhd>\<rhd> (e # es) \<turnstile> (l', m') \<surd>\<^sub>s" .
qed simp_all

lemma
  assumes
    "\<Gamma> \<turnstile> \<langle>c, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
    "\<Gamma>, A \<turnstile> c : T"
    "\<Gamma>, A \<turnstile> s \<surd>\<^sub>s"
  shows
    valid_state_preservation': "c' \<noteq> Exception \<Longrightarrow> \<Gamma>, A \<rhd> c \<turnstile> s' \<surd>\<^sub>s" and
    valid_state_preservation_scope_true': "c' = True\<^sub>c \<Longrightarrow> \<Gamma>, A \<rhd>+ c \<turnstile> s' \<surd>\<^sub>s" and
    valid_state_preservation_scope_false': "c' = False\<^sub>c \<Longrightarrow> \<Gamma>, A \<rhd>- c \<turnstile> s' \<surd>\<^sub>s"
proof -
  obtain l m l' m' where
    lHs: "(l, m) = s" and
    lHs': "(l', m') = s'" using prod.collapse by blast
  from valid_local_attachment_state_preservation valid_local_state_preservation'
  show "c' \<noteq> Exception \<Longrightarrow> \<Gamma>, A \<rhd> c \<turnstile> s' \<surd>\<^sub>s" using assms lHs lHs' by fastforce
  from valid_state_preservation_scope_true
  show "c' = True\<^sub>c \<Longrightarrow> \<Gamma>, A \<rhd>+ c \<turnstile> s' \<surd>\<^sub>s" using assms lHs lHs' by fastforce
  from valid_state_preservation_scope_false
  show "c' = False\<^sub>c \<Longrightarrow> \<Gamma>, A \<rhd>- c \<turnstile> s' \<surd>\<^sub>s" using assms lHs lHs' by fastforce
qed

subsection "Preservation of attachment property"

lemma value_attachment: "\<Gamma>, A \<turnstile> Value v : \<T>\<^sub>c v"
	by (cases "v = Void\<^sub>v") (fastforce, metis AT_ValueAtt attached_typed_value_is_not_void)

lemma values_attachment: "\<Gamma>, A \<turnstile> map Value vs [:] map \<T>\<^sub>c vs"
	using value_attachment by (induction vs arbitrary: \<Gamma> A) (simp, fastforce)

theorem
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	assumes
		"\<Gamma>, A \<turnstile> s \<surd>\<^sub>s" and
		"A = \<lceil>a\<rceil>"
	shows
		attachment_preservation_step: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow>
		\<Gamma>, A \<turnstile> e : T \<Longrightarrow>
		e' \<noteq> Exception \<Longrightarrow>
		\<exists> T'. \<Gamma>, A \<rhd> e \<turnstile> e' : T' \<and> T' \<rightarrow>\<^sub>a T" and
		attachment_preservation_step_s: "\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle> \<Longrightarrow>
		\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow>
		Exception \<notin> set es' \<Longrightarrow>
		\<exists> Ts'. \<Gamma>, A \<turnstile> es' [:] Ts'"
using assms
proof (induction arbitrary: A a T and A a Ts rule: "big_step_big_steps.inducts")
  case (Value v l m A a)
  then show ?case by blast
next
  case (Local l n v m A a)
  then show ?case
  proof (cases T)
    case Attached
    with Local.hyps Local.prems show ?thesis
	    using valid_local_attachment_state_def topset.discI by fastforce
  next
    case Detachable
    with Local show ?thesis by auto
  qed
next
  case (Seq c\<^sub>1 s s' c\<^sub>2 c\<^sub>2' s'' A a)
  then have
    lHS1: "\<Gamma> \<turnstile> \<langle>c\<^sub>1, s\<rangle> \<Rightarrow> \<langle>unit, s'\<rangle>" and
    lHV: "\<Gamma>, A \<turnstile> s \<surd>\<^sub>s"
    by simp_all
  from Seq.prems have
    lHT1: "\<Gamma>, A \<turnstile> c\<^sub>1: Attached" and
    lHT2: "\<Gamma>, \<A> c\<^sub>1 A \<turnstile> c\<^sub>2: Attached" and
    lHT: "T = Attached"
    by auto
  from lHT1 lHS1 lHV have
    lHV1: "\<Gamma>, \<A> c\<^sub>1 A \<turnstile> s' \<surd>\<^sub>s" using valid_state_preservation' by blast
  from lHS1 lHT1 \<open>A = \<lceil>a\<rceil>\<close> have "A \<rhd> c\<^sub>1 \<noteq> \<top>" using reachability_preservation
    by (metis value_neq_exception prod.collapse topset.distinct(1))
  with Seq.prems lHT2 lHT lHV1 have "\<exists>T'. \<Gamma>, (A \<rhd> c\<^sub>1 \<rhd> c\<^sub>2) \<turnstile> c\<^sub>2' : T' \<and> T' \<rightarrow>\<^sub>a Attached"
    using Seq.IH by (metis topset.collapse)
  then show "\<exists>T'. \<Gamma>, (A \<rhd> c\<^sub>1 ;; c\<^sub>2) \<turnstile> c\<^sub>2' : T' \<and> T' \<rightarrow>\<^sub>a T" by (simp add: lHT)
next
  case Assign then show ?case by auto
next
  case Create then show ?case by auto
next
  case Call then show ?case by blast
next
  case H: ("If\<^bsub>true\<^esub>" b s s' c1 c1' s'' c2 A a T)
  then have
    lHbT: "\<Gamma>, A \<turnstile> b: Attached" by blast
  with H.IH H.prems have
    "\<Gamma>, (A \<rhd> b) \<turnstile> s' \<surd>\<^sub>s" using  value_neq_exception by (simp add: valid_state_preservation')
  from H.IH H.prems lHbT have
    lHS: "\<Gamma>, (A \<rhd>+ b) \<turnstile> s' \<surd>\<^sub>s" using valid_state_preservation_scope_true by (metis prod.collapse)
  then have "\<Gamma>, (A \<rhd> b) \<turnstile> True\<^sub>c : Attached" by auto
  from lHbT H.IH "H.prems" have
    "A \<rhd> b \<noteq> \<top>" using reachability_preservation
      by (metis value_neq_exception prod.collapse topset.distinct(1))
  with lHbT H.IH H.prems have
    lHR: "A \<rhd>+ b \<noteq> \<top>" using reachability_preservation_scope_true
      by (metis prod.collapse topset.discI)
  from H obtain T1 where
    lHcT: "\<Gamma>, (A \<rhd>+ b) \<turnstile> c1 : T1" and
    lHcTT: "T1 \<rightarrow>\<^sub>a T" using upper_bound_left by auto 
  with lHR H.IH H.prems have "A \<rhd>+ b \<rhd> c1 \<noteq> \<top>" using reachability_preservation
    by (metis prod.collapse)
  then obtain Tc where "A \<rhd>+ b \<rhd> c1 = \<lceil>Tc\<rceil>" using topset.exhaust by metis
  with H.IH lHS lHcT H.prems obtain T' where
    lIH1: "\<Gamma>, (A \<rhd>+ b \<rhd> c1) \<turnstile> c1' : T' \<and> T' \<rightarrow>\<^sub>a T1"
      by (metis lHR topset.exhaust)
  from "H.prems" H.IH obtain v where
    "c1' = \<C> v" using Final_def big_step_final by metis
  with lIH1 have "\<exists>T'. \<Gamma>, A \<rhd> if b then c1 else c2 end \<turnstile> c1' : T' \<and> T' \<rightarrow>\<^sub>a T1" by auto
  with lHcTT show ?case using attachment_conforming_to_transitive by blast
next
  case H: ("If\<^bsub>false\<^esub>" b s s' c2 c2' s'' c1 A a T)
  then have
    lHbT: "\<Gamma>, A \<turnstile> b: Attached" by blast
  with H.IH H.prems have
    "\<Gamma>, (A \<rhd> b) \<turnstile> s' \<surd>\<^sub>s" using value_neq_exception by (simp add: valid_state_preservation')
  from H.IH H.prems lHbT have
    lHS: "\<Gamma>, (A \<rhd>- b) \<turnstile> s' \<surd>\<^sub>s" using valid_state_preservation_scope_false
      by (metis prod.collapse)
  have "\<Gamma>, (A \<rhd> b) \<turnstile> True\<^sub>c : Attached" by auto
  from lHbT H.IH "H.prems" have
    "A \<rhd> b \<noteq> \<top>" using reachability_preservation
      by (metis value_neq_exception prod.collapse topset.distinct(1))
  with H.IH H.prems lHbT have
    lHR: "A \<rhd>- b \<noteq> \<top>" using reachability_preservation_scope_false
      by (metis old.prod.exhaust topset.discI)
  from H obtain T2 where
    lHcT: "\<Gamma>, (A \<rhd>- b) \<turnstile> c2 : T2" and
    lHcTT: "T2 \<rightarrow>\<^sub>a T" using upper_bound_right by auto 
  with lHR H.IH H.prems have "A \<rhd>- b \<rhd> c2 \<noteq> \<top>" using reachability_preservation
    by (metis prod.collapse)
  then obtain Tc where "A \<rhd>- b \<rhd> c2 = \<lceil>Tc\<rceil>" using topset.exhaust by metis 
  with H.IH lHS lHcT H.prems obtain T' where
    lIH1: "\<Gamma>, (A \<rhd>- b \<rhd> c2) \<turnstile> c2' : T' \<and> T' \<rightarrow>\<^sub>a T2"
      by (metis lHR topset.exhaust)
  moreover from "H.prems" H.IH obtain v where
    "c2' = \<C> v" using Final_def big_step_final by metis
  with lIH1 have "\<exists>T'. \<Gamma>, A \<rhd> if b then c1 else c2 end \<turnstile> c2' : T' \<and> T' \<rightarrow>\<^sub>a T2" by auto
  with lHcTT show ?case using attachment_conforming_to_transitive by blast
next
  case ("Loop\<^bsub>true\<^esub>" e s s' c A a) then show ?case by auto
next
  case ("Loop\<^bsub>false\<^esub>" e s s\<^sub>e c s\<^sub>c c' s' A a T)
  moreover then have
    lHT: "T = Attached" by auto
  with "Loop\<^bsub>false\<^esub>.prems" have "\<Gamma>, A \<turnstile> until e loop c end: Attached" by simp
  with "Loop\<^bsub>false\<^esub>.prems" have
    lHTe0: "\<Gamma>, loop_computation e c A \<turnstile> e: Attached" and
    lHTc0: "\<Gamma>, loop_computation e c A \<rhd>- e \<turnstile> c: Attached"
    by auto
  then have
    lHTe: "\<Gamma>, A \<turnstile> e: Attached"
      using AT_mono conforms_to_attached loop_computation_le0 by metis
  ultimately have
    lHVe: "\<Gamma>, (A \<rhd>- e) \<turnstile> s\<^sub>e \<surd>\<^sub>s" using valid_state_preservation_scope_false'
      by metis
  from "Loop\<^bsub>false\<^esub>.prems" have "A \<noteq> \<top>" by simp
  with "Loop\<^bsub>false\<^esub>.IH" "Loop\<^bsub>false\<^esub>.prems" lHTe have "A \<rhd>- e \<noteq> \<top>"
    using reachability_preservation_scope_false "prod.collapse" by metis
  then obtain ae where
    lHAe: "A \<rhd>- e = \<lceil>ae\<rceil>" using topset.exhaust by auto
  have "loop_computation e c A \<le> A" using loop_computation_le0 by metis
  then have "loop_computation e c A \<rhd>- e \<le> A \<rhd>- e" by (simp add: \<A>f_mono')
  with lHTc0 have
    lHTc: "\<Gamma>, A \<rhd>- e \<turnstile> c: Attached"
      using AT_mono conforms_to_attached by blast
  have
    lHEu: "unit \<noteq> Exception" by simp
  from "Loop\<^bsub>false\<^esub>.IH" lHTc lHVe lHEu have
    lHVc: "\<Gamma>, (A \<rhd>- e \<rhd> c) \<turnstile> s\<^sub>c \<surd>\<^sub>s"
      using valid_state_preservation' by metis
  obtain l\<^sub>e m\<^sub>e l\<^sub>c m\<^sub>c where "s\<^sub>e = (l\<^sub>e, m\<^sub>e)" and "s\<^sub>c = (l\<^sub>c, m\<^sub>c)" by fastforce
  with "Loop\<^bsub>false\<^esub>.IH" have "\<Gamma> \<turnstile> \<langle>c, (l\<^sub>e, m\<^sub>e)\<rangle> \<Rightarrow> \<langle>unit, (l\<^sub>c, m\<^sub>c)\<rangle>" by simp
  with lHAe lHTc lHEu have "A \<rhd>- e \<rhd> c \<noteq> \<top>" by (simp add: reachability_preservation)
  then obtain ac where
    lHAc: "A \<rhd>- e \<rhd> c = \<lceil>ac\<rceil>" using topset.exhaust by auto
  with "Loop\<^bsub>false\<^esub>.IH" "Loop\<^bsub>false\<^esub>.prems" AT_loop_step lHVc have
    lHTl: "\<exists>T'. \<Gamma>, A \<rhd>- e \<rhd> c \<rhd> until e loop c end \<turnstile> c' : T' \<and> T' \<rightarrow>\<^sub>a T"
      by metis
  with lHT have "\<Gamma>, A \<rhd>- e \<rhd> c \<rhd> until e loop c end \<turnstile> c' : Attached"
    using conforms_to_attached by blast
  with "Loop\<^bsub>false\<^esub>.IH" "Loop\<^bsub>false\<^esub>.prems" obtain v where
    "c' = \<C> v" using big_step_final_value by metis
  with lHTl show ?case by blast
next
  case "Test\<^bsub>true\<^esub>" then show ?case by auto
next
  case "Test\<^bsub>false\<^esub>" then show ?case by auto
next
	case Nil then show ?case by simp
next
	case (Cons e s v se es es' s')
	from Cons.prems obtain Ts' where
		"\<Gamma>, A \<rhd> e \<turnstile> es [:] Ts'" using list_attachment_validity_tail by blast
	moreover from Cons have "\<Gamma>, A \<rhd> e \<turnstile> se \<surd>\<^sub>s" using value_neq_exception valid_state_preservation'
		by (metis ATs_iffs(3))
	moreover from Cons obtain b where "A \<rhd> e = \<lceil>b\<rceil>" using value_neq_exception reachability_preservation
		by (metis ATs_iffs(3) prod.collapse topset.exhaust_sel)
	moreover note Cons
	ultimately have "\<exists>a. \<Gamma>, A \<rhd> e \<turnstile> es' [:] a" by auto
	then obtain Ts'' where
		lHTs: "\<Gamma>, A \<rhd> e \<turnstile> es' [:] Ts''" by auto
	from Cons.prems have "Exception \<notin> set es'" by simp
	moreover from Cons.IH have "Finals es'" using big_step_finals by metis
	ultimately have "\<exists> vs. map Value vs = es'" using Finals_def by (metis in_set_conv_decomp)
	then obtain vs where "es' = map Value vs" by auto
	with lHTs have "\<Gamma>, A \<turnstile> es' [:] Ts''" using attachment_unique_es values_attachment by blast
	moreover obtain Te where "\<Gamma>, A \<rhd> e \<turnstile> Value v : Te" by blast
	ultimately have "\<Gamma>, A \<turnstile> (Value v) # es' [:] Te # Ts''" by auto
	then show ?case by blast
qed simp_all

theorem	attachment_preservation:
	"\<Gamma> \<turnstile> \<langle>e, (empty, empty)\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow>
	\<Gamma> \<turnstile> e : Attached \<Longrightarrow>
	e' = Exception \<or> (\<exists> v. e' = Value v \<and> v \<noteq> Void\<^sub>v)"
using big_step_final_value local_attachment_state_bottom attachment_type_valid_expression_def
	local_attachment_type_preservation by (metis surj_pair topset.discI)

end