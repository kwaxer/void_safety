section "Value definitions"

theory Value imports Type begin

datatype 'basic_value object_value =
	"Basic" 'basic_value |
	"Boolean" bool |
	"User"

datatype 'basic_value "reference_value" =
	"Void" |
	"AttachedObject" "'basic_value object_value"

datatype 'basic_value "value" =
	"Unit" |
	"Object" "'basic_value object_value" |
	"Reference" "'basic_value reference_value"

definition "is_value v \<equiv> v \<noteq> Unit"

primrec is_expanded :: "'basic_value value \<Rightarrow> bool" where
  "is_expanded Unit \<longleftrightarrow> False" |
  "is_expanded (Object _) \<longleftrightarrow> True" |
  "is_expanded (Reference _) \<longleftrightarrow> False"

primrec is_reference :: "'basic_value value \<Rightarrow> bool" where
  "is_reference Unit \<longleftrightarrow> False" |
  "is_reference (Object _) \<longleftrightarrow> False" |
  "is_reference (Reference _) \<longleftrightarrow> True"

lemma "is_reference v \<Longrightarrow> \<not> is_expanded v"
  by (cases v, simp_all)

lemma "is_expanded v \<Longrightarrow> \<not> is_reference v"
  by (cases v, simp_all)

lemma value_is_expanded_or_reference: "is_value v \<Longrightarrow> is_expanded v \<or> is_reference v"
  by (cases v, simp_all add: is_value_def)

primrec is_attached :: "'basic_value value \<Rightarrow> bool" where
	"is_attached Unit = True" |
	"is_attached (Object _) = True" |
	"is_attached (Reference x) = (case x of AttachedObject _ \<Rightarrow> True | _ \<Rightarrow> False)"

abbreviation "Void\<^sub>v \<equiv> Reference Void"

lemma is_attached_if_not_void [iff]: " is_attached x \<longleftrightarrow> x \<noteq> Void\<^sub>v"
  by (cases x, insert reference_value.exhaust, force+)

primrec is_basic :: "'basic_value value \<Rightarrow> bool" where
	"is_basic Unit = False" |
	"is_basic (Object x) = (case x of Boolean _ \<Rightarrow> True | Basic _ \<Rightarrow> True | _ \<Rightarrow> False)" |
	"is_basic (Reference x) = False"

primrec is_boolean :: "'basic_value value \<Rightarrow> bool" where
	"is_boolean Unit = False" |
	"is_boolean (Object x) = (case x of Boolean _ \<Rightarrow> True | _ \<Rightarrow> False)" |
	"is_boolean (Reference x) = False"

lemma expanded_is_attached: "is_expanded v \<Longrightarrow> is_attached v"
  by (cases v, simp_all)

lemma basic_is_expanded: "is_basic v \<Longrightarrow> is_expanded v"
  by (cases v, simp_all)

lemma boolean_is_basic: "is_boolean v \<Longrightarrow> is_basic v"
proof (cases v)
  case (Object x)
  moreover assume "is_boolean v"
  ultimately show ?thesis by (cases x, simp_all)
qed simp_all

lemma basic_is_attached: "is_basic v \<Longrightarrow> is_attached v"
  by auto

lemma boolean_is_expanded: "is_boolean v \<Longrightarrow> is_expanded v"
  by (simp add: boolean_is_basic basic_is_expanded)

lemma boolean_is_attached: "is_boolean v \<Longrightarrow> is_attached v"
  by auto

primrec boolean_coercion :: "bool \<Rightarrow> 'basic_value value" where
  "boolean_coercion False = Object (Boolean False)" |
  "boolean_coercion True = Object (Boolean True)"

locale base_types = 
	fixes
		is_valid_basic_type :: "'a type \<Rightarrow> bool" and
		default_value :: "'a type \<Rightarrow> 'basic_value option" and
		value_type :: "'basic_value \<Rightarrow> 'a type" and
		boolean_mark :: "'a"
	assumes
		"\<And> x. is_valid_basic_type x \<Longrightarrow> \<exists> y. (default_value x = Some y) \<and> (value_type y = x)" and
		"\<And> x. is_valid_basic_type (value_type x)"
		"\<And> x. value_type x \<noteq> boolean_class_type boolean_mark"
begin

end

end