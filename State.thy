theory State imports
	Name
	Value
begin

type_synonym 'value_type local_state = "vname \<Rightarrow> 'value_type value option"

type_synonym ('value_type, 'memory) state = "'value_type local_state \<times> 'memory"

end
