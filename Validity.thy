theory Validity imports
  Expression Environment
begin

inductive
	is_valid :: "'a environment \<Rightarrow> ('b, 't) expression \<Rightarrow> bool" ("_ \<turnstile>/ _ \<surd>" [59, 59] 59) and
	is_valid_list :: "'a environment \<Rightarrow> ('b, 't) expression list  \<Rightarrow> bool" ("_ \<turnstile>/ _ [\<surd>]" [59, 59] 59)
where
	V_Value: "\<Gamma> \<turnstile> \<C> v \<surd>" |
	V_Local: "\<Gamma> n = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma> \<turnstile> \<V> n \<surd>" |
	V_Seq: "\<lbrakk>\<Gamma> \<turnstile> c\<^sub>1 \<surd>; \<Gamma> \<turnstile> c\<^sub>2 \<surd>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> c\<^sub>1;; c\<^sub>2 \<surd>" |
	V_Assign: "\<lbrakk>\<Gamma> n = \<lfloor>T\<rfloor>; \<Gamma> \<turnstile> e \<surd>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> n ::= e \<surd>" |
	V_Create: "\<Gamma> n = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma> \<turnstile> create n \<surd>" |
	V_Call: "\<lbrakk>\<Gamma> \<turnstile> e \<surd>; \<Gamma> \<turnstile> a [\<surd>]\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> e \<^bsub>\<bullet>\<^esub> f (a) \<surd>" |
	V_If: "\<lbrakk>\<Gamma> \<turnstile> b \<surd>; \<Gamma> \<turnstile> c1 \<surd>; \<Gamma> \<turnstile> c2 \<surd>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> if b then c1 else c2 end \<surd>" |
	V_Loop: "\<lbrakk>\<Gamma> \<turnstile> e \<surd>; \<Gamma> \<turnstile> c \<surd>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> until e loop c end \<surd>" |
	V_Test: "\<lbrakk>\<Gamma> \<turnstile> e \<surd>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> attached t e as n \<surd>" |
	V_Nil: "\<Gamma> \<turnstile> [] [\<surd>]" |
	V_Cons: "\<lbrakk>\<Gamma> \<turnstile> e \<surd>; \<Gamma> \<turnstile> es [\<surd>]\<rbrakk>\<Longrightarrow> \<Gamma> \<turnstile> e # es [\<surd>]"

lemmas at_induct = "is_valid_is_valid_list.inducts"[split_format(complete)]
inductive_cases ValueE[elim!]: "\<Gamma> \<turnstile> \<C> v \<surd>"
inductive_cases LocalE[elim!]: "\<Gamma> \<turnstile> \<V> n \<surd>"
inductive_cases SeqE[elim!]: "\<Gamma> \<turnstile> c1 ;; c2 \<surd>"
inductive_cases AssignE[elim!]: "\<Gamma> \<turnstile> n ::= e \<surd>"
inductive_cases CreateE[elim!]: "\<Gamma> \<turnstile> create n \<surd>"
inductive_cases CallE[elim!]: "\<Gamma> \<turnstile> n \<^bsub>\<bullet>\<^esub> f (a) \<surd>"
inductive_cases IfE[elim!]: "\<Gamma> \<turnstile> if b then c1 else c2 end \<surd>"
inductive_cases LoopE[elim!]: "\<Gamma> \<turnstile> until e loop c end \<surd>"
inductive_cases TestE[elim!]: "\<Gamma> \<turnstile> attached t e as n \<surd>"

end