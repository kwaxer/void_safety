section "Environment"

theory Environment imports
	Name Type
begin

type_synonym 'a environment = "vname \<Rightarrow> 'a type option"

end