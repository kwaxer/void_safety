theory TransferFunction imports         
	ValueAttachment LoopOperator Environment Expression
begin

paragraph "Transfer function without scopes"

fun
  \<A>' :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>s' :: "('b, 't) expression list \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>\<T>' :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> bool"
where

-- "Access"

	\<A>'_Value: "\<A>' (\<C> v) A = A" |
	\<A>'_Local: "\<A>' (\<V> n) A = A" |

-- "Reattachment"

	\<A>'_Assign: "\<A>' (n ::= e) A =	(if \<A>\<T>' e A then \<A>' e A \<oplus> n else \<A>' e A \<ominus> n)" |
	\<A>'_Create: "\<A>' (create n) A = A \<oplus> n" |
	\<A>'_Call: "\<A>' (e \<^bsub>\<bullet>\<^esub> f (a)) A = \<A>s' a (\<A>' e A)" |

-- "Compound instructions"

  \<A>'_Seq: "\<A>' (c\<^sub>1;; c\<^sub>2) A = \<A>' c\<^sub>2 (\<A>' c\<^sub>1 A)" |
	\<A>'_If: "\<A>' (if b then c\<^sub>1 else c\<^sub>2 end) A = \<A>' c\<^sub>1 (\<A>' b A) \<sqinter> \<A>' c\<^sub>2 (\<A>' b A)" |
	\<A>'_Loop: "\<A>' (until e loop c end) A = \<A>' e (loop_operator (\<lambda> B. \<A>' c (\<A>' e B)) A)" |
	\<A>'_Exception: "\<A>' Exception A = \<top>" |

-- "Boolean expressions"

	\<A>'_Test: "\<A>' (attached t e as n) A = \<A>' e A" |

-- "List of expressions"

  \<A>s'_Nil: "\<A>s' [] A = A" |
  \<A>s'_Cons: "\<A>s' (e # es) A = \<A>s' es (\<A>' e A)" |

-- "Is value attached?"

	\<A>\<T>'_Value: "\<A>\<T>' (\<C> v) A \<longleftrightarrow> v \<noteq> Void\<^sub>v" |
	\<A>\<T>'_Local: "\<A>\<T>' (\<V> n) A \<longleftrightarrow> n \<in>\<^sup>\<top> A" |
	\<A>\<T>'_If: "\<A>\<T>' (if b then c\<^sub>1 else c\<^sub>2 end) A \<longleftrightarrow> \<A>\<T>' c\<^sub>1 (\<A>' b A) \<and> \<A>\<T>' c\<^sub>2 (\<A>' b A)" |

-- "Fallback"

	\<A>\<T>'_Other: "\<A>\<T>' _ A \<longleftrightarrow> True"

abbreviation (output) \<A>'_rep (infixl "\<rhd>" 72) where "\<A>'_rep A c \<equiv> \<A>' c A"
abbreviation (output) \<A>s'_rep (infixl "\<rhd>\<rhd>" 72) where "\<A>s'_rep A es \<equiv> \<A>s' es A"
abbreviation (output) \<A>\<T>'_rep (infixl "\<hookrightarrow>" 71) where "\<A>\<T>'_rep A c \<equiv> \<A>\<T>' c A"
abbreviation (output) loop_computation_rep' ("_ \<rhd>* '(_ \<rhd> _')" [73, 73, 73])
  where "loop_computation_rep' A e c \<equiv> loop_operator (\<lambda>X. \<A>' c (\<A>' e X)) A"

paragraph "Transfer function with scopes"

fun
  \<A> :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>t :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>f :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>s :: "('b, 't) expression list \<Rightarrow> vname topset \<Rightarrow> vname topset" and
  \<A>\<T> :: "('b, 't) expression \<Rightarrow> vname topset \<Rightarrow> bool"
where

-- "Access"

	\<A>_Value: "\<A> (\<C> v) A = A" |
	\<A>_Local: "\<A> (\<V> n) A = A" |

-- "Reattachment"

	\<A>_Assign: "\<A> (n ::= e) A =	(if \<A>\<T> e A then \<A> e A \<oplus> n else \<A> e A \<ominus> n)" |
	\<A>_Create: "\<A> (create n) A = A \<oplus> n" |
	\<A>_Call: "\<A> (e \<^bsub>\<bullet>\<^esub> f (a)) A = \<A>s a (\<A> e A)" |

-- "Compound instructions"

  \<A>_Seq: "\<A> (e\<^sub>1;; e\<^sub>2) A = \<A> e\<^sub>2 (\<A> e\<^sub>1 A)" |
	\<A>_If: "\<A> (if c then e\<^sub>1 else e\<^sub>2 end) A = \<A> e\<^sub>1 (\<A>t c A) \<sqinter> \<A> e\<^sub>2 (\<A>f c A)" |
	\<A>t_If: "\<A>t (if c then e\<^sub>1 else e\<^sub>2 end) A =
    (if is_false e\<^sub>1 then \<A>t e\<^sub>2 (\<A>f c A) else
    (if is_false e\<^sub>2 then \<A>t e\<^sub>1 (\<A>t c A) else
    \<A> (if c then e\<^sub>1 else e\<^sub>2 end) A))" |
  \<A>f_If: "\<A>f (if c then e\<^sub>1 else e\<^sub>2 end) A =
    (if is_true e\<^sub>1 then \<A>f e\<^sub>2 (\<A>f c A) else
    (if is_true e\<^sub>2 then \<A>f e\<^sub>1 (\<A>t c A) else
    \<A> (if c then e\<^sub>1 else e\<^sub>2 end) A))" |
	\<A>_Loop: "\<A> (until e loop b end) A = \<A>t e (loop_operator (\<lambda> B. \<A> b (\<A>f e B)) A)" |
	\<A>_Exception: "\<A> Exception A = \<top>" |

-- "Boolean expressions"

	\<A>_Test: "\<A> (attached t e as n) A = \<A> e A" |
	\<A>t_Test2: "\<A>t (attached T (\<V> n') as n) A = A \<oplus> n' \<oplus> n" |
	\<A>t_Test1: "\<A>t (attached T e as n) A = \<A> e A \<oplus> n" |

-- "List of expressions"

  \<A>s_Nil: "\<A>s [] A = A" |
  \<A>s_Cons: "\<A>s (e # es) A = \<A>s es (\<A> e A)" |

-- "Is value attached?"

	\<A>\<T>_Value: "\<A>\<T> (\<C> v) A \<longleftrightarrow> v \<noteq> Void\<^sub>v" |
	\<A>\<T>_Local: "\<A>\<T> (\<V> n) A \<longleftrightarrow> n \<in>\<^sup>\<top> A" |
	\<A>\<T>_If: "\<A>\<T> (if c then e\<^sub>1 else e\<^sub>2 end) A \<longleftrightarrow> \<A>\<T> e\<^sub>1 (\<A>t c A) \<and> \<A>\<T> e\<^sub>2 (\<A>f c A)" |

-- "Fallback"

	\<A>t_Other: "\<A>t e A = (if is_false e then \<top> else \<A> e A)" |
	\<A>f_Other: "\<A>f e A = (if is_true e then \<top> else \<A> e A)" |
	\<A>\<T>_Other: "\<A>\<T> _ A \<longleftrightarrow> True"


lemmas \<A>_induct = \<A>_\<A>t_\<A>f_\<A>s_\<A>\<T>.induct[split_format(complete)]

abbreviation \<A>_rep (infixl "\<rhd>" 72) where "A \<rhd> c \<equiv> \<A> c A"
abbreviation \<A>_true_rep (infixl "\<rhd>+" 72) where "A \<rhd>+ b \<equiv> \<A>t b A"
abbreviation \<A>_false_rep (infixl "\<rhd>-" 72) where "A \<rhd>- b \<equiv> \<A>f b A"
abbreviation \<A>s_rep (infixl "\<rhd>\<rhd>" 72) where "\<A>s_rep A es \<equiv> \<A>s es A"
abbreviation \<A>\<T>_rep (infixl "\<hookrightarrow>" 71) where "A \<hookrightarrow> c \<equiv> \<A>\<T> c A"

definition loop_step_def [simp]: "loop_step e c A = A \<rhd>- e \<rhd> c"
abbreviation (input) loop_computation where "loop_computation e c A \<equiv> loop_operator (\<lambda>X. X \<rhd>- e \<rhd> c) A"
abbreviation loop_computation_rep ("_ \<rhd>* '(- _ \<rhd> _')" [73, 73, 73]) where "A \<rhd>* (- e \<rhd> c) \<equiv> loop_operator (\<lambda>X. X \<rhd>- e \<rhd> c) A"

lemma transfer_fold: "A \<rhd>\<rhd> es = fold (\<lambda> e X. X \<rhd> e) es A"
	by (induction es arbitrary: A) simp_all

lemma transfer_unfold: "A \<rhd> e \<rhd>\<rhd> es = A \<rhd>\<rhd> (e # es)"
	by simp

lemma unreachable_if_false: "is_false e \<Longrightarrow> A \<rhd>+ e = \<top>"
	by (cases e) simp_all

lemma unreachable_if_true: "is_true e \<Longrightarrow> A \<rhd>- e = \<top>"
	by (cases e) simp_all

lemma
  fixes e :: "('b, 't) expression" and es :: "('b, 't) expression list"
  shows
    \<A>_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e" and
    \<A>t_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<rhd>+ e \<le> Y \<rhd>+ e" and
    \<A>f_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<rhd>- e \<le> Y \<rhd>- e" and
    \<A>s_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<rhd>\<rhd> es \<le> Y \<rhd>\<rhd> es" and
    \<A>\<T>_mono': "\<And> X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> e \<longrightarrow> Y \<hookrightarrow> e"
proof (induction rule: "\<A>_\<A>t_\<A>f_\<A>s_\<A>\<T>.induct")
  fix v :: "'b value" and A X Y :: "vname topset"
  assume
    H: "X \<le> Y"
  from H show "X \<rhd> (\<C> v) \<le> Y \<rhd> \<C> v" by simp
  from H show "X \<rhd>+ (\<C> v) \<le> Y \<rhd>+ \<C> v" by simp
  from H show "X \<rhd>- (\<C> v) \<le> Y \<rhd>- \<C> v" by simp
  from H show "X \<hookrightarrow> (\<C> v) \<longrightarrow> Y \<hookrightarrow> \<C> v" by auto
  fix n
  from H show "X \<rhd> (\<V> n) \<le> Y \<rhd> \<V> n" by simp
  from H show "X \<rhd>+ (\<V> n) \<le> Y \<rhd>+ \<V> n" by simp
  from H show "X \<rhd>- (\<V> n) \<le> Y \<rhd>- \<V> n" by simp
  from H show "X \<hookrightarrow> (\<V> n) \<longrightarrow> Y \<hookrightarrow> \<V> n" by (auto simp add: topset_subsetD)
  from H show "X \<rhd> create n \<le> Y \<rhd> create n" by (simp add: topset_add_mono)
  then show "X \<rhd>+ create n \<le> Y \<rhd>+ create n" by simp
  then show "X \<rhd>- create n \<le> Y \<rhd>- create n" by simp
  then show "X \<hookrightarrow> create n \<longrightarrow> Y \<hookrightarrow> create n" by simp
  then show "X \<rhd> Exception \<le> Y \<rhd> Exception" using topset_member_top by simp
  then show "X \<rhd>+ Exception \<le> Y \<rhd>+ Exception" using topset_member_top by simp
  then show "X \<rhd>- Exception \<le> Y \<rhd>- Exception" using topset_member_top by simp
  then show "X \<hookrightarrow> Exception \<longrightarrow> Y \<hookrightarrow> Exception" by simp
  fix c\<^sub>1 c\<^sub>2 :: "('b, 't) expression"
  from H show "X \<hookrightarrow> c\<^sub>1 ;; c\<^sub>2 \<longrightarrow> Y \<hookrightarrow> c\<^sub>1 ;; c\<^sub>2" by simp
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c\<^sub>1 \<le> Y \<rhd> c\<^sub>1"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c\<^sub>2 \<le> Y \<rhd> c\<^sub>2"
  with H show "X \<rhd> c\<^sub>1 ;; c\<^sub>2 \<le> Y \<rhd> c\<^sub>1 ;; c\<^sub>2" by simp
next
  fix c\<^sub>1 c\<^sub>2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>\<not> is_false (c\<^sub>1 ;; c\<^sub>2); X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> c\<^sub>1 ;; c\<^sub>2 \<le> Y \<rhd> c\<^sub>1 ;; c\<^sub>2"
    "X \<le> Y"
  then show "X \<rhd>+ c\<^sub>1 ;; c\<^sub>2 \<le> Y \<rhd>+ c\<^sub>1 ;; c\<^sub>2" by simp
next
  fix c\<^sub>1 c\<^sub>2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>\<not> is_true (c\<^sub>1 ;; c\<^sub>2); X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> c\<^sub>1 ;; c\<^sub>2 \<le> Y \<rhd> c\<^sub>1 ;; c\<^sub>2"
    "X \<le> Y"
  then show "X \<rhd>- c\<^sub>1 ;; c\<^sub>2 \<le> Y \<rhd>- c\<^sub>1 ;; c\<^sub>2" by simp
next
  fix n and e :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    H: "X \<le> Y"
  then show "X \<hookrightarrow> n ::= e \<longrightarrow> Y \<hookrightarrow> n ::= e" by simp
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> e \<longrightarrow> Y \<hookrightarrow> e"
    "\<And>X Y. \<lbrakk>A \<hookrightarrow> e; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "\<And>X Y. \<lbrakk>\<not> A \<hookrightarrow> e; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
  with H show "X \<rhd> n ::= e \<le> Y \<rhd> n ::= e" using topset_add_rem_mono1 topset_add_mono topset_rem_mono
    by (metis (no_types) \<A>_Assign)
(*
  then show "X \<rhd>+ n ::= e \<le> Y \<rhd>+ n ::= e" by simp
  then show "X \<rhd>- n ::= e \<le> Y \<rhd>- n ::= e" by simp
*)
next
  fix e :: "('b, 't) expression" and f and a :: "('b, 't) expression list" and A X Y :: "vname topset"
  assume
    H: "X \<le> Y"
  then show "X \<hookrightarrow> e \<^bsub>\<bullet>\<^esub> f (a) \<longrightarrow> Y \<hookrightarrow> e \<^bsub>\<bullet>\<^esub> f (a)" by simp
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>\<rhd> a \<le> Y \<rhd>\<rhd> a"
  with H show "X \<rhd> e \<^bsub>\<bullet>\<^esub> f (a) \<le> Y \<rhd> e \<^bsub>\<bullet>\<^esub> f (a)" by simp
next
  fix e :: "('b, 't) expression" and n a and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>\<not> is_false (e \<^bsub>\<bullet>\<^esub> n (a)); X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> e \<^bsub>\<bullet>\<^esub> n (a) \<le> Y \<rhd> e \<^bsub>\<bullet>\<^esub> n (a)"
    "X \<le> Y"
  then show "X \<rhd>+ e \<^bsub>\<bullet>\<^esub> n (a) \<le> Y \<rhd>+ e \<^bsub>\<bullet>\<^esub> n (a)" by simp
next
  fix e :: "('b, 't) expression" and n a and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>\<not> is_true (e \<^bsub>\<bullet>\<^esub> n (a)); X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> e \<^bsub>\<bullet>\<^esub> n (a) \<le> Y \<rhd> e \<^bsub>\<bullet>\<^esub> n (a)"
    "X \<le> Y"
  then show "X \<rhd>- e \<^bsub>\<bullet>\<^esub> n (a) \<le> Y \<rhd>- e \<^bsub>\<bullet>\<^esub> n (a)" by simp
next
  fix b c\<^sub>1 c\<^sub>2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>+ b \<le> Y \<rhd>+ b"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c\<^sub>1 \<le> Y \<rhd> c\<^sub>1"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>- b \<le> Y \<rhd>- b"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> c\<^sub>2 \<le> Y \<rhd> c\<^sub>2"
    "X \<le> Y"
  then show "X \<rhd> if b then c\<^sub>1 else c\<^sub>2 end \<le> Y \<rhd> if b then c\<^sub>1 else c\<^sub>2 end" by simp
next
  fix b e\<^sub>1 e\<^sub>2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>is_false e\<^sub>1; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>- b \<le> Y \<rhd>- b"
    "\<And>X Y. \<lbrakk>is_false e\<^sub>1; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>+ e\<^sub>2 \<le> Y \<rhd>+ e\<^sub>2"
    "\<And>X Y. \<lbrakk>\<not> is_false e\<^sub>1; is_false e\<^sub>2; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>+ b \<le> Y \<rhd>+ b"
    "\<And>X Y. \<lbrakk>\<not> is_false e\<^sub>1; is_false e\<^sub>2; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>+ e\<^sub>1 \<le> Y \<rhd>+ e\<^sub>1"
    "\<And>X Y. \<lbrakk>\<not> is_false e\<^sub>1; \<not> is_false e\<^sub>2; X \<le> Y\<rbrakk> \<Longrightarrow>
               X \<rhd> if b then e\<^sub>1 else e\<^sub>2 end \<le> Y \<rhd> if b then e\<^sub>1 else e\<^sub>2 end"
    "X \<le> Y"
  then show "X \<rhd>+ if b then e\<^sub>1 else e\<^sub>2 end \<le> Y \<rhd>+ if b then e\<^sub>1 else e\<^sub>2 end" by simp
next
  fix b e\<^sub>1 e\<^sub>2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>is_true e\<^sub>1; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>- b \<le> Y \<rhd>- b"
    "\<And>X Y. \<lbrakk>is_true e\<^sub>1; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>- e\<^sub>2 \<le> Y \<rhd>- e\<^sub>2"
    "\<And>X Y. \<lbrakk>\<not> is_true e\<^sub>1; is_true e\<^sub>2; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>+ b \<le> Y \<rhd>+ b"
    "\<And>X Y. \<lbrakk>\<not> is_true e\<^sub>1; is_true e\<^sub>2; X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>- e\<^sub>1 \<le> Y \<rhd>- e\<^sub>1"
    "\<And>X Y. \<lbrakk>\<not> is_true e\<^sub>1; \<not> is_true e\<^sub>2; X \<le> Y\<rbrakk> \<Longrightarrow>
               X \<rhd> if b then e\<^sub>1 else e\<^sub>2 end \<le> Y \<rhd> if b then e\<^sub>1 else e\<^sub>2 end"
    "X \<le> Y"
  then show "X \<rhd>- if b then e\<^sub>1 else e\<^sub>2 end \<le> Y \<rhd>- if b then e\<^sub>1 else e\<^sub>2 end" by simp
next
  fix b c\<^sub>1 c\<^sub>2 :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>+ b \<le> Y \<rhd>+ b"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> c\<^sub>1 \<longrightarrow> Y \<hookrightarrow> c\<^sub>1"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>- b \<le> Y \<rhd>- b"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<hookrightarrow> c\<^sub>2 \<longrightarrow> Y \<hookrightarrow> c\<^sub>2"
    "X \<le> Y"
  then show "X \<hookrightarrow> if b then c\<^sub>1 else c\<^sub>2 end \<longrightarrow> Y \<hookrightarrow> if b then c\<^sub>1 else c\<^sub>2 end"
    by (meson \<A>\<T>_If) 
next
  fix e b :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    H: "X \<le> Y"
  then show "X \<hookrightarrow> until e loop b end \<longrightarrow> Y \<hookrightarrow> until e loop b end" by simp
  assume
    lHi: "\<And>x X Y. \<lbrakk>X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>- e \<le> Y \<rhd>- e" and
    lHb: "\<And>x X Y. \<lbrakk>X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> b \<le> Y \<rhd> b" and
    lHe: "\<And>X Y. \<lbrakk>X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd>+ e \<le> Y \<rhd>+ e"
  then have "mono (\<lambda>x. x \<rhd>- e \<rhd> b)" if "\<not> is_false e" using that  by (simp add: monoI)
  with H have "X \<rhd>* (- e \<rhd> b) \<le> Y \<rhd>* (- e \<rhd> b)" using loop_operator_mono' by simp
  then have "X \<rhd>* (- e \<rhd> b) \<rhd>+ e \<le> Y \<rhd>* (- e \<rhd> b) \<rhd>+ e" using lHe by simp
  then show "X \<rhd> until e loop b end \<le> Y \<rhd> until e loop b end" by simp
next
  fix e b :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>\<not> is_false (until e loop b end); X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> until e loop b end \<le> Y \<rhd> until e loop b end"
    "X \<le> Y"
  then show "X \<rhd>+ until e loop b end \<le> Y \<rhd>+ until e loop b end" by simp
next
  fix e b :: "('b, 't) expression" and A X Y :: "vname topset"
  assume
    "\<And>X Y. \<lbrakk>\<not> is_true (until e loop b end); X \<le> Y\<rbrakk> \<Longrightarrow> X \<rhd> until e loop b end \<le> Y \<rhd> until e loop b end"
    "X \<le> Y"
  then show "X \<rhd>- until e loop b end \<le> Y \<rhd>- until e loop b end" by simp
next
  fix t and e :: "('b, 't) expression" and n n' and A X Y :: "vname topset"
  show "X \<hookrightarrow> attached t e as n \<longrightarrow> Y \<hookrightarrow> attached t e as n" by simp
  assume
    "X \<le> Y"
  then show "X \<rhd>+ attached t (Local n') as n \<le> Y \<rhd>+ attached t (Local n') as n"
  	by (simp add: topset_add_mono)
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "X \<le> Y"
  then show "X \<rhd> attached t e as n \<le> Y \<rhd> attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n"
  	by (cases e) (simp_all add: topset_add_mono)
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
  then show "X \<rhd>+ attached t e as n \<le> Y \<rhd>+ attached t e as n" by simp
next
  fix A X Y :: "vname topset"
  assume
    H: "X \<le> Y"
  then show "X \<rhd>\<rhd> [] \<le> Y \<rhd>\<rhd> []" by simp
  fix e :: "('b, 't) expression" and es :: "('b, 't) expression list"
  assume
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd> e \<le> Y \<rhd> e"
    "\<And>X Y. X \<le> Y \<Longrightarrow> X \<rhd>\<rhd> es \<le> Y \<rhd>\<rhd> es"
  with H show "X \<rhd>\<rhd> (e # es) \<le> Y \<rhd>\<rhd> (e # es)" by simp
qed simp_all

lemma \<A>_mono: "mono (\<lambda> X. X \<rhd> e)" by (simp add: monoI \<A>_mono')
lemma \<A>t_mono: "mono (\<lambda> X. X \<rhd>+ e)" by (simp add: monoI \<A>t_mono')
lemma \<A>f_mono: "mono (\<lambda> X. X \<rhd>- e)" by (simp add: monoI \<A>f_mono')
lemma \<A>s_mono: "mono (\<lambda> X. X \<rhd>\<rhd> e)" by (simp add: monoI \<A>s_mono')
lemma \<A>\<T>_mono: "mono (\<lambda> X. X \<hookrightarrow> e)" by (simp add: monoI \<A>\<T>_mono')

lemma loop_computation_le0:
  fixes e b :: "('b, 't) expression"
  shows "loop_computation e b A \<le> A"
proof -
  have "\<And>X. A \<sqinter> (X \<rhd>- e \<rhd> b) \<le> A" by simp
  then show ?thesis
    by (metis gfp_least loop_function_def loop_operator_def topset_inter_subset_iff)
qed  

lemma loop_step_mono: "mono (loop_step e b)"
  by (simp add: \<A>_mono' \<A>f_mono' monoI)

lemma loop_computation_mono: "mono (loop_computation e b)"
  by (metis loop_operator_mono' monoI)

lemma loop_computation_le1:
  fixes e b :: "('b, 't) expression"
  shows "loop_computation e b A \<le> A \<rhd>- e \<rhd> b"
proof -
  let ?f = "(\<lambda>X. X \<rhd>- e \<rhd> b)"
  have
    lHm: "mono ?f" by (simp add: \<A>_mono' \<A>f_mono' monoI)
  then have "loop_operator ?f A = loop_function ?f A (loop_operator ?f A)"
    by (rule loop_operator_unfold)
  also from lHm have "\<dots> \<le> loop_function ?f A A"
    using loop_function_mono2 loop_operator_le0 by (metis monoD)
  finally show ?thesis by (simp add: topset_inter_subset_iff)
qed

lemma loop_application1: "A \<rhd> until e loop b end \<le> A \<rhd>- e \<rhd> b \<rhd> until e loop b end"
proof -
  let ?f = "(\<lambda>X. X \<rhd>- e \<rhd> b)"
  have "mono ?f" by (simp add: \<A>_mono' \<A>f_mono' monoI)
  then have "loop_operator ?f A \<le> loop_operator ?f (?f A)" by (rule loop_operator_le1)
  then show ?thesis by (simp add: \<A>_mono' \<A>t_mono')
qed

lemma
  \<A>_to_all [simp]: "\<A> e \<top> = \<top>" and
  \<A>t_to_all [simp]: "\<A>t e \<top> = \<top>" and
  \<A>f_to_all [simp]: "\<A>f e \<top> = \<top>"
proof (induction e)
case (Loop b c)
  then show "\<A> (until b loop c end) \<top> = \<top>" by (simp_all add: topset_Union_def gfp_def)
  then show "\<A>t (until b loop c end) \<top> = \<top>" by (simp_all add: topset_Union_def gfp_def)
  then show "\<A>f (until b loop c end) \<top> = \<top>" by (simp_all add: topset_Union_def gfp_def)
case (Test t e n)
  then show
    lHt: "\<A> (attached t e as n) \<top> = \<top>" by (cases e) simp_all
  then show "\<A>t (attached t e as n) \<top> = \<top>" by (cases e) simp_all
  from lHt show "\<A>f (attached t e as n) \<top> = \<top>" by simp
case (Call e f a)
  then show "\<top> \<rhd> e \<^bsub>\<bullet>\<^esub> f (a) = \<top>" by (induction a, simp_all)
  then show "\<top> \<rhd>+ e \<^bsub>\<bullet>\<^esub> f (a) = \<top>" by simp
  then show "\<top> \<rhd>- e \<^bsub>\<bullet>\<^esub> f (a) = \<top>" by simp
qed simp_all

lemma reachability:
  assumes
    HR: "B \<rhd> e \<noteq> \<top>" and
    HS: "A \<le> B"
  shows
    "A \<rhd> e \<noteq> \<top>"
proof
  assume
    lH: "A \<rhd> e = \<top>"
  from HS have "A \<rhd> e \<le> B \<rhd> e" by (rule \<A>_mono')
  with lH have "\<top> \<le> B \<rhd> e" by simp
  then have "B \<rhd> e = \<top>" using topset_top_le by simp
  with HR show False by simp
qed

lemma
  scope_true_ge: "A \<rhd> e \<le> A \<rhd>+ e" and
  scope_false_ge: "A \<rhd> e \<le> A \<rhd>- e"
proof (induction e arbitrary: A)
  fix A
  case (Test t e n)
  then show "A \<rhd> (attached t e as n) \<le> A \<rhd>+ (attached t e as n)"
    by (cases e) (simp_all add: topset_add_insert topset_subset_insertI2)
  then show "A \<rhd> (attached t e as n) \<le> A \<rhd>- (attached t e as n)" by simp
next
  case (If b c1 c2)
  fix A
  show "A \<rhd> (if b then c1 else c2 end) \<le> A \<rhd>+ (if b then c1 else c2 end)" using If.IH
    by (metis \<A>_If \<A>t_If eq_iff order.trans topset_inter_lower1 topset_inter_lower2)
  show "A \<rhd> (if b then c1 else c2 end) \<le> A \<rhd>- (if b then c1 else c2 end)" using If.IH
    by (metis \<A>_If \<A>f_If eq_iff order.trans topset_inter_lower1 topset_inter_lower2) 
qed simp_all

lemma
  reachability_with_scope_true: "A \<rhd>+ e \<noteq> \<top> \<longrightarrow> A \<rhd> e \<noteq> \<top>" and
  reachability_with_scope_false: "A \<rhd>- e \<noteq> \<top> \<longrightarrow> A \<rhd> e \<noteq> \<top>"
  by (metis scope_true_ge topset_top_le) (metis scope_false_ge topset_top_le)

(* The following lemmas are false for
    
  "c = if False\<^sub>c then unit ;; False\<^sub>c else Exception end"

lemma
  reachability_with_scope_true: "A \<rhd> c \<noteq> \<top> \<longrightarrow> A \<rhd>+ c \<noteq> \<top>"
nitpick

lemma
  reachability_with_scope_true: "A \<rhd> c \<noteq> \<top> \<longleftrightarrow> A \<rhd>+ c \<noteq> \<top>" and
  reachability_with_scope_false: "A \<rhd> c \<noteq> \<top> \<longleftrightarrow> A \<rhd>- c \<noteq> \<top>"
*)

lemma attachment_loop_condition: "A \<rhd> until e loop b end \<le> A \<rhd>+ e"
proof -
  let ?F = "\<lambda> B. B \<rhd>- e \<rhd> b"
  have "mono ?F" by (simp add: \<A>_mono' \<A>f_mono' monoD monoI topset_inter_mono_arg2)
  then have "loop_computation e b A \<le> loop_function ?F A (loop_computation e b A)"
  	using loop_operator_unfold by (metis order_refl)
  then have "loop_computation e b A \<le> A" by (simp add: topset_inter_subset_iff)
  then have ?thesis by (simp add: \<A>_mono' \<A>t_mono')
  moreover have "A \<rhd>+ e = \<top>" if "is_false e" using unreachable_if_false that by blast
  then have "A \<rhd> until e loop b end = A \<rhd>+ e" if "is_false e"
  	using unreachable_if_false that by simp
  ultimately show ?thesis by simp
qed

end