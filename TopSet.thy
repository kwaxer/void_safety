theory TopSet imports
	"~~/src/HOL/Lattice/Orders"
	Common
begin

datatype 'a topset =
  Top |
  Set (the: "'a set") ("\<lceil>_\<rceil>")

declare [[coercion "\<lambda>x::'a set. Set x"]]

definition topset_subset :: "'a topset \<Rightarrow> 'a topset \<Rightarrow> bool"
where
  "topset_subset A B \<equiv> case B of Top \<Rightarrow> True | \<lceil>b\<rceil> \<Rightarrow> (case A of Top \<Rightarrow> False | \<lceil>a\<rceil> \<Rightarrow> a \<subseteq> b)"

instantiation topset :: (type) complete_lattice
begin

  definition less_eq_topset_def: "less_eq_topset \<equiv> topset_subset" 
  definition less_topset_def: "less n1 n2 \<equiv> (topset_subset n1 n2) \<and> \<not> (topset_subset n2 n1)"  

  definition topset_union :: "'a topset \<Rightarrow> 'a topset \<Rightarrow> 'a topset" (infixl "\<squnion>" 65)
    where
      "A \<squnion> B  \<equiv> case A of Top \<Rightarrow> Top | Set a \<Rightarrow> (case B of Top \<Rightarrow> Top | Set b \<Rightarrow> Set (a \<union> b))"
  
  definition topset_inter :: "'a topset \<Rightarrow> 'a topset \<Rightarrow> 'a topset" (infixl "\<sqinter>" 70)
    where
      "A \<sqinter> B  \<equiv> case A of Top \<Rightarrow> B | Set a \<Rightarrow> (case B of Top \<Rightarrow> A | Set b \<Rightarrow> Set (a \<inter> b))"
  
  definition topset_Union :: "'a topset set \<Rightarrow> 'a topset" where
    "topset_Union A = (if Top \<in> A then Top else \<lceil>\<Union> {a. \<lceil>a\<rceil> \<in> A}\<rceil>)"
  
  definition topset_Inter :: "'a topset set \<Rightarrow> 'a topset" where
    "topset_Inter A = (if (\<exists> a. \<lceil>a\<rceil> \<in> A) then \<lceil>\<Inter> {a. \<lceil>a\<rceil> \<in> A}\<rceil> else Top)"

  definition [simp]: "Inf = topset_Inter"
  definition [simp]: "Sup = topset_Union"
  definition [simp]: "bot = Set {}"
  definition [iff]: "top = Top"
  definition [simp]: "inf = topset_inter"
  definition [simp]: "sup = topset_union"
  instance proof
    fix x y :: "'a topset"
    show "(x < y) = (x \<le> y \<and> \<not> y \<le> x)" by (simp add: less_topset_def less_eq_topset_def)
  next
    fix x y :: "'a topset"
    show "x \<le> x" by (cases x, simp_all add: topset_subset_def less_eq_topset_def)
  next
    fix x y z :: "'a topset"
    assume "x \<le> y" "y \<le> z"
    then show "x \<le> z"
    	apply (cases x)
    	apply (cases y)
			apply simp
			apply (simp add: less_eq_topset_def topset_subset_def)
			apply (cases z)
			apply (simp add: less_eq_topset_def topset_subset_def)
			apply (cases y)
			apply (simp add: less_eq_topset_def topset_subset_def)
			by (metis less_eq_topset_def order.trans topset.simps(5) topset_subset_def)
  next
    fix x y :: "'a topset"
    assume "x \<le> y" "y \<le> x"
    then show "x = y"
      apply (cases x)
      apply (cases y)
      apply (simp_all add: topset_subset_def less_eq_topset_def)
      apply (cases y)
      apply simp_all
    done
  next
    fix x y :: "'a topset"
    show "inf x y \<le> x"
      apply (cases x)
      apply (simp add: topset_subset_def less_eq_topset_def)
      apply (cases y)
      apply (simp_all add: less_eq_topset_def topset_inter_def topset_subset_def)
    done
  next
    fix x y :: "'a topset"
    show "inf x y \<le> y"
      apply (cases y)
      apply (simp add: topset_subset_def less_eq_topset_def)
      apply (cases x)
      apply (simp_all add: topset_inter_def topset_subset_def less_eq_topset_def)
    done
  next
    fix x y z :: "'a topset"
    assume "x \<le> y" "x \<le> z"
    then show "x \<le> inf y z"
      apply (cases y)
      apply (simp add: topset_inter_def)
      apply (cases z)
      apply (simp add: topset_inter_def)
      apply (cases x)
      apply (simp_all add: topset_inter_def topset_subset_def less_eq_topset_def)
    done
  next
    fix x y :: "'a topset"
    show "x \<le> sup x y"
      by (simp add: less_eq_topset_def topset_union_def topset.case_eq_if topset_subset_def)
  next
    fix x y :: "'a topset"
    show "y \<le> sup x y"
      apply (cases x)
      apply (simp add: topset_subset_def topset_union_def less_eq_topset_def)
      apply (cases y)
      apply (simp_all add: topset_subset_def topset_union_def less_eq_topset_def)
    done
  next
    fix x y z:: "'a topset"
    assume "y \<le> x" "z \<le> x"
    then show "sup y z \<le> x"
      apply (cases y)
      apply (simp add: topset_union_def)
      apply (cases z)
      apply (simp add: topset_union_def)
      apply (cases x)
      apply (simp_all add: topset_subset_def topset_union_def less_eq_topset_def)
    done
  next
    fix x:: "'a topset" and A
    assume "x \<in> A"
    then show "Inf A \<le> x"
    proof (cases x)
      case Top then show ?thesis by (simp add: topset_subset_def less_eq_topset_def)
    next
      case (Set s)
      moreover then have "\<Inter> {a. \<lceil>a\<rceil> \<in> A} \<subseteq> s" using \<open>x \<in> A\<close> by blast
      ultimately show ?thesis using  \<open>x \<in> A\<close> by
        (auto simp add: topset_subset_def topset_Inter_def less_eq_topset_def)
    qed
  next
    fix z:: "'a topset" and A
    assume
      H: "\<And>x. x \<in> A \<Longrightarrow> z \<le> x"
    then show "z \<le> Inf A"
    proof (cases z)
      case Top with H show ?thesis
        by (metis Inf_topset_def less_eq_topset_def topset.simps(4) topset.simps(5) topset_Inter_def topset_subset_def)
    next
      case (Set s)
      assume "z = \<lceil>s\<rceil>"
      with H have "\<And>x. \<lceil>x\<rceil> \<in> A \<Longrightarrow> s \<le> x"
        using topset_subset_def less_eq_topset_def
  by (metis topset.simps(5))
      then have "s \<le> \<Inter> {x. \<lceil>x\<rceil> \<in> A}" by auto
      then have
        HH: "\<lceil>s\<rceil> \<le> \<lceil>\<Inter> {x. \<lceil>x\<rceil> \<in> A}\<rceil>" by (simp add: less_eq_topset_def topset_subset_def)
      show ?thesis
      proof (cases "Inf A")
        case Top then show ?thesis by (simp add: less_eq_topset_def topset_subset_def)
      next
        case (Set a)
        have "z = \<lceil>s\<rceil>" by (simp add: \<open>z = \<lceil>s\<rceil>\<close>)
        also have "\<dots> \<le> \<lceil>\<Inter> {x. \<lceil>x\<rceil> \<in> A}\<rceil>" using HH by simp
        also have "\<dots> = Inf A" using Set
          by (metis Inf_topset_def topset.distinct(1) topset_Inter_def)
        finally show ?thesis by simp
      qed
    qed
  next
    fix x:: "'a topset" and A
    assume "x \<in> A"
    then show "x \<le> Sup A"
    proof (cases x)
      case Top then show ?thesis using \<open>x \<in> A\<close> topset_Union_def
        by (simp add: less_eq_topset_def topset_subset_def)
    next
      case (Set s)
      moreover then have "s \<subseteq> \<Union> {a. \<lceil>a\<rceil> \<in> A}" using \<open>x \<in> A\<close> by blast
      ultimately show ?thesis using  \<open>x \<in> A\<close>
        by (simp add: topset_Union_def topset_subset_def less_eq_topset_def) 
    qed
  next
    fix z:: "'a topset" and A
    assume
      H: "\<And>x. x \<in> A \<Longrightarrow> x \<le> z"
    then show "Sup A \<le> z"
    proof (cases z)
      case Top with H show ?thesis by (simp add: topset_subset_def less_eq_topset_def)
    next
      case (Set s)
      assume "z = \<lceil>s\<rceil>"
      with H have "\<And>x. \<lceil>x\<rceil> \<in> A \<Longrightarrow> \<lceil>x\<rceil> \<le> z" by simp
      with Set have "\<And>x. \<lceil>x\<rceil> \<in> A \<Longrightarrow> x \<le> s" by (simp add: less_eq_topset_def topset_subset_def)
      then have "\<Union> {x. \<lceil>x\<rceil> \<in> A} \<le> s" by auto
      then have
        HH: "\<lceil>\<Union> {x. \<lceil>x\<rceil> \<in> A}\<rceil> \<le> \<lceil>s\<rceil>" by (simp add: topset_subset_def less_eq_topset_def)
      show ?thesis
      proof (cases "Sup A")
        case Top then show ?thesis using H HH Set topset_Union_def by force
      next
        case (Set a)
        then have "Sup A = \<lceil>\<Union> {x. \<lceil>x\<rceil> \<in> A}\<rceil>"
          by (metis Sup_topset_def topset.distinct(1) topset_Union_def)
        also have "\<dots> \<le> \<lceil>s\<rceil>" using HH by simp
        also have "\<dots> = z" by (simp add: \<open>z = \<lceil>s\<rceil>\<close>)
        finally show ?thesis by simp
      qed
    qed
  next
    show "Inf {} = (top:: 'a topset)" by (simp add: topset_Inter_def)
  next
    show "Sup {} = (bot:: 'a topset)" by (simp add: topset_Union_def)
  qed

  declare
    Inf_topset_def
    Sup_topset_def
    bot_topset_def
    inf_topset_def
    sup_topset_def [simp del]
  
  notation Top ("\<top>")

end

instantiation topset :: (type) distrib_lattice
begin
  instance proof
    fix x y z:: "'a topset"
    show "sup x (inf y z) = inf (sup x y) (sup x z)"
      apply (cases x)
      apply (metis inf_sup_absorb sup_top_left top_topset_def)
      apply (cases y)
      apply (metis inf_top_left sup_top_right top_topset_def)
      apply (cases z)
      apply (metis inf_top_right sup_top_right top_topset_def)
      apply (simp add: topset_inter_def topset_union_def Un_Int_distrib sup_topset_def)
    done
  qed
end

definition topset_member :: "'a \<Rightarrow> 'a topset \<Rightarrow> bool" (infix "\<in>\<^sup>\<top>" 50)
where
 "x \<in>\<^sup>\<top> A  \<equiv> (case A of Top \<Rightarrow> True | \<lceil>a\<rceil> \<Rightarrow> x \<in> a)"

lemma topset_member_top [simp]: "c \<in>\<^sup>\<top> \<top>"
  by (simp add: topset_member_def)

lemma topset_member_set [iff]: "c \<in>\<^sup>\<top> \<lceil>a\<rceil> \<longleftrightarrow> c \<in> a"
  by (simp add: topset_member_def)

definition topset_insert :: "'a \<Rightarrow> 'a topset \<Rightarrow> 'a topset" where
  "topset_insert a B = (case B of Top \<Rightarrow> Top | Set b \<Rightarrow> \<lceil>{x. x = a \<or> x \<in> b}\<rceil>)"

lemma topset_subsetI [intro!]: "\<lbrakk>\<And>x. x \<in>\<^sup>\<top> A \<Longrightarrow> x \<in>\<^sup>\<top> B; A \<noteq> \<top>\<rbrakk> \<Longrightarrow> A \<le> B"
proof -
  assume
    H1: "\<And>x. x \<in>\<^sup>\<top> A \<Longrightarrow> x \<in>\<^sup>\<top> B" and
    H2: "A \<noteq> \<top>"
  then obtain a where
    H3: "A = \<lceil>a\<rceil>" using topset.exhaust by auto
  show ?thesis
  proof (cases B)
    case Top then show ?thesis using top_greatest [of A] by simp
  next
    case (Set b)
    with H1 H3 topset_member_def have "\<And>x. x \<in> a \<Longrightarrow> x \<in> b" by fastforce
    then have "a \<subseteq> b" by auto
    with H3 Set show ?thesis by (simp add: less_eq_topset_def topset_subset_def)
  qed
qed

lemma topset_subsetD [elim, intro?]: "\<lbrakk>A \<le> B; c \<in>\<^sup>\<top> A\<rbrakk> \<Longrightarrow> c \<in>\<^sup>\<top> B"
proof (cases A)
  case Top
  assume
    Hle: "A \<le> B" and
    Hin: "c \<in>\<^sup>\<top> A" and
    HA: "A = Top"
  then have "A = \<top>" by simp
  with Hle have "B = \<top>" using dual_order.antisym by fastforce
  then show ?thesis using Hin HA by simp
next
  case (Set a)
  assume
    Hle: "A \<le> B" and
    Hin: "c \<in>\<^sup>\<top> A" and
    HA: "A = \<lceil>a\<rceil>"
  show ?thesis
  proof (cases B)
    case Top
    have "c \<in>\<^sup>\<top> \<top>" by (rule topset_member_top)
    with Top show ?thesis by simp
  next
    case HB: (Set b)
    with Hin Hle HA show ?thesis
      by (simp add: less_eq_topset_def set_mp topset_member_def topset_subset_def)
  qed
qed

lemma rev_topset_subsetD [intro?]: "\<lbrakk>c \<in>\<^sup>\<top> A; A \<le> B\<rbrakk> \<Longrightarrow> c \<in>\<^sup>\<top> B"
  by (simp add: topset_subsetD)

lemma topset_subsetCE [elim]: "A \<le> B \<Longrightarrow> (\<not> c \<in>\<^sup>\<top> A \<Longrightarrow> P) \<Longrightarrow> (c \<in>\<^sup>\<top> B \<Longrightarrow> P) \<Longrightarrow> P"
  \<comment> \<open>Classical elimination rule.\<close>
  by (meson topset_subsetD)

lemma topset_subset_eq: "A \<noteq> \<top> \<Longrightarrow> A \<le> B = (\<forall>x. x \<in>\<^sup>\<top> A \<longrightarrow> x \<in>\<^sup>\<top> B)" by blast

lemma contra_topset_subsetD: "A \<le> B \<Longrightarrow> \<not> c \<in>\<^sup>\<top> B \<Longrightarrow> \<not> c \<in>\<^sup>\<top> A" by blast


lemma topset_insert_top [simp]: "topset_insert x \<top> = \<top>"
  by (simp add: topset_insert_def)

lemma topset_insert_set: "topset_insert x \<lceil>a\<rceil> = \<lceil>insert x a\<rceil>"
  by (simp add: insert_compr topset_insert_def)

lemma topset_insertI1: "a \<in>\<^sup>\<top> topset_insert a B"
  by (cases B, simp_all add: topset_insert_def topset_member_def)

lemma topset_insertI2: "a \<in>\<^sup>\<top> B \<Longrightarrow> a \<in>\<^sup>\<top> topset_insert b B"
  by (cases B, simp_all add: topset_insert_def topset_member_def)

lemma topset_subset_insertI: "B \<le> topset_insert a B"
  by (cases B) (auto simp add: topset_insertI2)

lemma topset_subset_insertI2: "A \<le> B \<Longrightarrow> A \<le> topset_insert b B"
  apply (cases B)
  apply simp
by (meson order.trans topset_subset_insertI)

lemma topset_subset_insert: "\<not> x \<in>\<^sup>\<top> A \<Longrightarrow> (A \<le> topset_insert x B) = (A \<le> B)"
proof (cases A)
  assume
    H: "\<not> x \<in>\<^sup>\<top> A"
  case Top with H show ?thesis by (simp add: topset_member_def)
next
  case (Set a)
  assume
    H: "\<not> x \<in>\<^sup>\<top> A" and
    HA: "A = \<lceil>a\<rceil>"
  then have
    HM: "x \<notin> a" by (simp add: topset_member_def)
  show ?thesis
  proof (cases B)
    case Top then show ?thesis by (simp add: topset_member_def)
  next
    case (Set b)
    from HM have "(a \<subseteq> insert x b) = (a \<subseteq> b)" by (simp add: subset_insert)
    with HA Set show ?thesis
      by (simp add: less_eq_topset_def topset_insert_set topset_subset_def)
  qed
qed


lemma topset_le_top [simp]: "A \<le> \<top>"
  using top_greatest [of A] by simp

lemma topset_top_le [simp]: "\<top> \<le> A \<Longrightarrow> A = \<top>"
  by (simp add: dual_order.antisym)

lemma topset_union_assoc: "A \<squnion> (B \<squnion> C) = A \<squnion> B \<squnion> C"
  by (metis sup.assoc sup_topset_def)

lemma topset_inter_assoc: "A \<sqinter> (B \<sqinter> C) = A \<sqinter> B \<sqinter> C"
  by (metis inf.assoc inf_topset_def)

lemma topset_union_commute: "A \<squnion> B = B \<squnion> A"
  using sup.commute sup_topset_def by metis

lemma topset_inter_commute: "A \<sqinter> B = B \<sqinter> A"
  using inf.commute inf_topset_def by metis

lemma topset_union_idem [simp]: "A \<squnion> A = A"
  by (metis sup.idem sup_topset_def)

lemma topset_inter_idem [simp]: "A \<sqinter> A = A"
  by (metis inf.idem inf_topset_def)

lemma topset_union_bot_right [simp]: "A \<squnion> \<lceil>{}\<rceil> = A"
  by (simp add: topset.case_eq_if topset_union_def)

lemma topset_union_bot_left [simp]: "\<lceil>{}\<rceil> \<squnion> A  = A"
  by (simp add: topset.case_eq_if topset_union_def)

lemma topset_inter_bot_right [simp]: "A \<sqinter> \<lceil>{}\<rceil> = \<lceil>{}\<rceil>"
  by (metis bot_topset_def inf_bot_right inf_topset_def)

lemma topset_inter_bot_left [simp]: "\<lceil>{}\<rceil> \<sqinter> A  = \<lceil>{}\<rceil>"
  by (metis bot_topset_def inf_bot_left inf_topset_def)

lemma
  topset_union_top_right [simp]: "A \<squnion> \<top> = \<top>" and
  topset_union_top_left [simp]: "\<top> \<squnion> A = \<top>"
  by (cases A) (simp_all add: topset_union_def)

lemma topset_union_topD1: "A \<squnion> B \<noteq> \<top> \<Longrightarrow> A \<noteq> (\<top>:: 'a topset)"
  by auto

lemma topset_union_topD2: "A \<squnion> B \<noteq> \<top> \<Longrightarrow> B \<noteq> (\<top>:: 'a topset)"
  by auto

lemma topset_union_topI [intro]: "\<lbrakk>A \<noteq> \<top>; B \<noteq> \<top>\<rbrakk> \<Longrightarrow> A \<squnion> B \<noteq> (\<top>:: 'a topset)"
  by (simp add: topset.case_eq_if topset_union_def)

lemma
  topset_inter_top_right [simp]: "A \<sqinter> \<top> = A" and
  topset_inter_top_left [simp]: "\<top> \<sqinter> A = A"
  by (cases A) (simp_all add: topset_inter_def)

lemma topset_inter_topD1: "A \<sqinter> B = \<top> \<Longrightarrow> A = (\<top>:: 'a topset)"
  apply (cases A)
  apply simp
  apply (cases B)
  apply (simp_all add: topset_inter_def)
done

lemma topset_inter_topD2: "A \<sqinter> B = \<top> \<Longrightarrow> B = (\<top>:: 'a topset)"
  apply (cases B)
  apply simp
  apply (cases A)
  apply (simp_all add: topset_inter_def)
done

lemma topset_inter_subset_iff: "C \<le> A \<sqinter> B = (C \<le> A \<and> C \<le> B)"
proof -
  have "(C \<le> inf A B) = (C \<le> A \<and> C \<le> B)" by (fact le_inf_iff)
  then show ?thesis by simp
qed

definition topset_add :: "'a topset \<Rightarrow> 'a \<Rightarrow> 'a topset" (infixl "\<oplus>" 65)
where
  "A \<oplus> b \<equiv> case A of Top \<Rightarrow> Top | Set a \<Rightarrow> \<lceil>insert b a\<rceil>"

definition topset_rem :: "'a topset \<Rightarrow> 'a \<Rightarrow> 'a topset" (infixl "\<ominus>" 65)
where
  "A \<ominus> b \<equiv> case A of Top \<Rightarrow> Top | Set a \<Rightarrow> \<lceil>a - {b}\<rceil>"

lemma topset_add_insert: "A \<oplus> b = topset_insert b A"
  by (cases A) (simp_all add: topset_add_def topset_insert_set)

lemma topset_add_union: "A \<oplus> b = A \<squnion> \<lceil>{b}\<rceil>"
  by (cases A) (simp_all add: topset_add_insert topset_insert_set topset_union_def)

lemma topset_top_add [simp]: "\<top> \<oplus> x = \<top>"
  by (simp add: topset_add_def)

lemma topset_top_rem [simp]: "\<top> \<ominus> x = \<top>"
  by (simp add: topset_rem_def)

lemma topset_set_add: "\<lceil>a\<rceil> \<oplus> b = \<lceil>insert b a\<rceil>"
  by (simp add: topset_add_def)

lemma topset_set_rem: "\<lceil>a\<rceil> \<ominus> b = \<lceil>a - {b}\<rceil>"
  by (simp add: topset_rem_def)

lemma topset_add_subset: "A \<le> A \<oplus> b"
proof (cases A)
  case Top then show ?thesis using topset_member_top by simp
next
  case (Set a)
  have "a \<subseteq> insert b a" by (rule subset_insertI)
  then have "\<lceil>a\<rceil> \<le> topset_insert b \<lceil>a\<rceil>" using topset_subset_insertI by fastforce
  with Set show ?thesis using topset_set_add by (simp add: topset_add_insert)
qed

lemma topset_rem_subset: "A \<ominus> b \<le> A"
  by (cases A) (simp_all add: Diff_subset topset_subset_def topset_set_rem less_eq_topset_def)

lemma topset_add_absorb [iff]: "A \<oplus> b = \<top> \<longleftrightarrow> A = \<top>"
  by (simp add: topset.case_eq_if topset_add_def)

lemma topset_rem_absorb [iff]: "A \<ominus> b = \<top> \<longleftrightarrow> A = \<top>"
  by (simp add: topset.case_eq_if topset_rem_def)

lemma topset_add_right_commute [iff]: "A \<oplus> x \<oplus> y = A \<oplus> y \<oplus> x"
	by (metis topset_add_union topset_union_assoc topset_union_commute)

lemma topset_rem_right_commute [iff]: "A \<ominus> x \<ominus> y = A \<ominus> y \<ominus> x"
	by (cases A) (simp, metis Diff_insert Diff_insert2 topset_set_rem)

lemma topset_union_upper1: "A \<le> A \<squnion> B"
  by (metis sup_ge1 sup_topset_def)

lemma topset_union_upper2: "B \<le> A \<squnion> B"
  by (metis sup_ge2 sup_topset_def)

lemma topset_inter_lower1 [simp]: "A \<sqinter> B \<le> A"
  by (metis inf_le1 inf_topset_def)

lemma topset_inter_lower2 [simp]: "A \<sqinter> B \<le> B"
  by (metis inf_le2 inf_topset_def)

lemma topset_insert_absorb: "a \<in>\<^sup>\<top> A \<Longrightarrow> topset_insert a A = A"
  by (cases A) (simp_all add: insert_absorb topset_insert_set topset_member_def)

lemma topset_union_mono [simp]:
  assumes "A \<le> B" and "C \<le> D"
  shows "A \<squnion> C \<le> B \<squnion> D"
using assms
  by (metis sup_mono sup_topset_def)

lemma topset_union_mono1 [simp]:
  assumes "A \<le> B"
  shows "A \<squnion> C \<le> B \<squnion> C"
proof -
  have "C \<le> C" by simp
  from assms this show ?thesis by (rule topset_union_mono)
qed

lemma topset_union_mono2 [simp]:
  assumes "A \<le> B"
  shows "C \<squnion> A \<le> C \<squnion> B"
proof -
  have "C \<le> C" by simp
  from this assms show ?thesis by (rule topset_union_mono)
qed

lemma topset_inter_mono [simp]:
  assumes "A \<le> B" and "C \<le> D"
  shows "A \<sqinter> C \<le> B \<sqinter> D"
using assms
  by (metis inf_mono inf_topset_def)

lemma topset_inter_mono1 [simp]:
  assumes "A \<le> B"
  shows "A \<sqinter> C \<le> B \<sqinter> C"
proof -
  have "C \<le> C" by simp
  from assms this show ?thesis by (rule topset_inter_mono)
qed

lemma topset_inter_mono2 [simp]:
  assumes "A \<le> B"
  shows "C \<sqinter> A \<le> C \<sqinter> B"
proof -
  have "C \<le> C" by simp
  from this assms show ?thesis by (rule topset_inter_mono)
qed

lemma topset_inter_mono_arg1: "mono (\<lambda> A. A \<sqinter> B)"
  by (simp add: monoI)

lemma topset_inter_mono_arg2: "mono (\<lambda> B. A \<sqinter> B)"
  by (simp add: monoI)

lemma topset_add_mono: "A \<le> B \<Longrightarrow> A \<oplus> x \<le> B \<oplus> x"
proof -
  assume
    HS: "A \<le> B"
  then have
    "x \<in>\<^sup>\<top> A \<oplus> x"
    "x \<in>\<^sup>\<top> B \<oplus> x" by (simp_all add: topset_add_insert topset_insertI1)
  with HS have
    HS': "\<forall> y. y \<in>\<^sup>\<top> A \<oplus> x \<longrightarrow> y \<in>\<^sup>\<top> B \<oplus> x"
      by (auto simp add: topset.case_eq_if topset_add_def topset_member_def)
  show ?thesis
  proof (cases "A")
    case Top
    then have "B = \<top>" using HS by simp
    then show ?thesis by simp
  next
    case Set
    then show ?thesis by (simp add: HS' topset_subsetI)
  qed
qed

lemma topset_rem_mono: "A \<le> B \<Longrightarrow> A \<ominus> x \<le> B \<ominus> x"
proof (cases A)
  case Top then show "A \<le> B \<Longrightarrow> A \<ominus> x \<le> B \<ominus> x" by (simp add: topset.case_eq_if topset_rem_def)
next
  case (Set a)
  assume
    H: "A \<le> B" and
    HA: "A = \<lceil>a\<rceil>"
  show ?thesis
  proof (cases B)
    case Top then show ?thesis using topset_member_top by simp
  next
    case (Set b)
    with H HA have "a \<subseteq> b" by (simp add: topset_subset_def less_eq_topset_def)
    then have "a - {x} \<subseteq> b - {x}" by (simp add: Diff_mono)
    with HA Set show ?thesis by (simp add: topset_rem_def topset_subset_def less_eq_topset_def)
  qed
qed

lemma topset_add_rem_mono: "A \<le> B \<Longrightarrow> A \<ominus> x \<le> B \<oplus> y"
proof-
  assume
    H: "A \<le> B"
  have "A \<ominus> x \<le> A" by (rule topset_rem_subset)
  also have "A \<le> B" by (rule H)
  also have "B \<le> B \<oplus> y" by (rule topset_add_subset)
  finally show ?thesis by simp
qed

lemma topset_add_rem_mono1: "A \<le> B \<Longrightarrow> A \<ominus> x \<le> B \<oplus> x"
  using topset_add_rem_mono by (simp add: less_eq_topset_def)

lemma topset_inter_union_distrib: "A \<sqinter> (B \<squnion> C) = (A \<sqinter> B) \<squnion> (A \<sqinter> C)"
  using inf_sup_distrib1 by (metis inf_topset_def sup_topset_def)

lemma topset_inter_union_distrib2: "(B \<squnion> C) \<sqinter> A = (B \<sqinter> A) \<squnion> (C \<sqinter> A)"
  by (simp add: topset_inter_commute topset_inter_union_distrib)

lemma topset_union_inter_distrib: "A \<squnion> (B \<sqinter> C) = (A \<squnion> B) \<sqinter> (A \<squnion> C)"
  using sup_inf_distrib1 by (metis inf_topset_def sup_topset_def)

lemma topset_union_inter_distrib2: "(B \<sqinter> C) \<squnion> A = (B \<squnion> A) \<sqinter> (C \<squnion> A)"
  by (simp add: topset_union_commute topset_union_inter_distrib)

lemma topset_inter_add_distrib: "(A \<oplus> c) \<sqinter> (B \<oplus> c) = (A \<sqinter> B) \<oplus> c"
  by (simp add: topset_add_union topset_union_inter_distrib2)

lemma topset_inter_rem_distrib: "(A \<ominus> c) \<sqinter> (B \<ominus> c) = (A \<sqinter> B) \<ominus> c"
proof (cases A)
  case Top then show ?thesis by simp
next
  case HA: (Set a)
  thus ?thesis
  proof (cases B)
    case Top
    then have "B \<ominus> c = \<top>" and "A \<sqinter> B = A" by simp_all
    then show ?thesis using topset_inter_def topset_rem_def
    by simp
  next
    case HB: (Set b)
    from HA have "A \<ominus> c = \<lceil>a - {c}\<rceil>" using topset_set_rem by simp
    moreover from HB have "B \<ominus> c = \<lceil>b - {c}\<rceil>" using topset_set_rem by simp
    ultimately have "(A \<ominus> c) \<sqinter> (B \<ominus> c) = \<lceil>a - {c}\<rceil> \<sqinter> \<lceil>b - {c}\<rceil>" by simp
    also have "\<dots> = \<lceil>(a - {c}) \<inter> (b - {c})\<rceil>" by (simp add: topset_inter_def)
    also have "\<dots> = \<lceil>(a \<inter> b) - {c}\<rceil>" by blast
    moreover from HA HB have "A \<sqinter> B = \<lceil>a \<inter> b\<rceil>" by (simp add: topset_inter_def)
    then have "(A \<sqinter> B) \<ominus> c = \<lceil>(a \<inter> b) - {c}\<rceil>" using topset_set_rem by simp
    ultimately show ?thesis by simp
  qed
qed

lemma topset_inter_rem: "B \<noteq> \<top> \<Longrightarrow> A \<sqinter> (B \<ominus> c) = (A \<sqinter> B) \<ominus> c"
  apply (cases A)
  apply simp
  apply (cases B)
  apply (simp_all add: Int_Diff topset_inter_def topset_set_rem)
done

lemma topset_add_rem: "A \<oplus> x \<ominus> x = A \<ominus> x"
  by (simp add: topset_add_def topset_rem_def topset_union_def "topset.case_eq_if")

(*
lemma topset_subset_iff: "A \<le> B \<longleftrightarrow> A \<le> (B:: 'a topset )"
  by (simp add: less_eq_topset_def)
*)

lemma topset_gfp_inter_left: "gfp (\<lambda> x. A \<sqinter> f x) \<le> A"
proof -
    have "gfp (\<lambda> x. A \<sqinter> f x) \<le> gfp (\<lambda> B. A)" by (simp add: gfp_mono)
    then show ?thesis by (simp add: gfp_unfold monoI)
qed

lemma topset_lfp_inter_left: "lfp (\<lambda> x. A \<sqinter> f x) \<le> A"
proof -
    have "lfp (\<lambda> x. A \<sqinter> f x) \<le> lfp (\<lambda> B. A)" by (simp add: lfp_mono)
    then show ?thesis by (simp add: lfp_unfold monoI)
qed

end
