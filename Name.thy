section "Locals"

theory Name imports
	Main
begin

-- "Local variable name"
type_synonym vname = string

-- "Feature name"
type_synonym fname = string

end