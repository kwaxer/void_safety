theory LoopOperator imports
  TopSet
begin

subsubsection "Loop operator"

definition "loop_function f A = (\<lambda> X. A \<sqinter> f X)"
declare loop_function_def [simp]

lemma loop_function_mono1: "mono (loop_function f)"
  by (simp add: le_funI monoI)

lemma loop_function_mono2: "mono f \<Longrightarrow> mono (loop_function f A)"
  by (simp add: mono_def)

definition "loop_operator f A = gfp (loop_function f A)"
declare loop_operator_def [simp]

lemma loop_operator_mono: "mono (loop_operator f)"
  by (rule monoI) (simp add: gfp_mono)

lemma loop_operator_mono': "A \<le> B \<Longrightarrow> loop_operator f A \<le> loop_operator f B"
  using loop_operator_mono by (rule monoD) simp

lemma loop_operator_unfold: "mono f \<Longrightarrow> loop_operator f A = loop_function f A (loop_operator f A)"
proof -
  let ?F = "loop_function f A"
  assume
    "mono f"
  then have "mono ?F" using loop_function_mono2 by simp
  then have "gfp ?F = ?F (gfp ?F)" by (rule gfp_unfold)
  then show ?thesis by simp
qed

lemma loop_operator_idem:
  assumes "mono f"
  shows "loop_operator f (loop_operator f x) = loop_operator f x"
proof -
  { fix tt :: "'a topset"
    obtain tta :: "('a topset \<Rightarrow> 'a topset) \<Rightarrow> 'a topset" and ttb :: "('a topset \<Rightarrow> 'a topset) \<Rightarrow> 'a topset" where
      ff1: "\<forall>f. tta f \<le> ttb f \<and> \<not> f (tta f) \<le> f (ttb f) \<or> mono f"
      by (metis (no_types) monoI)
    have ff2: "\<forall>f. \<not> mono f \<or> Sup {t. (t :: 'a topset) \<le> f t} \<le> f (Sup {t. t \<le> f t})"
      by (metis gfp_def gfp_lemma2)
    have ff3: "\<forall>t f. \<not> (t :: 'a topset) \<le> f t \<or> t \<le> Sup {t. t \<le> f t}"
      by (metis (full_types) gfp_def gfp_upperbound)
    have ff4: "\<forall>t ta. (t :: 'a topset) \<sqinter> ta \<le> ta"
      by (metis order_refl topset_inter_subset_iff)
    have ff5: "\<forall>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t \<le> Sup {t. t \<le> x \<sqinter> f t}"
      by (simp add: gfp_def)
    have ff6: "\<forall>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t \<le> f t"
      using ff4 by metis
    have ff7: "\<forall>t ta. (t :: 'a topset) \<sqinter> ta = ta \<sqinter> t"
      using ff4 by (simp add: eq_iff topset_inter_subset_iff)
    have ff8: "\<forall>fa. x \<sqinter> f (tta fa) \<le> x \<sqinter> f (ttb fa) \<or> mono fa"
      using ff4 ff1 by (metis (no_types) assms dual_order.trans monoD topset_inter_lower1 topset_inter_subset_iff)
    then have ff9: "mono (\<lambda>t. x \<sqinter> f t)"
      using ff1 by metis
    then have ff10: "Sup {t. t \<le> x \<sqinter> f t} \<le> x \<sqinter> f (Sup {t. t \<le> x \<sqinter> f t})"
      using ff2 by meson
    then have ff11: "Sup {t. t \<le> x \<sqinter> f t} \<le> x"
      by (meson topset_inter_subset_iff)
    then have ff12: "\<forall>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t \<le> x"
      using ff5 by (meson dual_order.trans)
    then have ff13: "\<forall>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t) \<le> f x"
      using ff6 by (meson assms dual_order.trans monoD)
    have "\<forall>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t \<le> f x"
      using ff10 ff9 ff5 by (meson dual_order.trans monoD topset_inter_subset_iff)
    then have ff14: "\<forall>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x"
      using ff5 by (simp add: gfp_def topset_inter_subset_iff)
    have "Sup {t. t \<le> x \<sqinter> f t} \<le> f x"
      using ff11 ff10 ff9 by (meson dual_order.trans monoD topset_inter_subset_iff)
    then have "Sup {t. t \<le> x \<sqinter> f t} \<le> f x \<sqinter> Sup {t. t \<le> x \<sqinter> f t}"
      by (meson order_refl topset_inter_subset_iff)
    then have ff15: "Sup {t. t \<le> x \<sqinter> f t} = gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x"
      using ff7 by (metis (no_types) eq_iff gfp_def topset_inter_lower1)
    then have ff16: "\<forall>t ta. \<not> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t \<le> ta \<or> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t \<le> ta \<sqinter> (gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x)"
      by (simp add: gfp_def topset_inter_subset_iff)
    have ff17: "gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x)"
      using ff15 ff10 by (simp add: gfp_def topset_inter_subset_iff)
    then have ff18: "gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x \<le> Sup {t. t \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t}"
      using ff3 by meson
    have ff19: "gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x = gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x)"
      using ff17 ff13 ff5 by (simp add: eq_iff gfp_def topset_inter_subset_iff)
    have ff20: "\<forall>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t = x \<sqinter> f t \<sqinter> (gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x)"
      using ff16 ff12 by (meson eq_iff topset_inter_subset_iff)
    have "\<forall>fa. mono fa \<or> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (tta fa) \<le> x \<sqinter> f (ttb fa) \<sqinter> (gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x)"
      using ff16 ff12 ff8 ff6 by (meson dual_order.trans topset_inter_subset_iff)
    then have ff21: "mono (\<lambda>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t)"
      using ff20 ff1 by (metis (no_types))
    then have ff22: "gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x) \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (Sup {t. t \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t})"
      using ff18 by (meson monoD)
    have ff23: "Sup {t. t \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t} \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (Sup {t. t \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t})"
      using ff21 ff2 by meson
    have "gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x = gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f (Sup {t. t \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t})"
      using ff22 ff19 ff14 eq_iff by auto
    then have "Sup {t. t \<le> gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t} = gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f x"
      using ff23 ff18 by auto
    then have "gfp (\<lambda>t. x \<sqinter> f t) = gfp (\<lambda>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t) \<or> x \<sqinter> f tt = gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f tt"
      using ff15 by (simp add: gfp_def) }
  then have "gfp (\<lambda>t. gfp (\<lambda>t. x \<sqinter> f t) \<sqinter> f t) = gfp (\<lambda>t. x \<sqinter> f t)"
    by metis
  then show ?thesis by (simp only: loop_operator_def loop_function_def)
qed

lemma loop_operator_le0: "mono f \<Longrightarrow> loop_operator f A \<le> A"
  by (metis eq_iff loop_function_def loop_operator_unfold topset_inter_subset_iff)

lemma loop_operator_le1: "mono f \<Longrightarrow> loop_operator f A \<le> loop_operator f (f A)"
by (simp only: loop_operator_def loop_function_def)
    (smt gfp_least gfp_upperbound mono_sup sup.absorb_iff2 sup.boundedE topset_inter_subset_iff)

end