theory Lattice_Prop imports
	"~~/src/HOL/Lattice/Orders"
begin

lemma power_inf_le_Inf_power:
  "mono f \<Longrightarrow> ((\<lambda> x. inf A (f x)) ^^ n) (A::'a::complete_lattice) \<le> Inf {(f ^^ i) A | i. i \<le> n}"
proof (induction n)
  case 0
  show ?case by auto
next
  case (Suc n)
  have "{((f ^^ (Suc i)) A) |i. i \<le> n} = {((f ^^ i) A) |i. i \<le> Suc n \<and> i \<noteq> 0}"
    using not0_implies_Suc by blast
  then have "{((f ^^ (Suc i)) A) |i. i \<le> n} \<union> {A} = {((f ^^ i) A) |i. i \<le> Suc n \<and> i \<noteq> 0} \<union> {A}"
    by simp
  also have "\<dots> = {((f ^^ i) A) |i. i \<le> Suc n \<and> i \<noteq> 0} \<union> {((f ^^ 0) A)}" by simp
  also have "\<dots> = {((f ^^ i) A) |i. i \<le> Suc n \<and> i \<noteq> 0} \<union> {((f ^^ i) A) |i. i = 0}" by simp
  also have "\<dots> = {((f ^^ i) A) |i. i \<le> Suc n}"
  proof -
    {
      fix f :: "nat \<Rightarrow> 'a" and m n :: nat
      assume "m \<le> n"
      then have "{f i | i. i \<le> n} = {f i | i. i \<le> n \<and> i \<noteq> m} \<union> {f m}" by auto
    }
    note this [of 0 "Suc n" "\<lambda> i. (f ^^ i) A"]
    moreover have "0 \<le> Suc n" by simp
    ultimately have
      "{((f ^^ i) A) |i. i \<le> Suc n} = {((f ^^ i) A) |i. i \<le> Suc n \<and> i \<noteq> 0} \<union> {((f ^^ 0) A)}"
      by simp
    thus ?thesis by simp
  qed
  finally have 
    **: "{((f ^^ (Suc i)) A) |i. i \<le> n} \<union> {A} = {((f ^^ i) A) |i. i \<le> Suc n}" by simp
  from Suc.prems have
    *: "(f (Inf {(f ^^ i) A |i. i \<le> n})) \<le> Inf {f ((f ^^ i) A) |i. i \<le> n}"
    by (smt Inf_greatest Inf_lower mem_Collect_eq mono_def)
  then have
    "\<dots> = Inf {((f ^^ (Suc i)) A) |i. i \<le> n}" by simp
  have "((\<lambda>x. inf A (f x)) ^^ Suc n) A = (\<lambda>x. inf A (f x)) (((\<lambda>x. inf A (f x)) ^^ n) A)" by simp
  also with Suc have "\<dots> \<le> inf A (f (Inf {(f ^^ i) A |i. i \<le> n}))"
    by (meson inf_mono monoD order_refl)
  also with * have "\<dots> \<le> inf A (Inf ({f ((f ^^ i) A) |i. i \<le> n}))" using inf_mono by auto
  also have "\<dots> = inf A (Inf ({((f ^^ (Suc i)) A) |i. i \<le> n}))" by simp
  also have "\<dots> = Inf ({((f ^^ (Suc i)) A) |i. i \<le> n} \<union> {A})" by simp
  also have "\<dots> = Inf ({((f ^^ i) A) |i. i \<le> Suc n})" using ** by simp
  finally show ?case by simp
qed

end
