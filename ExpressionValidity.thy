theory ExpressionValidity imports
	EnvironmentAttachment
	TransferFunction
begin

subsubsection "Attachment validity rules and type checks"

text {* A predicate that tells if an expression is valid with respect to attachment rules
and what is the expected type of the expression. *}

definition declared_attachment_type_of_local :: "attachment_environment \<Rightarrow> vname \<Rightarrow> attachment_type option" ("\<T>\<^sub>l") where
	"\<T>\<^sub>l \<Gamma> n = (case \<Gamma> n of None \<Rightarrow> None | Some T \<Rightarrow> Some (type_attachment (\<lambda> x. False) T))"
	  -- {* TODO: Provide implementation for @{term is_expanded} *}
declare declared_attachment_type_of_local_def [simp]

fun declared_attachment_type_of :: "attachment_environment \<Rightarrow> ('b, 't) expression \<Rightarrow> attachment_type" ("\<T>d") where
	"\<T>d _ (\<C> v) = \<T>\<^sub>c v" |
	"\<T>d \<Gamma> (\<V> n) = (case \<T>\<^sub>l \<Gamma> n of None \<Rightarrow> Detachable | Some T \<Rightarrow> T)" |
	"\<T>d \<Gamma> (if b then c1 else c2 end) = upper_bound (\<T>d \<Gamma> c1) (\<T>d \<Gamma> c2)" |
	"\<T>d _ _ = Attached"


inductive
	AT :: "attachment_environment \<Rightarrow> vname topset \<Rightarrow> ('b, 't) expression \<Rightarrow> attachment_type \<Rightarrow> bool" ("_, _ \<turnstile> _ : _" [60, 54, 0, 60] 60) and
	ATs :: "attachment_environment \<Rightarrow> vname topset \<Rightarrow> ('b, 't) expression list \<Rightarrow> attachment_type list \<Rightarrow> bool" ("_, _ \<turnstile> _ [:] _" [60, 54, 0, 60] 60)
where
	AT_ValueDet: "v = Void\<^sub>v \<Longrightarrow> \<Gamma>, A \<turnstile> \<C> v: Detachable" |
	AT_ValueAtt: "v \<noteq> Void\<^sub>v \<Longrightarrow> \<Gamma>, A \<turnstile> \<C> v: Attached" |
	AT_LocalAtt: "n \<in>\<^sup>\<top> A \<Longrightarrow> \<Gamma>, A \<turnstile> \<V> n: Attached" |
	AT_LocalDet: "\<not> n \<in>\<^sup>\<top> A \<Longrightarrow> \<Gamma>, A \<turnstile> \<V> n: Detachable" |
	AT_Seq: "\<lbrakk>\<Gamma>, A \<turnstile> e\<^sub>1: Attached; \<Gamma>, \<A> e\<^sub>1 A \<turnstile> e\<^sub>2: Attached\<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> (e\<^sub>1;; e\<^sub>2): Attached" |
	AT_Assign: "\<Gamma>, A \<turnstile> e: T \<Longrightarrow> \<Gamma>, A \<turnstile> (n ::= e): Attached" |
	AT_Create: "\<Gamma>, A \<turnstile> (create n): Attached" |
	AT_Call: "\<lbrakk>\<Gamma>, A \<turnstile> e: Attached; \<Gamma>, \<A> e A \<turnstile> a [:] Ts\<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> (e \<^bsub>\<bullet>\<^esub> f (a)): Attached" |
	AT_If: "\<lbrakk>
	    \<Gamma>, A \<turnstile> b: Attached;
	    \<Gamma>, A \<rhd>+ b \<turnstile> e\<^sub>1: T\<^sub>1;
	    \<Gamma>, A \<rhd>- b \<turnstile> e\<^sub>2: T\<^sub>2
	  \<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> if b then e\<^sub>1 else e\<^sub>2 end: upper_bound T\<^sub>1 T\<^sub>2" |
	AT_Loop: "\<lbrakk>
      \<Gamma>, loop_computation e b A \<turnstile> e: Attached;
      \<Gamma>, loop_computation e b A \<rhd>- e \<turnstile> b: Attached
    \<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> until e loop b end: Attached" |
	AT_Test: "\<Gamma>, A \<turnstile> e: T \<Longrightarrow> \<Gamma>, A \<turnstile> (attached t e as n): Attached" |
	AT_Exception: "\<Gamma>, A \<turnstile> Exception: Attached" |
	ATs_Nil: "\<Gamma>, A \<turnstile> [] [:] []" |
	ATs_Cons: "\<lbrakk>\<Gamma>, A \<turnstile> e: T; \<Gamma>, \<A> e A \<turnstile> es [:] Ts\<rbrakk> \<Longrightarrow> \<Gamma>, A \<turnstile> e # es [:] T # Ts"

declare AT_ATs.intros[intro!]

inductive_simps ATs_iffs [iff]:
  "\<Gamma>, A \<turnstile> [] [:] Ts"
  "\<Gamma>, A \<turnstile> e#es [:] T#Ts"
  "\<Gamma>, A \<turnstile> e#es [:] Ts"

(*
lemma ATs_append [iff]: "\<And> Ts. (\<Gamma>, A \<turnstile> es1 @ es2 [:] Ts) =
  (\<exists> Ts1 Ts2. Ts = Ts1 @ Ts2 \<and> \<Gamma>, A \<turnstile> es1 [:] Ts1 \<and> \<Gamma>, A \<turnstile> es2 [:] Ts2)"
*)

paragraph "Type checks inductive cases"

lemmas at_induct = AT_ATs.induct[split_format(complete)]
inductive_cases ValueE[elim!]: "\<Gamma>, A \<turnstile> \<C> v: T"
inductive_cases LocalE[elim!]: "\<Gamma>, A \<turnstile> \<V> n: T"
inductive_cases SeqE[elim!]: "\<Gamma>, A \<turnstile> c1 ;; c2: T"
inductive_cases AssignE[elim!]: "\<Gamma>, A \<turnstile> n ::= e: T"
inductive_cases CreateE[elim!]: "\<Gamma>, A \<turnstile> create n: T"
inductive_cases CallE[elim!]: "\<Gamma>, A \<turnstile> n \<^bsub>\<bullet>\<^esub> f (a): T"
inductive_cases IfE[elim!]: "\<Gamma>, A \<turnstile> if b then c1 else c2 end: T"
inductive_cases LoopE[elim!]: "\<Gamma>, A \<turnstile> until e loop c end: T"
inductive_cases TestE[elim!]: "\<Gamma>, A \<turnstile> attached t e as x: T"
inductive_cases ExceptionE[elim!]: "\<Gamma>, A \<turnstile> Exception: T"
(*inductive_cases ListE[elim!]: "\<Gamma>, A \<turnstile> es [:] Ts"*)

paragraph "Type checks properties"

lemma attachment_unique:
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	shows
		attachment_unique_e: "\<lbrakk>\<Gamma>, A \<turnstile> e : T;	\<Gamma>, A \<turnstile> e : T'\<rbrakk> \<Longrightarrow> T = T'" and
		attachment_unique_es: "\<lbrakk>\<Gamma>, A \<turnstile> es [:] Ts;	\<Gamma>, A \<turnstile> es [:] Ts'\<rbrakk> \<Longrightarrow> Ts = Ts'"
	by (induction arbitrary: T' and Ts' rule: "AT_ATs.inducts") ((blast?, fastforce)+)

lemma checked_attached_is_attached:
    "\<Gamma>, A \<turnstile> e: Attached \<Longrightarrow> A \<hookrightarrow> e"
proof (induction e arbitrary: A)
  case (If b c1 c2)
  then have
    "\<Gamma>, A \<turnstile> b : Attached" by blast
  from If.prems
    obtain T\<^sub>1 T\<^sub>2 where "\<Gamma>, A \<rhd>+ b \<turnstile> c1 : T\<^sub>1" "\<Gamma>, A \<rhd>- b \<turnstile> c2 : T\<^sub>2" "upper_bound T\<^sub>1 T\<^sub>2 = Attached"
      by force
  moreover then have "T\<^sub>1 = Attached" and "T\<^sub>2 = Attached"
    using upper_bound_bot_left' upper_bound_bot_right' by blast+
  ultimately have
    "A \<rhd>+ b \<hookrightarrow> c1" and
    "A \<rhd>- b \<hookrightarrow> c2" using \<A>\<T>_mono' If.IH topset_union_upper1 by blast+
  then show ?case by simp
qed auto

lemma list_attachment_length:
	"\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow> length es = length Ts"
	by (induction es arbitrary: \<Gamma> A Ts) (insert ATs.cases, fastforce, auto)

lemma list_attachment_validity:
	"\<Gamma>, A \<turnstile> es1 @ es2 [:] Ts \<Longrightarrow> \<Gamma>, A \<rhd>\<rhd> es1 \<turnstile> es2 [:] drop (length es1) Ts"
proof (induction es1 arbitrary: \<Gamma> A Ts)
	case Nil then show ?case by simp
next
	fix es :: "('a, 'b) expression list"
	case (Cons e es \<Gamma> A Ts)
	then obtain "es'" where "es' = es @ es2" by simp
	with Cons have "\<Gamma>, A \<turnstile> [e] @ es' [:] Ts" by simp
	then have "\<Gamma>, A \<rhd>\<rhd> [e] \<turnstile> es @ es2 [:] drop 1 Ts" using Cons.IH
	proof -
		have f1: "\<forall>f t (es::('a, 'b) expression list) as'. \<not> f, t \<turnstile> es [:] as'\<or> es = [] \<and> as'= [] \<or> (\<exists>e a esa asa. es = e # esa \<and> as' = a # asa \<and> f, t \<turnstile> e : a \<and> f, t \<rhd> e \<turnstile> esa [:] asa)"
			using ATs.simps by auto
		obtain
			ee  :: "attachment_type list \<Rightarrow> ('a, 'b) expression list \<Rightarrow> char list topset \<Rightarrow>
				(char list \<Rightarrow> attachment_mark Type.type option) \<Rightarrow> ('a, 'b) expression" and
		  ees :: "attachment_type list \<Rightarrow> ('a, 'b) expression list \<Rightarrow> char list topset \<Rightarrow>
		  	(char list \<Rightarrow> attachment_mark Type.type option) \<Rightarrow> ('a, 'b) expression list" and
			aa  :: "attachment_type list \<Rightarrow> ('a, 'b) expression list \<Rightarrow> char list topset \<Rightarrow>
				(char list \<Rightarrow> attachment_mark Type.type option) \<Rightarrow> attachment_type" and
			aas :: "attachment_type list \<Rightarrow> ('a, 'b) expression list \<Rightarrow> char list topset \<Rightarrow>
				(char list \<Rightarrow> attachment_mark Type.type option) \<Rightarrow> attachment_type list" where
			"\<forall>x0 x1 x2 x3. (\<exists>v4 v5 v6 v7. x1 = v4 # v6 \<and> x0 = v5 # v7 \<and> x3, x2 \<turnstile> v4 : v5 \<and> x3, x2 \<rhd> v4 \<turnstile> v6 [:] v7) = (x1 = ee x0 x1 x2 x3 # ees x0 x1 x2 x3 \<and> x0 = aa x0 x1 x2 x3 # aas x0 x1 x2 x3 \<and> x3, x2 \<turnstile> ee x0 x1 x2 x3 : aa x0 x1 x2 x3 \<and> x3, x2 \<rhd> ee x0 x1 x2 x3 \<turnstile> ees x0 x1 x2 x3 [:] aas x0 x1 x2 x3)"
			by moura
		then have f2: "\<forall>f t es as'. \<not> f, t \<turnstile> es [:] as' \<or> es = [] \<and> as' = [] \<or> es = ee as' es t f # ees as' es t f \<and> as' = aa as' es t f # aas as' es t f \<and> f, t \<turnstile> ee as' es t f : aa as' es t f \<and> f, t \<rhd> ee as' es t f \<turnstile> ees as' es t f [:] aas as' es t f"
			using f1 by presburger
		have f3: "\<forall>as' es t f. (\<not> f, t \<turnstile> es [:] as' \<or> es = [] \<and> as' = [] \<or> es = ee as' es t f # ees as' es t f \<and> as' = aa as' es t f # aas as' es t f \<and> f, t \<turnstile> ee as' es t f : aa as' es t f \<and> f, t \<rhd> ee as' es t f \<turnstile> ees as' es t f [:] aas as' es t f) = (\<not> f, t \<turnstile> es [:] as' \<or> es = [] \<and> as' = [] \<or> es = ee as' es t f # ees as' es t f \<and> as' = aa as' es t f # aas as' es t f \<and> f, t \<turnstile> ee as' es t f : aa as' es t f \<and> f, t \<rhd> ee as' es t f \<turnstile> ees as' es t f [:] aas as' es t f)"
			by blast
		have "\<forall>as' es t f. (\<not> f, t \<turnstile> es [:] as' \<or> es = [] \<and> as' = [] \<or> es = ee as' es t f # ees as' es t f \<and> as' = aa as' es t f # aas as' es t f \<and> f, t \<turnstile> ee as' es t f : aa as' es t f \<and> f, t \<rhd> ee as' es t f \<turnstile> ees as' es t f [:] aas as' es t f) = (\<not> f, t \<turnstile> es [:] as' \<or> es = [] \<and> as' = [] \<or> es = ee as' es t f # ees as' es t f \<and> as' = aa as' es t f # aas as' es t f \<and> f, t \<turnstile> ee as' es t f : aa as' es t f \<and> f, t \<rhd> ee as' es t f \<turnstile> ees as' es t f [:] aas as' es t f)"
			by blast
		then have "([e] @ es) @ es2 = [] \<and> Ts = [] \<or> ([e] @ es) @ es2 = ee Ts (([e] @ es) @ es2) A \<Gamma> # ees Ts (([e] @ es) @ es2) A \<Gamma> \<and> Ts = aa Ts (([e] @ es) @ es2) A \<Gamma> # aas Ts (([e] @ es) @ es2) A \<Gamma> \<and> \<Gamma>, A \<turnstile> ee Ts (([e] @  es) @  es2) A \<Gamma> : aa Ts (([e] @ es) @ es2) A \<Gamma> \<and> \<Gamma>, A \<rhd> ee Ts (([e] @ es) @ es2) A \<Gamma> \<turnstile> ees Ts (([e] @ es) @ es2) A \<Gamma> [:] aas Ts (([e] @ es) @ es2) A \<Gamma>"
			using f3 f2 by (metis Cons.prems append_Cons append_Nil)
		then have f4: "([e] @ es) @ es2 = ee Ts (([e] @ es) @ es2) A \<Gamma> # ees Ts (([e] @ es) @ es2) A \<Gamma> \<and> Ts = aa Ts (([e] @ es) @ es2) A \<Gamma> # aas Ts (([e] @ es) @ es2) A \<Gamma> \<and> \<Gamma>, A \<turnstile> ee Ts (([e] @ es) @ es2) A \<Gamma> : aa Ts (([e] @ es) @ es2) A \<Gamma> \<and> \<Gamma>, A \<rhd> ee Ts (([e] @ es) @ es2) A \<Gamma> \<turnstile> ees Ts (([e] @ es) @ es2) A \<Gamma> [:] aas Ts (([e] @ es) @ es2) A \<Gamma>"
			by blast
		then have "drop 1 Ts = drop 0 (aas Ts (([e] @ es) @ es2) A \<Gamma>)"
			by (metis (no_types) One_nat_def drop_Suc_Cons)
		then show ?thesis
			using f4 by force
	qed
	then show ?case
		by (metis Cons.IH One_nat_def TransferFunction.\<A>s_Nil drop_drop list.size(4) transfer_unfold)
qed

lemma list_attachment_validity_head:
	"\<Gamma>, A \<turnstile> e # es [:] Ts \<Longrightarrow> \<Gamma>, A \<turnstile> e : hd Ts"
	by (induction Ts) auto

lemma list_attachment_validity_tail:
	"\<Gamma>, A \<turnstile> e # es [:] Ts \<Longrightarrow> \<Gamma>, A \<rhd> e \<turnstile> es [:] tl Ts"
	by (induction Ts) auto

lemma
	fixes e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	shows
		attachment_validity_unique: "\<lbrakk>\<Gamma>, A \<turnstile> e : T; \<Gamma>, A \<turnstile> e : T'\<rbrakk> \<Longrightarrow> T = T'" and
		list_attachment_validity_unique: "\<lbrakk>\<Gamma>, A \<turnstile> es [:] Ts; \<Gamma>, A \<turnstile> es [:] Ts'\<rbrakk> \<Longrightarrow> Ts = Ts'"
proof (induct arbitrary: T' and Ts' rule: AT_ATs.inducts)
	case (AT_If \<Gamma> A c e1 T1 e2 T2)
	then show ?case using IfE by blast
next
	case ATs_Nil then show ?case by simp
next
	case (ATs_Cons \<Gamma> A x T xs Ts)
	then show ?case	using ATs_iffs by blast
qed auto


lemma 
	fixes
		e :: "('b, 't) expression" and
		es :: "('b, 't) expression list"
  shows
    AT_mono: "\<Gamma>, A \<turnstile> e: T\<^sub>A \<Longrightarrow> (\<And> B. A \<le> B \<Longrightarrow> \<exists> T\<^sub>B. \<Gamma>, B \<turnstile> e: T\<^sub>B \<and> T\<^sub>B \<rightarrow>\<^sub>a T\<^sub>A)" and
    ATs_mono: "\<Gamma>, A \<turnstile> es [:] Tsa \<Longrightarrow> (\<And> B. A \<le> B \<Longrightarrow> \<exists> Tsb. \<Gamma>, B \<turnstile> es [:] Tsb)"
proof (induction arbitrary: rule:"AT_ATs.inducts")
	case AT_ValueAtt then show ?case by auto
next
	case AT_ValueDet then show ?case by auto
next
	case AT_LocalAtt then show ?case using attached_conforms_to by blast
next
	case AT_LocalDet then show ?case using conforms_to_detachable by blast
next
	case (AT_Seq \<Gamma> A c1 c2) 
  with \<A>_mono' have "A \<rhd> c1 \<le> B \<rhd> c1" by metis
  with AT_Seq show ?case using conforms_to_attached by blast
next
	case AT_Assign then show ?case using attached_conforms_to by blast
next
	case AT_Create then show ?case by auto
next
	case (AT_Call \<Gamma> A  e es Ts)
  with \<A>_mono' have "A \<rhd> e \<le> B \<rhd> e" by metis
  with AT_Call show ?case using conforms_to_attached by blast
next
	case (AT_If \<Gamma> A c e1 T1 e2 T2)
	then have
		lHc: "\<Gamma>, B \<turnstile> c : Attached" using conforms_to_attached by auto
	from AT_If obtain T1' T2' where
		lHe1: "\<Gamma>, A \<rhd>+ c \<turnstile> e1 : T1'" and
		lHT1: "T1' \<rightarrow>\<^sub>a T1" and
		lHe2: "\<Gamma>, A \<rhd>- c \<turnstile> e2 : T2'" and
		lHT2: "T2' \<rightarrow>\<^sub>a T2" using is_attached_conformance by blast+
	from AT_If have
		"A \<rhd>+ c \<le> B \<rhd>+ c"
		"A \<rhd>- c \<le> B \<rhd>- c"
			by (simp_all add: \<A>t_mono' \<A>f_mono')
	with lHe1 lHe2 AT_If have
		"\<exists>Tb. \<Gamma>, B \<rhd>+ c \<turnstile> e1 : Tb \<and> Tb \<rightarrow>\<^sub>a T1'"
		"\<exists>Tb. \<Gamma>, B \<rhd>- c \<turnstile> e2 : Tb \<and> Tb \<rightarrow>\<^sub>a T2'"
			using attachment_validity_unique by force+
	then obtain Tb1 Tb2 where
		lHBe1:"\<Gamma>, B \<rhd>+ c \<turnstile> e1 : Tb1" and
		lHTb1: "Tb1 \<rightarrow>\<^sub>a T1'" and
		lHBe2: "\<Gamma>, B \<rhd>- c \<turnstile> e2 : Tb2" and
		lHTb2: "Tb2 \<rightarrow>\<^sub>a T2'" by blast
	with lHc have
		lHB: "\<Gamma>, B \<turnstile> if c then e1 else e2 end : upper_bound Tb1 Tb2" by auto
	from lHT1 lHTb1 lHT2 lHTb2 have
		"Tb1 \<rightarrow>\<^sub>a T1" and
		"Tb2 \<rightarrow>\<^sub>a T2"
			using attachment_conforming_to_transitive by blast+
	then have "upper_bound Tb1 Tb2 \<rightarrow>\<^sub>a upper_bound T1 T2" using sup.mono by blast
	then show ?case using lHB by meson
next
	case (AT_Loop \<Gamma> e b A)
	then have "\<Gamma>, loop_computation e b B \<turnstile> e : Attached"
		using conforms_to_attached loop_operator_mono' by metis
	moreover from AT_Loop have "\<Gamma>, loop_computation e b B \<rhd>- e \<turnstile> b : Attached"
		using \<A>f_mono' conforms_to_attached loop_operator_mono' by metis
	ultimately show ?case using attached_conforms_to by blast
next
	case AT_Test then show ?case using attached_conforms_to by blast
next
	case AT_Exception then show ?case by auto
next
	case ATs_Nil then show ?case by simp
next
	case (ATs_Cons \<Gamma> A e T es Ts)
  with \<A>_mono' have "A \<rhd> e \<le> B \<rhd> e" by metis
  with ATs_Cons show ?case by auto
qed

lemma attached_assignment_set:
  assumes
    HA: "\<Gamma>, A \<turnstile> (n ::= e): Attached" and
    HE: "\<Gamma>, A \<turnstile> e: Attached"
  shows "\<A> (n ::= e) A = (\<A> e A) \<oplus> n"
using HE HA
proof (induction e arbitrary: \<Gamma> A)
  case (If b c1 c2)
  moreover then have
    lHA1: "\<Gamma>, A \<rhd>+ b \<turnstile> c1 : Attached" and
    lHA2: "\<Gamma>, A \<rhd>- b \<turnstile> c2 : Attached"
      apply (metis IfE upper_bound_bot_left')
        by (metis IfE calculation(4) upper_bound_bot_right')
  moreover with If have "\<Gamma>, A \<rhd>+ b \<turnstile> n ::= c1 : Attached" and "\<Gamma>, A \<rhd>- b \<turnstile> n ::= c2 : Attached"
    using AT_Assign AssignE IfE declared_attachment_type_of_local_def by blast+
  ultimately have "\<A> (n ::= c1) (A \<rhd>+ b) = (\<A> c1 (A \<rhd>+ b)) \<oplus> n" and
    "\<A> (n ::= c2) (A \<rhd>- b) = (\<A> c2 (A \<rhd>- b)) \<oplus> n" by simp_all
  from lHA1 have
    lHA1': "A \<rhd>+ b \<hookrightarrow> c1" by (rule checked_attached_is_attached)
  from lHA2 have
    lHA2': "A \<rhd>- b \<hookrightarrow> c2" by (rule checked_attached_is_attached)
  have "A \<rhd> n ::= (if b then c1 else c2 end) =
    (if (A \<hookrightarrow> (if b then c1 else c2 end)) then
      (A \<rhd> (if b then c1 else c2 end)) \<oplus> n else
      (A \<rhd> (if b then c1 else c2 end)) \<ominus> n)" by simp
  also have "\<dots> = (if ((A \<rhd>+ b \<hookrightarrow> c1) \<and> (A \<rhd>- b \<hookrightarrow> c2)) then
    ((A \<rhd>+ b \<rhd> c1) \<sqinter> (A \<rhd>- b \<rhd> c2)) \<oplus> n else
    ((A \<rhd>+ b \<rhd> c1) \<sqinter> (A \<rhd>- b \<rhd> c2)) \<ominus> n)" by simp
  also with lHA1' lHA2' have "\<dots> = ((A \<rhd>+ b \<rhd> c1) \<sqinter> (A \<rhd>- b \<rhd> c2)) \<oplus> n" by simp
  finally show ?case by simp
qed auto

lemma checked_detachable_is_detachable':
  assumes
    HA: "\<Gamma>, A \<turnstile> e: T" and
    HT: "T = Detachable"
  shows
    "\<not> A \<hookrightarrow> e"
using assms
proof (induction e arbitrary: \<Gamma> A T)
  case (Value v)
  then show ?case by auto
next
  case (If c e\<^sub>1 e\<^sub>2)
  moreover then obtain T\<^sub>1 T\<^sub>2 where
	  "\<Gamma>, A \<turnstile> c: Attached"
	  "\<Gamma>, A \<rhd>+ c \<turnstile> e\<^sub>1: T\<^sub>1"
	  "\<Gamma>, A \<rhd>- c \<turnstile> e\<^sub>2: T\<^sub>2"
	  "T = upper_bound T\<^sub>1 T\<^sub>2"
	    using IfE by auto
	moreover with If.prems have "T\<^sub>1 = Detachable \<or> T\<^sub>2 = Detachable" by (metis sup_attachment_type_def)
  ultimately have "\<not> A \<rhd>+ c \<hookrightarrow> e\<^sub>1 \<or> \<not> A \<rhd>- c \<hookrightarrow> e\<^sub>2" by force
  then show ?case using \<A>\<T>_If by simp
qed auto

lemma checked_detachable_is_detachable: "\<Gamma>, A \<turnstile> e: Detachable \<Longrightarrow> \<not> A \<hookrightarrow> e"
  by (simp add: checked_detachable_is_detachable')

lemma detachable_assignment_set:
  assumes
    HA: "\<Gamma>, A \<turnstile> (n ::= e): Attached" and
    HE: "\<Gamma>, A \<turnstile> e: Detachable"
  shows "\<A> (n ::= e) A = (\<A> e A) \<ominus> n"
using HE HA
proof (induction e arbitrary: \<Gamma> A)
  case (Value v)
  then show ?case by auto
next
  case (If b c1 c2)
  then show ?case using checked_detachable_is_detachable
    using \<A>_Assign by metis
next
  case (Loop e c)
  then show ?case by auto
qed auto

lemma AT_loop: "\<Gamma>, A \<turnstile> until e loop c end : T \<Longrightarrow> \<Gamma>, loop_computation e c A \<turnstile> until e loop c end : T"
proof -
  assume
    lH: "\<Gamma>, A \<turnstile> until e loop c end : T"
  then have
    lHe: "\<Gamma>, loop_computation e c A \<turnstile> e: Attached" and
    lHc: "\<Gamma>, loop_computation e c A \<rhd>- e \<turnstile> c: Attached" and
    lHt: "T = Attached" by auto
  have "mono (\<lambda>X. X \<rhd>- e \<rhd> c)" by (simp add: \<A>_mono'  \<A>f_mono' monoI)
  then have "loop_computation e c (loop_computation e c A) = loop_computation e c A"
    using loop_operator_idem by auto
  with lHe lHc have
    "\<Gamma>, loop_computation e c (loop_computation e c A) \<turnstile> e: Attached" and
    "\<Gamma>, loop_computation e c (loop_computation e c A) \<rhd>- e \<turnstile> c: Attached" by simp_all
  with lHt show ?thesis by (simp add: AT_Loop)
qed

lemma AT_loop_step: "\<Gamma>, A \<turnstile> until e loop c end : T \<Longrightarrow> \<Gamma>, A \<rhd>- e \<rhd> c \<turnstile> until e loop c end : T"
  by (metis AT_loop AT_mono LoopE loop_computation_le1)

text "Function that computes attachment status."

fun
	at_type :: "attachment_environment \<Rightarrow> vname topset \<Rightarrow> ('b, 't) expression \<Rightarrow> attachment_type option" and
	ats_type :: "attachment_environment \<Rightarrow> vname topset \<Rightarrow> ('b, 't) expression list \<Rightarrow> attachment_type list option"
where
	at_type_value: "at_type \<Gamma> A (\<C> v) = (if v = Void\<^sub>v then \<lfloor>Detachable\<rfloor> else \<lfloor>Attached\<rfloor>)" |
	at_type_local: "at_type \<Gamma> A (\<V> n) = (if n \<in>\<^sup>\<top> A then \<lfloor>Attached\<rfloor> else \<lfloor>Detachable\<rfloor>)" |
	at_type_seq: "at_type \<Gamma> A (c\<^sub>1;; c\<^sub>2) = (if at_type \<Gamma> A c\<^sub>1 = \<lfloor>Attached\<rfloor> \<and> at_type \<Gamma> (\<A> c\<^sub>1 A) c\<^sub>2 = \<lfloor>Attached\<rfloor> then \<lfloor>Attached\<rfloor> else None)" |
	at_type_assign: "at_type \<Gamma> A (n ::= e) = (if at_type \<Gamma> A e \<noteq> None then \<lfloor>Attached\<rfloor> else None)" |
	at_type_create: "at_type \<Gamma> A (create n) = \<lfloor>Attached\<rfloor>" |
	at_type_call: "at_type \<Gamma> A (e \<^bsub>\<bullet>\<^esub> f (a)) = (\<lambda> t ts. (if t = \<lfloor>Attached\<rfloor> then
		case ts of
			Some Ts \<Rightarrow> \<lfloor>Attached\<rfloor> |
			None \<Rightarrow> None
		else None)) (at_type \<Gamma> A e) (ats_type \<Gamma> (A \<rhd> e) a)" |
	at_type_if: "at_type \<Gamma> A (if b then c1 else c2 end) = (if at_type \<Gamma> A b = \<lfloor>Attached\<rfloor> then
	  (case at_type \<Gamma> (A \<rhd>+ b) c1 of
	    None \<Rightarrow> None |
	    \<lfloor>T1\<rfloor> \<Rightarrow> (case at_type \<Gamma> (A \<rhd>- b) c2 of
	      None \<Rightarrow> None |
  	    \<lfloor>T2\<rfloor> \<Rightarrow> \<lfloor>upper_bound T1 T2\<rfloor>))
  	else None)" |
	at_type_loop: "at_type \<Gamma> A (until e loop c end) =
	  (if
	    at_type \<Gamma> (loop_computation e c A) e = \<lfloor>Attached\<rfloor> \<and>
	    at_type \<Gamma> (loop_computation e c A \<rhd>- e) c = \<lfloor>Attached\<rfloor>
	  then \<lfloor>Attached\<rfloor>
	  else None)" |
	at_type_test: "at_type \<Gamma> A (attached t e as n) = (if at_type \<Gamma> A e \<noteq> None then \<lfloor>Attached\<rfloor> else None)" |
  at_type_exception: "at_type \<Gamma> A Exception = \<lfloor>Attached\<rfloor>" |
  ats_nil: "ats_type \<Gamma> A [] = Some []" |
  ats_cons: "ats_type \<Gamma> A (e # es) =
  	(case at_type \<Gamma> A e of
 			None \<Rightarrow> None |
 			\<lfloor>T\<rfloor> \<Rightarrow> (case ats_type \<Gamma> (A \<rhd> e) es of
	  		None \<Rightarrow> None |
	  		\<lfloor>Ts\<rfloor> \<Rightarrow> Some (T # Ts)))"

lemma ats_type_nil:
	"ats_type \<Gamma> A [] = \<lfloor>Ts\<rfloor> \<Longrightarrow> Ts = []"
by simp

lemma ats_type_cons:
	"ats_type \<Gamma> A (e # es) = \<lfloor>Ts\<rfloor> \<Longrightarrow> \<exists> T' Ts'. at_type \<Gamma> A e = \<lfloor>T'\<rfloor> \<and> ats_type \<Gamma> (A \<rhd> e) es = \<lfloor>Ts'\<rfloor> \<and> Ts = T' # Ts'"
proof -
  assume
  	"ats_type \<Gamma> A (e # es) = \<lfloor>Ts\<rfloor>"
  moreover then have
  	"at_type \<Gamma> A e \<noteq> None"
  	"ats_type \<Gamma> (A \<rhd> e) es \<noteq> None"
	  	using ats_cons by (metis (no_types) ats_cons option.case_eq_if option.simps(3))+
  ultimately show ?thesis by auto
qed

lemma ats_type_not_empty:
	"ats_type \<Gamma> A (e # es) = \<lfloor>Ts\<rfloor> \<Longrightarrow> Ts \<noteq> []"
using ats_type_cons by blast

lemma ats_type_head:
	"es \<noteq> [] \<Longrightarrow> ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor> \<Longrightarrow> \<lfloor>hd Ts\<rfloor> = at_type \<Gamma> A (hd es)"
using ats_type_cons by (metis list.sel(1) neq_Nil_conv)

lemma ats_type_head':
	"ats_type \<Gamma> A (e # es) = \<lfloor>Ts\<rfloor> \<Longrightarrow> at_type \<Gamma> A e =  \<lfloor>hd Ts\<rfloor>"
using ats_type_head by fastforce

lemma ats_type_tail:
	"es \<noteq> [] \<Longrightarrow> ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor> \<Longrightarrow> \<lfloor>tl Ts\<rfloor> = ats_type \<Gamma> (A \<rhd> hd es) (tl es)"
proof -
	assume
		lHe: "es \<noteq> []" and
		lHt: "ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor>"
	from lHe obtain e' es' where
		lHc: "es = e' # es'" using list.exhaust_sel by blast
	with lHt have
		"ats_type \<Gamma> A (e' # es') = \<lfloor>Ts\<rfloor>" by simp
	then obtain T' Ts' where
		"at_type \<Gamma> A e' = \<lfloor>T'\<rfloor> \<and> ats_type \<Gamma> (A \<rhd> e') es' = \<lfloor>Ts'\<rfloor> \<and> Ts = T' # Ts'"
			using ats_type_cons by blast
	moreover from lHc have "e' = hd es" "es' = tl es" by simp_all
	ultimately show ?thesis by simp
qed

lemma ats_type_tail':
	"ats_type \<Gamma> A (e # es) = \<lfloor>Ts\<rfloor> \<Longrightarrow> ats_type \<Gamma> (A \<rhd> e) es = \<lfloor>tl Ts\<rfloor>"
using ats_type_tail by fastforce

lemma ats_type_cons':
	"es \<noteq> [] \<Longrightarrow> ats_type \<Gamma> A es \<noteq> None \<Longrightarrow> ats_type \<Gamma> A es = \<lfloor>option.the (at_type \<Gamma> A (hd es)) # option.the (ats_type \<Gamma> (A \<rhd> hd es) (tl es))\<rfloor>"
by (metis (no_types, lifting) ats_type.simps(2) list.exhaust_sel option.case_eq_if)

lemma ats_type_tail_length:
	"es \<noteq> [] \<Longrightarrow> ats_type \<Gamma> A es \<noteq> None \<Longrightarrow> length (tl es) = length (option.the (ats_type \<Gamma> (A \<rhd> hd es) (tl es)))"
proof (induction es arbitrary: \<Gamma> A)
	case Nil then show ?case by simp
next
	case (Cons x xs \<Gamma> A)
	then have "ats_type \<Gamma> (A \<rhd> x) xs \<noteq> None" using ats_type_tail' not_None_eq by blast
	then have "length xs = length (option.the (ats_type \<Gamma> (A \<rhd> x) (xs)))"
		using Cons.IH ats_type_cons' by (cases xs) (simp, fastforce)
	then show ?case by simp
qed

lemma ats_type_length:
		"ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor> \<Longrightarrow> length es = length Ts"
proof (cases es)
	case Nil
	assume
		"ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor>"
	with Nil show ?thesis by simp
next
	case (Cons x xs)
	assume
		lHT: "ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor>"
	with Cons have
		"ats_type \<Gamma> A es = \<lfloor>option.the (at_type \<Gamma> A (hd es)) # option.the (ats_type \<Gamma> (A \<rhd> hd es) (tl es))\<rfloor>"
			using ats_type_cons' by blast
	with lHT have
		lHl: "Ts = option.the (at_type \<Gamma> A (hd es)) # option.the (ats_type \<Gamma> (A \<rhd> hd es) (tl es))"
			by simp
	from Cons lHT have
		"length (tl es) = length (option.the (ats_type \<Gamma> (A \<rhd> hd es) (tl es)))"
			using ats_type_tail_length by blast
	with lHl have "length Ts = Suc (length (tl es))" by simp
	with Cons show ?thesis by simp
qed

lemma
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	shows
		"\<Gamma>, A \<turnstile> e: T = (at_type \<Gamma> A e = \<lfloor>T\<rfloor>)" and
		"\<Gamma>, A \<turnstile> es [:] Ts = (ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor>)"
proof (auto simp only:)
  show
  	"\<Gamma>, A \<turnstile> e : T \<Longrightarrow> at_type \<Gamma> A e = \<lfloor>T\<rfloor>" and
  	"\<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow> ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor>"
  		by (induction rule: "AT_ATs.inducts") simp_all
  show
  	"at_type \<Gamma> A e = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> e : T" and
  	"ats_type \<Gamma> A es = \<lfloor>Ts\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> es [:] Ts"
  proof (induction arbitrary: T and Ts rule: "at_type_ats_type.induct")
    fix \<Gamma> A and v :: "'b value" and T
    show "at_type \<Gamma> A (\<C> v) = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> \<C> v : T" by (auto split: split_if_asm)
  next
    fix \<Gamma> A n T
    from AT_LocalAtt AT_LocalDet show "at_type \<Gamma> A (\<V> n) = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> \<V> n : T"
    using option.inject at_type.simps(2) by metis
  next
    fix \<Gamma> A T and c\<^sub>1 c\<^sub>2 :: "('b, 't) expression"
    assume
    	"\<And>T. at_type \<Gamma> A c\<^sub>1 = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> c\<^sub>1 : T"
    	"\<And>T. at_type \<Gamma> (A \<rhd> c\<^sub>1) c\<^sub>2 = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<rhd> c\<^sub>1 \<turnstile> c\<^sub>2 : T"
    	"at_type \<Gamma> A (c\<^sub>1 ;; c\<^sub>2) = \<lfloor>T\<rfloor>"
    with AT_Seq show "\<Gamma>, A \<turnstile> c\<^sub>1 ;; c\<^sub>2 : T"
      by (metis at_type.simps(3) option.sel option.simps(3))
  next
    fix \<Gamma> A n T and e :: "('b, 't) expression"
    assume
      "\<And>T. at_type \<Gamma> A e = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> e : T"
      "at_type \<Gamma> A (n ::= e) = \<lfloor>T\<rfloor>"
    with AT_Assign show "\<Gamma>, A \<turnstile> n ::= e : T" by (metis at_type.simps(4) not_None_eq option.sel)
  next
    fix \<Gamma> A n T
    assume "at_type \<Gamma> A (create n) = \<lfloor>T\<rfloor>"
    with AT_Create show "\<Gamma>, A \<turnstile> create n : T" by simp
  next
  	case (6 \<Gamma> A e f a T)
  	term ?case
    fix \<Gamma> A and e :: "('b, 't) expression" and f a T
    assume
      lHB: "\<And>T. at_type \<Gamma> A e = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> e : T" and
      lHI: "\<And>Ts. \<lbrakk>at_type \<Gamma> A e = \<lfloor>Attached\<rfloor>; ats_type \<Gamma> (A \<rhd> e) a = \<lfloor>Ts\<rfloor>\<rbrakk> \<Longrightarrow> \<Gamma>, A \<rhd> e \<turnstile> a [:] Ts" and
      lHp: "at_type \<Gamma> A (e \<^bsub>\<bullet>\<^esub> f (a)) = \<lfloor>T\<rfloor>"
    then have
    	lHe: "\<Gamma>, A \<turnstile> e : Attached" by force
    from lHp have
    	lHt: "at_type \<Gamma> A e = \<lfloor>Attached\<rfloor>"
    		using at_type_call option.discI by fastforce
    moreover from lHp obtain Ts where
    	lHs: "ats_type \<Gamma> (A \<rhd> e) a = \<lfloor>Ts\<rfloor>"
    		by (metis at_type_call option.distinct(1) option.exhaust_sel option.simps(4))
    ultimately have "\<Gamma>, A \<rhd> e \<turnstile> a [:] Ts" using lHI by simp
    with AT_Call lHe have "\<Gamma>, A \<turnstile> e \<^bsub>\<bullet>\<^esub> f (a) : Attached" by blast
    with lHt lHs lHp show "\<Gamma>, A \<turnstile> e \<^bsub>\<bullet>\<^esub> f (a) : T" by simp
  next
    fix \<Gamma> A T and b c1 c2 :: "('b, 't) expression"
    assume
       IHb: "\<And>T. at_type \<Gamma> A b = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> b : T" and
       IH1: "\<And>T. \<lbrakk>at_type \<Gamma> A b = \<lfloor>Attached\<rfloor>; at_type \<Gamma> (A \<rhd>+ b) c1 = \<lfloor>T\<rfloor>\<rbrakk> \<Longrightarrow> \<Gamma>, A \<rhd>+ b \<turnstile> c1 : T" and
       IH2: "\<And>x2 T. \<lbrakk>at_type \<Gamma> A b = \<lfloor>Attached\<rfloor>; at_type \<Gamma> (A \<rhd>+ b) c1 = \<lfloor>x2\<rfloor>; at_type \<Gamma> (A \<rhd>- b) c2 = \<lfloor>T\<rfloor>\<rbrakk> \<Longrightarrow>
                \<Gamma>, A \<rhd>- b \<turnstile> c2 : T" and
       lHT: "at_type \<Gamma> A (if b then c1 else c2 end) = \<lfloor>T\<rfloor>"
    then have
    	lHfb: "at_type \<Gamma> A b = \<lfloor>Attached\<rfloor>" using at_type_if by (metis option.simps(3))
    with IHb have
      lHb: "\<Gamma>, A \<turnstile> b : Attached" by simp
		from lHfb lHT have "None \<noteq> at_type \<Gamma> (A \<rhd>+ b) c1" by auto
		then have "at_type \<Gamma> (A \<rhd>+ b) c1 \<noteq> None" by simp
		moreover with lHfb lHT have "None \<noteq> at_type \<Gamma> (A \<rhd>- b) c2" by auto
		then have "at_type \<Gamma> (A \<rhd>- b) c2 \<noteq> None" by simp
    ultimately obtain T1 T2 where
      "at_type \<Gamma> (A \<rhd>+ b) c1 = \<lfloor>T1\<rfloor>" and
      "at_type \<Gamma> (A \<rhd>- b) c2 = \<lfloor>T2\<rfloor>" and
      "T = upper_bound T1 T2"
      using lHT lHfb by auto
    with IH1 IH2 lHfb have
    	"\<Gamma>, (A \<rhd>+ b) \<turnstile> c1 : T1"
    	"\<Gamma>, (A \<rhd>- b) \<turnstile> c2 : T2" 
      "T = upper_bound T1 T2" by simp_all
    with lHb show "\<Gamma>, A \<turnstile> if b then c1 else c2 end : T" using AT_If by simp
  next
    fix \<Gamma> A T and e c:: "('b, 't) expression"
    assume
      "\<And>T. at_type \<Gamma> (loop_computation e c A) e = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, loop_computation e c A \<turnstile> e : T"
      "\<And>T. at_type \<Gamma> (loop_computation e c A \<rhd>- e) c = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, loop_computation e c A \<rhd>- e \<turnstile> c : T"
      "at_type \<Gamma> A (until e loop c end) = \<lfloor>T\<rfloor>"
    with AT_Loop show "\<Gamma>, A \<turnstile> until e loop c end : T" using at_type_loop
      by (metis option.discI option.inject)
  next
    fix \<Gamma> A T t x and e:: "('b, 't) expression"
    assume
      "\<And>T. at_type \<Gamma> A e = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> e : T"
      "at_type \<Gamma> A (attached t e as x) = \<lfloor>T\<rfloor>"
    with AT_Test show "\<Gamma>, A \<turnstile> attached t e as x : T"
      by (metis at_type.simps(9) not_Some_eq option.sel)
  next
    fix \<Gamma> A T
    assume
      "at_type \<Gamma> A Exception = \<lfloor>T\<rfloor>"
    with AT_Exception show "\<Gamma>, A \<turnstile> Exception : T" by simp
	next
		fix \<Gamma> A \<Gamma>' Aa Ts
		assume
			"ats_type \<Gamma> A [] = \<lfloor>Ts\<rfloor>"
		then show "\<Gamma>, A \<turnstile> [] [:] Ts" by simp
	next
		fix \<Gamma> A e and es :: "('b, 't) expression list" and Ts
		assume
			lHb: "\<And>T. at_type \<Gamma> A e = \<lfloor>T\<rfloor> \<Longrightarrow> \<Gamma>, A \<turnstile> e : T" and
			lHi: "\<And>x2 Ts. \<lbrakk>at_type \<Gamma> A e = \<lfloor>x2\<rfloor>; ats_type \<Gamma> (A \<rhd> e) es = \<lfloor>Ts\<rfloor>\<rbrakk> \<Longrightarrow> \<Gamma>, A \<rhd> e \<turnstile> es [:] Ts" and
			lHp: "ats_type \<Gamma> A (e # es) = \<lfloor>Ts\<rfloor>"
		from lHp have
			lHe: "at_type \<Gamma> A e = \<lfloor>hd Ts\<rfloor>" using ats_type_head' by blast
		with lHb have
			lHT: "\<Gamma>, A \<turnstile> e : hd Ts" by simp
		from lHp have "ats_type \<Gamma> (A \<rhd> e) es = \<lfloor>tl Ts\<rfloor>" using ats_type_tail' by blast
		with lHi lHe have "\<Gamma>, A \<rhd> e \<turnstile> es [:] tl Ts" by simp
		with lHT lHp show "\<Gamma>, A \<turnstile> e # es [:] Ts"
			using ATs_iffs(2) ats_type_not_empty by fastforce
  qed
qed

definition attachment_type_valid_expression ("_ \<turnstile> _ : _") where
  "attachment_type_valid_expression \<Gamma> e T = ExpressionValidity.AT \<Gamma> \<emptyset> e T"

definition attachment_valid_expression :: "attachment_environment \<Rightarrow> ('b, 't) expression \<Rightarrow> bool"
	("_ \<turnstile> _ \<surd>\<^sub>e") where
  "attachment_valid_expression \<Gamma> e = (\<exists> T. attachment_type_valid_expression \<Gamma> e T)"

end