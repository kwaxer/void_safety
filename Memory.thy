section "Instances of a type"

theory Memory imports Value begin

locale memory =
	fixes
		instantiated :: "'memory \<Rightarrow> 'a type  \<Rightarrow> ('memory \<times> 'b value) option"
	assumes
		direct_instance: "instantiated m t = \<lfloor>(m', v)\<rfloor> \<Longrightarrow> v \<noteq> Unit \<and> is_attached v"
begin
end

definition instantiated :: "'memory \<Rightarrow> 'a type \<Rightarrow> ('memory \<times> 'b value) option" where
	"instantiated m t = Some (m, Reference (AttachedObject User))"
declare instantiated_def [simp]

interpretation basic_memory: memory
		instantiated
by (unfold_locales, auto)

definition "instance \<equiv> instantiated:: 'memory \<Rightarrow> 'a type \<Rightarrow> ('memory \<times> 'b value) option"

consts is_instance_of :: "'b value \<Rightarrow> 't \<Rightarrow> bool"

end