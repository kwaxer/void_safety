section "Common definitions and lemmas"

theory Common imports
	Main
	"~~/src/HOL/Library/LaTeXsugar"
	"~~/src/HOL/Library/OptionalSugar"
begin

no_notation floor ("\<lfloor>_\<rfloor>")
no_notation ceiling ("\<lceil>_\<rceil>")
notation Some ("\<lfloor>_\<rfloor>")

lemma fold_mono: "(\<And> x. mono (f x)) \<Longrightarrow> mono (\<lambda> x. fold f xs x)"
proof -
	assume "\<And> x. mono (f x)"
	then have "(\<And> x X Y. X \<le> Y \<Longrightarrow> f x X \<le> f x Y)" by (simp add: monoD)
	then have "\<And> X Y. X \<le> Y \<Longrightarrow> fold f xs X \<le> fold f xs Y" by (induction xs) simp_all
	then show "mono (\<lambda> x. fold f xs x)" by (simp add: monoI)
qed

end