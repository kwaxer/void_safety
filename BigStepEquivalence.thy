theory BigStepEquivalence imports
	BigStepSafety
	BigStep_unsafe
begin

lemma
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	shows
		big_step_safe_implies_unsafe: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow>' \<langle>e', s'\<rangle>"
	and
		big_steps_safe_implies_unsafe: "\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>]' \<langle>es', s'\<rangle>"
	by (induction rule: "BigStep.big_step_big_steps.inducts") simp_all

lemma
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	assumes
		"\<Gamma>, A \<turnstile> s \<surd>\<^sub>s" and
		"A \<noteq> \<top>"
	shows
		big_step_unsafe_implies_safe_step:
			"\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow>' \<langle>e', s'\<rangle> \<Longrightarrow> \<Gamma>, A \<turnstile> e : T \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle>" and
		big_step_unsafe_implies_safe_steps:
			"\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>]' \<langle>es', s'\<rangle> \<Longrightarrow> \<Gamma>, A \<turnstile> es [:] Ts \<Longrightarrow> \<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle>"
using assms
proof (induction arbitrary: A T and A Ts rule: BigStep_unsafe.big_step_big_steps.inducts)
  case Value then show ?case by simp
next
  case Local then show ?case by simp
next
  case (Seq c1 s s' c2 c2' s'')
  then have "\<Gamma> \<turnstile> \<langle>c1, s\<rangle> \<Rightarrow> \<langle>unit, s'\<rangle>" by auto
  moreover from Seq have "A \<rhd> c1 \<noteq> \<top>" using value_neq_exception reachability_preservation
    by (metis ExpressionValidity.SeqE prod.collapse)
  moreover with Seq have "\<Gamma> \<turnstile> \<langle>c2, s'\<rangle> \<Rightarrow> \<langle>c2', s''\<rangle>"
    using value_neq_exception valid_state_preservation' by blast
  ultimately show ?case by simp
next
  case Assign then show ?case by auto
next
  case Create then show ?case by simp
next
  case "Create\<^bsub>fail\<^esub>" then show ?case by simp
next
	case (Call e s v se es vs s' n)
	then obtain Ts where
		lHeT: "\<Gamma>, A \<turnstile> e : Attached" and
		lHesTs: "\<Gamma>, A \<rhd> e \<turnstile> es [:] Ts" by blast
  with Call.IH Call.prems have
  	lHeS: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Value v, se\<rangle>" by simp
  moreover with lHeT Call.prems have "\<Gamma>, A \<rhd> e \<turnstile> se \<surd>\<^sub>s"
		using value_neq_exception valid_state_preservation' by blast
	moreover from lHeS lHeT Call.prems have "A \<rhd> e \<noteq> \<top>" using value_neq_exception reachability_preservation
		by (metis prod.collapse)
	moreover note Call.IH lHesTs
	ultimately have "\<Gamma> \<turnstile> \<langle>es, se\<rangle> [\<Rightarrow>] \<langle>map Value vs, s'\<rangle>" by blast
  with lHeS Call.hyps show ?case by auto
next
  case ("Call\<^bsub>fail\<^esub>" e s v s' f es) (* \<le>== Interesting case *)
  from "Call\<^bsub>fail\<^esub>.prems" have "\<Gamma>, A \<turnstile> e: Attached" by auto
  with "Call\<^bsub>fail\<^esub>.prems" "Call\<^bsub>fail\<^esub>.IH" have "v \<noteq> Void\<^sub>v"
    using StateValidity.valid_stateD2 local_attachment_type_preservation
    by (metis prod.collapse)
  with "Call\<^bsub>fail\<^esub>.hyps" show ?case by simp
next
  case ("If\<^bsub>true\<^esub>" b s s' c1 c1' s'' c2)
  then have
    lHb: "\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, s'\<rangle>" by auto
  from "If\<^bsub>true\<^esub>.prems" have "\<Gamma>, A \<turnstile> b : Attached" by blast
  with "If\<^bsub>true\<^esub>" have "A \<rhd>+ b \<noteq> \<top>" using reachability_preservation_scope_true
    by (metis prod.collapse)
  with lHb "If\<^bsub>true\<^esub>.prems" "If\<^bsub>true\<^esub>.IH" have "\<Gamma> \<turnstile> \<langle>c1, s'\<rangle> \<Rightarrow> \<langle>c1', s''\<rangle>"
    using valid_state_preservation_scope_true' by blast
  with lHb show ?case by simp
next
  case ("If\<^bsub>false\<^esub>" b s s' c2 c2' s'' c1)
  then have
    lHb: "\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s'\<rangle>" by auto
  from "If\<^bsub>false\<^esub>.prems" have "\<Gamma>, A \<turnstile> b : Attached" by blast
  with "If\<^bsub>false\<^esub>" have "A \<rhd>- b \<noteq> \<top>" using reachability_preservation_scope_false
    by (metis prod.collapse)
  with lHb "If\<^bsub>false\<^esub>.prems" "If\<^bsub>false\<^esub>.IH" have "\<Gamma> \<turnstile> \<langle>c2, s'\<rangle> \<Rightarrow> \<langle>c2', s''\<rangle>"
    using valid_state_preservation_scope_false' by blast
  with lHb show ?case by simp
next
  case ("Loop\<^bsub>true\<^esub>" e s s' c)
  then have "\<Gamma>, TransferFunction.loop_computation e c A \<turnstile> e: Attached" by auto
  moreover have
    lHle: "TransferFunction.loop_computation e c A \<le> A" by (rule TransferFunction.loop_computation_le0)
  ultimately have "\<Gamma>, A \<turnstile> e: Attached" using ExpressionValidity.AT_mono conforms_to_attached by blast
  with "Loop\<^bsub>true\<^esub>.prems" "Loop\<^bsub>true\<^esub>.IH" have
    lHSe: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, s'\<rangle>" by simp
  then show ?case by simp
next
  case ("Loop\<^bsub>false\<^esub>" e s se c sc c' s')
  then have
    lHLe: "\<Gamma>, TransferFunction.loop_computation e c A \<turnstile> e: Attached" by auto
  moreover have
    lHle: "TransferFunction.loop_computation e c A \<le> A" by (rule TransferFunction.loop_computation_le0)
  ultimately have
    lHTe: "\<Gamma>, A \<turnstile> e: Attached" using ExpressionValidity.AT_mono conforms_to_attached by blast
  with "Loop\<^bsub>false\<^esub>.prems" "Loop\<^bsub>false\<^esub>.IH" have
    lHSe: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, se\<rangle>" by simp
  from lHTe "Loop\<^bsub>false\<^esub>" have
    lHAe: "A \<rhd>- e \<noteq> \<top>" using reachability_preservation_scope_false
      by (metis prod.collapse)
  from lHSe lHTe "Loop\<^bsub>false\<^esub>.prems" have
    lHVe: "\<Gamma>, A \<rhd>- e \<turnstile> se \<surd>\<^sub>s"
      by (simp add: valid_state_preservation_scope_false')
  from "Loop\<^bsub>false\<^esub>.prems" have
    lHLc: "\<Gamma>, TransferFunction.loop_computation e c A \<rhd>- e \<turnstile> c: Attached" by auto
  with lHle have
    lHTc: "\<Gamma>, A \<rhd>- e \<turnstile> c : Attached"
      using ExpressionValidity.AT_mono conforms_to_attached \<A>f_mono' by metis
  with lHAe lHVe "Loop\<^bsub>false\<^esub>.IH" have
    lHSc: "\<Gamma> \<turnstile> \<langle>c, se\<rangle> \<Rightarrow> \<langle>unit, sc\<rangle>" by simp
  with lHTc lHAe have
    lHAc: "A \<rhd>- e \<rhd> c \<noteq> \<top>" using reachability_preservation value_neq_exception
      by (metis prod.collapse)
  from lHSc lHTc lHVe "Loop\<^bsub>false\<^esub>.prems" have
    lHVc: "\<Gamma>, A \<rhd>- e \<rhd> c \<turnstile> sc \<surd>\<^sub>s" by (simp add: valid_state_preservation')
  with lHLe lHLc have "\<Gamma>, A \<rhd>- e \<rhd> c \<turnstile> until e loop c end : Attached"
  	by (simp add: AT_ATs.AT_Loop ExpressionValidity.AT_loop_step)
  with "Loop\<^bsub>false\<^esub>.IH" lHAc lHSc lHSe lHVc show ?case by simp
next
  case "Test\<^bsub>true\<^esub>" then show ?case by auto
next
  case "Test\<^bsub>false\<^esub>" then show ?case by auto
next
	case Nil then show ?case by simp
next
	case Cons then show ?case
		using value_neq_exception exception_detection valid_state_preservation'
			by (metis ATs_iffs(3) BigStep.big_step_big_steps.Cons prod.collapse)
next
  case Exception then show ?case by simp
next
  case SeqEx then show ?case by auto
next
  case AssignEx then show ?case by auto
next
  case CallEx then show ?case by auto
next
  case (CallArgEx e s v se es vs es' s' n)
  then have
  	lHeT: "\<Gamma>, A \<turnstile> e \<^bsub>\<bullet>\<^esub> n (es) : Attached" by auto
  then obtain Ts where "\<Gamma>, A \<rhd> e \<turnstile> es [:] Ts" by blast
  from CallArgEx.IH CallArgEx.prems have
  	lHse: "\<Gamma>, A \<rhd> e \<turnstile> se \<surd>\<^sub>s" using valid_state_preservation' by blast
	from CallArgEx have "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Value v, se\<rangle>" by blast
  with lHeT lHse CallArgEx.IH CallArgEx.prems show ?case
  	using value_neq_exception exception_detection
  		BigStep.big_step_big_steps.CallArgEx ExpressionValidity.CallE by (metis surj_pair)
next
  case IfEx then show ?case by auto
next
  case (LoopEx e s s' c) (* \<le>== Interesting case *)
  then have "\<Gamma>, TransferFunction.loop_computation e c A \<turnstile> e: Attached" by auto
  moreover have
    lHle: "TransferFunction.loop_computation e c A \<le> A" by (rule TransferFunction.loop_computation_le0)
  ultimately have "\<Gamma>, A \<turnstile> e: Attached" using ExpressionValidity.AT_mono by blast
  with LoopEx.IH LoopEx.prems show ?case by simp
next
  case ("Loop\<^bsub>false\<^esub>Ex" e s se c s')
  then have "\<Gamma>, TransferFunction.loop_computation e c A \<turnstile> e: Attached" by auto
  moreover have
    lHle: "TransferFunction.loop_computation e c A \<le> A" by (rule TransferFunction.loop_computation_le0)
  ultimately have
    lHTe: "\<Gamma>, A \<turnstile> e: Attached" using ExpressionValidity.AT_mono conforms_to_attached by blast
  with "Loop\<^bsub>false\<^esub>Ex.prems" "Loop\<^bsub>false\<^esub>Ex.IH" have
    lHSe: "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, se\<rangle>" by simp
  from lHTe "Loop\<^bsub>false\<^esub>Ex" have
    lHAe: "A \<rhd>- e \<noteq> \<top>" using reachability_preservation_scope_false
      by (metis prod.collapse)
  from lHSe lHTe "Loop\<^bsub>false\<^esub>Ex.prems" have
    lHVe: "\<Gamma>, A \<rhd>- e \<turnstile> se \<surd>\<^sub>s"
      by (simp add: valid_state_preservation_scope_false')
  from "Loop\<^bsub>false\<^esub>Ex.prems" have "\<Gamma>, TransferFunction.loop_computation e c A \<rhd>- e \<turnstile> c: Attached"
    by auto
  with lHle have "\<Gamma>, A \<rhd>- e \<turnstile> c : Attached"
    using ExpressionValidity.AT_mono conforms_to_attached \<A>f_mono' by metis
  with lHAe lHVe "Loop\<^bsub>false\<^esub>Ex.IH" have "\<Gamma> \<turnstile> \<langle>c, se\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" by simp
  with lHSe show ?case by simp
next
  case TestEx then show ?case by auto
next
	case ConsEx then show ?case by blast
qed

lemma big_step_unsafe_implies_safe:
	assumes
		"\<Gamma> \<turnstile> \<langle>p, s\<^sub>0\<rangle> \<Rightarrow>' \<langle>v, s\<rangle>"
		"\<Gamma> \<turnstile> p \<surd>\<^sub>e"
		"\<Gamma> \<turnstile> s\<^sub>0 \<surd>\<^sub>s"
	shows
	  "\<Gamma> \<turnstile> \<langle>p, s\<^sub>0\<rangle> \<Rightarrow> \<langle>v, s\<rangle>"
using assms big_step_unsafe_implies_safe_step using topset.discI
  by (metis attachment_valid_expression_def attachment_valid_state_def
  		attachment_type_valid_expression_def)

lemma big_step_unsafe_safe_eq:
	assumes
		"\<Gamma> \<turnstile> e \<surd>\<^sub>e"
		"\<Gamma> \<turnstile> s\<^sub>0 \<surd>\<^sub>s"
	shows
		"\<Gamma> \<turnstile> \<langle>e, s\<^sub>0\<rangle> \<Rightarrow>' \<langle>v, s\<rangle> \<longleftrightarrow> \<Gamma> \<turnstile> \<langle>e, s\<^sub>0\<rangle> \<Rightarrow> \<langle>v, s\<rangle>"
using big_step_unsafe_implies_safe big_step_safe_implies_unsafe using assms by blast

end