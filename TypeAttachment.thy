section "Types with attachment status"

theory TypeAttachment imports
	Type
begin

subsection "Type abstraction describing attachment status"

datatype attachment_type =
	"Attached" | -- "Expanded or attached reference type"
	"Detachable" -- "Detachable reference type"

subsubsection "Attachment types lattice"

instantiation attachment_type :: complete_lattice
begin
	definition "Inf X \<equiv> (if Attached \<in> X then Attached else Detachable)"
	definition "Sup X \<equiv> (if Detachable \<in> X then Detachable else Attached)"
	definition "inf A B \<equiv> (if A = Attached then Attached else B)"
	definition "sup A B \<equiv> (if A = Detachable then Detachable else B)"
	definition [simp]: "bot \<equiv> Attached"
	definition [simp]: "top \<equiv> Detachable"
	definition "less_eq A B \<equiv> A = B \<or> (A = Attached \<and> B = Detachable)"
	definition "less A B \<equiv> A = Attached \<and> B = Detachable"
instance proof
	fix x y :: attachment_type
	show "(x < y) = (x \<le> y \<and> \<not> y \<le> (x::attachment_type))"
		using less_attachment_type_def less_eq_attachment_type_def by auto
	show "x \<le> x"
		by (simp add: less_eq_attachment_type_def)
	show "inf x y \<le> x"
		by (metis attachment_type.exhaust inf_attachment_type_def less_eq_attachment_type_def)
	show "inf x y \<le> y"
		by (metis attachment_type.exhaust inf_attachment_type_def less_eq_attachment_type_def)
	show "x \<le> sup x y"
		by (metis attachment_type.exhaust sup_attachment_type_def less_eq_attachment_type_def)
	show "y \<le> sup x y"
		by (metis attachment_type.exhaust sup_attachment_type_def less_eq_attachment_type_def)
	show "Inf {} = (top :: attachment_type)"
		by (simp add: Inf_attachment_type_def)
	show "Sup {} = (bot :: attachment_type)"
		by (simp add: Sup_attachment_type_def)
next
	fix x y z :: attachment_type
	assume "x \<le> y" "y \<le> z"
	then show "x \<le> z" using less_eq_attachment_type_def by auto
next
	fix x y :: attachment_type
	assume "x \<le> y" "y \<le> x"
	then show "x = y" using less_eq_attachment_type_def by auto
next
	fix x y z :: attachment_type
	assume "x \<le> y" "x \<le> z"
	then show "x \<le> inf y z" using inf_attachment_type_def by auto
next
	fix x y z :: attachment_type
	assume "y \<le> x" "z \<le> x"
	then show "sup y z \<le> x" using sup_attachment_type_def by auto
next
	fix x :: attachment_type and A
	assume "x \<in> A"
	then show "Inf A \<le> x"
		by (metis Inf_attachment_type_def attachment_type.exhaust less_eq_attachment_type_def)
next
	fix x :: attachment_type and A
	assume "x \<in> A"
	then show "x \<le> Sup A"
		by (metis Sup_attachment_type_def attachment_type.exhaust less_eq_attachment_type_def)
next
	fix z :: attachment_type and A
	assume "\<And>x. x \<in> A \<Longrightarrow> z \<le> x"
	then show "z \<le> Inf A"
		using Inf_attachment_type_def attachment_type.exhaust less_eq_attachment_type_def by auto
next
	fix z :: attachment_type and A
	assume "\<And>x. x \<in> A \<Longrightarrow> x \<le> z"
	then show "Sup A \<le> z"
		using Sup_attachment_type_def attachment_type.exhaust less_eq_attachment_type_def by auto
qed
end

instantiation attachment_type :: distrib_lattice
begin
  instance proof
    fix x y z :: attachment_type
    show "sup x (inf y z) = inf (sup x y) (sup x z)" using sup_attachment_type_def by auto
  qed
end

abbreviation (input) attachment_type_less_eq_rep ("_ \<rightarrow>\<^sub>a _" [90, 90] 90) where "A \<rightarrow>\<^sub>a B \<equiv> A \<le> (B :: attachment_type)"

subsubsection "Conformance of attachment types"

lemma attachment_conforming_to_transitive [trans]: "\<lbrakk>A \<rightarrow>\<^sub>a B; B \<rightarrow>\<^sub>a C\<rbrakk> \<Longrightarrow> A \<rightarrow>\<^sub>a C"
	by simp

lemma attachment_conforming_to_reflexive [simp]: "A \<rightarrow>\<^sub>a A"
	by simp

lemma attachment_conforming_to_antisymmetric: "\<lbrakk>A \<rightarrow>\<^sub>a B; B \<rightarrow>\<^sub>a A\<rbrakk> \<Longrightarrow> A = B"
  by simp

lemma attachment_conforming_to_asymmetric: "\<not> A \<rightarrow>\<^sub>a B \<Longrightarrow> B \<rightarrow>\<^sub>a A"
	by (metis (full_types) attachment_type.exhaust less_eq_attachment_type_def)

lemma conforms_to_detachable [simp]: "T \<rightarrow>\<^sub>a Detachable"
	using attachment_conforming_to_asymmetric less_eq_attachment_type_def by auto

lemma conforms_to_attached [intro?]: "T \<rightarrow>\<^sub>a Attached \<Longrightarrow> T = Attached"
	by (simp add: less_eq_attachment_type_def)

lemma attached_conforms_to: "Attached \<rightarrow>\<^sub>a T"
	using attachment_type.exhaust less_eq_attachment_type_def by auto

subsubsection "Upper bound of attachment status"

abbreviation (input) "upper_bound A B \<equiv> sup A (B :: attachment_type)"

abbreviation (input) "lower_bound A B \<equiv> inf A (B :: attachment_type)"

lemma upper_bound_left: "A \<rightarrow>\<^sub>a upper_bound A B"
	by simp

lemma upper_bound_right: "B \<rightarrow>\<^sub>a upper_bound A B"
	by simp

lemma upper_bound_definition: "A \<rightarrow>\<^sub>a upper_bound A B \<and> B \<rightarrow>\<^sub>a upper_bound A B"
	by simp

lemma upper_bound_commute: "upper_bound A B = upper_bound B A"
	by (rule sup_commute)

lemma upper_bound_assoc: "upper_bound (upper_bound A B) C = upper_bound A (upper_bound B C)"
	by (rule sup_assoc)

lemma upper_bound_conf: "A \<rightarrow>\<^sub>a B \<Longrightarrow> upper_bound A C \<rightarrow>\<^sub>a upper_bound B C"
	using sup.mono by blast

lemma upper_bound_bot_left: "upper_bound A B \<rightarrow>\<^sub>a Attached \<Longrightarrow> A = Attached"
	by (simp add: conforms_to_attached)

lemma upper_bound_bot_right: "upper_bound A B \<rightarrow>\<^sub>a Attached \<Longrightarrow> B = Attached"
	by (simp add: conforms_to_attached)

lemma upper_bound_bot_left': "upper_bound A B = Attached \<Longrightarrow> A = Attached"
	by (metis bot_attachment_type_def sup_eq_bot_iff)

lemma upper_bound_bot_right': "upper_bound A B = Attached \<Longrightarrow> B = Attached"
	by (metis bot_attachment_type_def sup_eq_bot_iff)
  
lemma upper_bound_absorb_right [simp]: "upper_bound Attached A = A"
	by (simp add: sup_attachment_type_def)

lemma upper_bound_absorb_left [simp]: "upper_bound A Attached = A"
	by (metis bot_attachment_type_def sup_bot.right_neutral)

definition attachment_type_Union :: "attachment_type set \<Rightarrow> attachment_type" where
  "attachment_type_Union A = (if Attached \<in> A then Attached else Detachable)"
  
definition attachment_type_Inter :: "attachment_type set \<Rightarrow> attachment_type" where
  "attachment_type_Inter A = (if (Detachable \<in> A) then Detachable else Attached)"

subsubsection "Attached type properties"

definition "is_attached_type" :: "attachment_type \<Rightarrow> bool" where
	"is_attached_type t \<longleftrightarrow> t = Attached"

lemma attached_is_attached [simp]: "is_attached_type Attached"
  by (simp only: is_attached_type_def)

lemma is_attached_conforms_to [simp]: "is_attached_type A \<Longrightarrow> A \<rightarrow>\<^sub>a B"
  by (simp add: attached_conforms_to is_attached_type_def)

lemma is_attached_not_upper_bound [simp]:
    "is_attached_type A \<Longrightarrow> upper_bound A B = B" and
    "is_attached_type A \<Longrightarrow> upper_bound B A = B"
  by (auto simp add: attachment_conforming_to_antisymmetric)

lemma is_attached_conformance: "is_attached_type B \<longrightarrow> is_attached_type A \<Longrightarrow> A \<rightarrow>\<^sub>a B"
	using attachment_conforming_to_asymmetric is_attached_type_def less_eq_attachment_type_def by auto

lemma conformance_consistency: "A \<rightarrow>\<^sub>a B \<Longrightarrow> is_attached_type B \<longrightarrow> is_attached_type A"
	using less_eq_attachment_type_def by auto

subsection "Attachment status of types"

datatype attachment_mark = No_attachment_mark | Attached_mark | Detachable_mark

type_synonym attachable_type = "attachment_mark type"

type_synonym "type_attachment" = "attachable_type \<Rightarrow> attachment_type"

type_synonym "is_expanded_class" = "class_name \<Rightarrow> bool"

definition type_attachment_valid :: "is_expanded_class \<Rightarrow> type_attachment \<Rightarrow> bool"
	where "type_attachment_valid is_expanded f \<longleftrightarrow> (\<forall> t. f t =
		(case t of
		  UnitType \<Rightarrow> Attached |
			ClassType Detachable_mark c g \<Rightarrow> if is_expanded c then Attached else Detachable |
			_ \<Rightarrow> Attached))"

fun type_attachment :: "is_expanded_class \<Rightarrow> attachable_type \<Rightarrow> attachment_type" where
  "type_attachment _ UnitType = Attached" |
  "type_attachment _ (ClassType No_attachment_mark _ _) = Attached" |
  "type_attachment _ (ClassType Attached_mark _ _) = Attached" |
  "type_attachment is_expanded (ClassType Detachable_mark c _) =
    (if is_expanded c then Attached else Detachable)"

lemma "type_attachment_valid is_expanded (type_attachment is_expanded)"
proof -
  have "(\<forall> t. type_attachment is_expanded t =
		(case t of
		  UnitType \<Rightarrow> Attached |
			ClassType Detachable_mark c g \<Rightarrow> if is_expanded c then Attached else Detachable |
			_ \<Rightarrow> Attached))"
	proof (rule allI)
    fix t
    show "type_attachment is_expanded t =
      (case t of
        UnitType \<Rightarrow> Attached |
        ClassType Detachable_mark c g \<Rightarrow> if is_expanded c then Attached else Detachable |
        _ \<Rightarrow> Attached)"
    proof (cases t)
      case UnitType
      thus ?thesis by simp
    next
      case (ClassType mark name generics)
      thus ?thesis by (cases mark, simp_all)
    qed
  qed
  thus ?thesis by (simp add: type_attachment_valid_def)
qed

end