section "Expression"

theory Expression imports
	Name
	Value
begin

subsection "Expressions"

datatype ('b, 't) "expression" =
	"Value" "'b value" |
	"Local" vname |
	Sequence "('b, 't) expression" "('b, 't) expression" ("_ ;; _" [80, 81] 80) |
	Assignment vname "('b, 't) expression" ("_ ::= _" [1000, 81] 81) |
	Creation vname ("create _" [81] 81) |
	"Call" "('b, 't) expression" fname "('b, 't) expression list" ("_ \<^bsub>\<bullet>\<^esub> _ '(_')" [90, 99, 0] 90) |
	"If" "('b, 't) expression" "('b, 't) expression" "('b, 't) expression" ("if _ then _ else _ end" [80, 80, 80] 81) |
	"Loop" "('b, 't) expression" "('b, 't) expression" ("until _ loop _ end" [80, 81] 81) |
	"Test" "'t option" "('b, 't) expression" "vname" ("attached _ _ as _" [80, 81, 81] 81) |
	"Exception"

notation (input) Value ("\<C> _" 85)
notation (input) Local ("\<V> _" 85)

lemma value_neq_exception[simp, intro]: "(\<C> v) \<noteq> Exception"
  by simp

abbreviation "unit \<equiv> \<C> Unit"

lemma inj_Value [simp]: "inj Value"
	by (simp add: inj_on_def)

fun "is_expression" :: "('b, 't) expression \<Rightarrow> bool" where
	"is_expression (\<C> _) \<longleftrightarrow> True" |
	"is_expression (\<V> _) \<longleftrightarrow> True" |
	"is_expression (_ \<^bsub>\<bullet>\<^esub> _ (_)) \<longleftrightarrow> True" |
	"is_expression (if _ then _ else _ end) \<longleftrightarrow> True" |
	"is_expression (attached _ _ as _) \<longleftrightarrow> True" |
	"is_expression _ \<longleftrightarrow> False"

fun "is_instruction" :: "('b, 't) expression \<Rightarrow> bool" where
	"is_instruction (\<C> _) \<longleftrightarrow> False" |
	"is_instruction (\<V> _) \<longleftrightarrow> False" |
	"is_instruction (attached _ _ as _) \<longleftrightarrow> False" |
	"is_instruction Exception \<longleftrightarrow> False" |
	"is_instruction _ \<longleftrightarrow> True"

definition "folded transform combine initial list = fold combine (map transform list) initial"

primrec
  is_wf_computation :: "('b, 't) expression \<Rightarrow> bool"
where
	"is_wf_computation (\<C> _) \<longleftrightarrow> True" |
	"is_wf_computation (\<V> _) \<longleftrightarrow> True" |
	"is_wf_computation (c1 ;; c2) \<longleftrightarrow>
		is_instruction c1 \<and> is_instruction c2 \<and> is_wf_computation c1 \<and> is_wf_computation c2" |
	"is_wf_computation (n ::= c) \<longleftrightarrow> is_expression c \<and> is_wf_computation c" |
	"is_wf_computation (create n) \<longleftrightarrow> True" |
	"is_wf_computation (e \<^bsub>\<bullet>\<^esub> f (a)) \<longleftrightarrow> is_expression e \<and>
	  folded is_expression op\<and> True a \<and> fold op\<and> (map is_wf_computation a) True" |
	"is_wf_computation (if b then c1 else c2 end) \<longleftrightarrow>
	  is_expression b \<and> (is_instruction c1 \<and> is_instruction c2 \<or> is_expression c1 \<and> is_expression c2) \<and>
	  is_wf_computation b \<and> is_wf_computation c1 \<and> is_wf_computation c2" |
	"is_wf_computation (until e loop c end) \<longleftrightarrow> is_expression e \<and> is_instruction c \<and>
	  is_wf_computation e \<and> is_wf_computation c" |
	"is_wf_computation (attached _ e as _) \<longleftrightarrow> is_expression e \<and> is_wf_computation e" |
	"is_wf_computation Exception \<longleftrightarrow> False"

subsection {* Free variables *}

primrec fv :: "('b, 't) expression \<Rightarrow> vname set" where
	"fv (Value _) = {}" |
	"fv (Local n) = {n}" |
	"fv (c\<^sub>1 ;; c\<^sub>2) = fv c\<^sub>1 \<union> fv c\<^sub>2" |
	"fv (n ::= e) = {n} \<union> fv e" |
	"fv (create n) = {n}" |
	"fv (e \<^bsub>\<bullet>\<^esub> f (a)) = fv e \<union> fold op\<union> (map fv a) {}" |
	"fv (if b then c1 else c2 end) = fv b \<union> fv c1 \<union> fv c2" |
	"fv (until e loop c end) = fv e \<union> fv c" |
	"fv (attached _ e as n) = fv e \<union> {n}" |
	"fv Exception = {}"

subsection {* Final computations *}

inductive final :: "('b, 't) expression \<Rightarrow> bool" where
  "final (\<C> v)" |
  "final Exception"

declare final.cases [elim]
declare final.intros [simp]

lemmas finalE [consumes 1, case_names Value Exception] = final.cases

lemma final_iff: "final e = (\<exists> v. e = \<C> v) \<or> (e = Exception)"
	by auto

subsection "Boolean expressions"

abbreviation "False\<^sub>c \<equiv> \<C> (Object (Boolean False))"
abbreviation "True\<^sub>c \<equiv> \<C> (Object (Boolean True))"

fun is_false where
  is_false_true: "is_false (c ;; False\<^sub>c) \<longleftrightarrow> True" |
  is_false_false: "is_false _ \<longleftrightarrow> False"

fun is_true where
  is_true_true: "is_true (c ;; True\<^sub>c) \<longleftrightarrow> True" |
  is_true_false: "is_true _ \<longleftrightarrow> False"

(*<*)
lemma is_true_false':
    "\<lbrakk>case c of \<V> n \<Rightarrow> True | _ \<Rightarrow> False\<rbrakk> \<Longrightarrow>
      is_true c \<longleftrightarrow> False"
  by (cases c) simp_all
lemma is_false_false':
    "\<lbrakk>case c of \<V> n \<Rightarrow> True | _ \<Rightarrow> False\<rbrakk> \<Longrightarrow>
      is_false c \<longleftrightarrow> False"
  by (cases c) simp_all
(*>*)

end