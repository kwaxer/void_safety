section "Environment"

theory EnvironmentAttachment imports
	TypeAttachment Environment
begin

type_synonym attachment_environment = "attachment_mark environment"

end