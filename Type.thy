section "Types"

theory Type imports Common begin

subsection "Declaration type"

type_synonym "class_name" = string
type_synonym 'a "generic_list" = "'a list"

datatype 'a type =
  UnitType |
	ClassType 'a "class_name" "'a type generic_list"

type_synonym 'a generics = "'a type generic_list"

definition "BOOLEAN \<equiv> ''BOOLEAN''"
definition "boolean_class_type" :: "'a \<Rightarrow> 'a type" where
	"boolean_class_type x = ClassType x BOOLEAN []"

primrec is_boolean_type :: "'a type \<Rightarrow> bool" where
	"is_boolean_type UnitType \<longleftrightarrow> False" |
	"is_boolean_type (ClassType mark name generics) \<longleftrightarrow> name = BOOLEAN \<and> generics = []"

lemma "is_boolean_type (boolean_class_type mark)"
  by (simp add: boolean_class_type_def)

primrec is_expression_type :: "'a type \<Rightarrow> bool" where
  "is_expression_type UnitType \<longleftrightarrow> False" |
	"is_expression_type (ClassType mark name generics) \<longleftrightarrow> True"
	
subsection "Run-time type"

text {* TODO *}

end