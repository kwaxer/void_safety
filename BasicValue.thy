section "Interpretation of values with some basic types"

theory BasicValue imports
	Value "~~/src/Tools/Adhoc_Overloading"
begin

datatype basic_values =
	Integer int

definition integer_coercion :: "int \<Rightarrow> basic_values value" where
  "integer_coercion x = Object (Basic (Integer x))"

declare [[coercion "boolean_coercion :: bool \<Rightarrow> basic_values value"]]
declare [[coercion "integer_coercion"]]

definition "INTEGER \<equiv> ''INTEGER''"

primrec is_valid_basic_type :: "'mark type \<Rightarrow> bool" where
	"is_valid_basic_type UnitType \<longleftrightarrow> False" |
	"is_valid_basic_type (ClassType m n g) \<longleftrightarrow> n = INTEGER \<and> length g = 0"

primrec default_value :: "'mark type \<Rightarrow> basic_values option" where
  "default_value UnitType = None" |
	"default_value (ClassType mark n g) = (if n = INTEGER then Some (Integer 0) else None)"

primrec value_type :: "'mark \<Rightarrow> basic_values \<Rightarrow> 'mark type" where
  "value_type default_mark (Integer x) = ClassType default_mark INTEGER []"

lemma [simp]: "n \<noteq> BOOLEAN \<Longrightarrow> ClassType mark n [] \<noteq> boolean_class_type mark'"
	by (simp add: boolean_class_type_def)

lemma [simp]: "ClassType mark INTEGER [] \<noteq> boolean_class_type mark'"
	by (simp add: BOOLEAN_def INTEGER_def)

datatype empty = empty

interpretation basic_types: base_types
	"is_valid_basic_type"
	"default_value"
	"value_type empty"
	"empty"
proof (unfold_locales)
  fix t
  show "is_valid_basic_type t \<Longrightarrow>
         \<exists>v. default_value t = \<lfloor>v\<rfloor> \<and> value_type empty.empty v = t"
  proof (cases t)
    case UnitType
    thus "is_valid_basic_type t \<Longrightarrow>
         \<exists>v. default_value t = \<lfloor>v\<rfloor> \<and> value_type empty.empty v = t" by simp
  next
    case (ClassType mark name generics)
    then have "name = INTEGER" and "generics = []" if "is_valid_basic_type t" using that by simp_all
    thus "is_valid_basic_type t \<Longrightarrow>
         \<exists>v. default_value t = \<lfloor>v\<rfloor> \<and> value_type empty.empty v = t"
         by (metis (full_types) ClassType default_value.simps(2) empty.exhaust value_type.simps)
  qed
next
  fix v
  show "is_valid_basic_type (value_type empty.empty v)"
    by (cases v, simp)
next
  fix v
  show "value_type empty.empty v \<noteq> boolean_class_type empty.empty"
    by (cases v, simp)
qed

consts "\<T>" :: "'a \<Rightarrow> 'mark type"

definition declared_type_of_value :: "'mark \<Rightarrow> basic_values value \<Rightarrow> 'mark type" where
	"declared_type_of_value mark v = (case v of
				Object (Boolean v) \<Rightarrow> ClassType mark BOOLEAN [] |
				Object (Basic (Integer v)) \<Rightarrow> ClassType mark INTEGER [])"
declare declared_type_of_value_def [simp]

adhoc_overloading \<T> declared_type_of_value

type_synonym "value" = "basic_values value"

end