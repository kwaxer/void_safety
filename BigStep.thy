section "Void-safe Big-step semantics"

theory BigStep imports
	Environment
	Expression
	Memory
	State
begin

subsection "Big-step semantics rules"

abbreviation has_type_rep (infix "has'_type" 60) where
  "has_type_rep v T \<equiv> (T = None \<or> is_instance_of v T)"

inductive
  big_step :: "'a environment \<Rightarrow> ('b, 't) expression \<Rightarrow> ('b, 'm) state \<Rightarrow> ('b, 't) expression \<Rightarrow> ('b, 'm) state \<Rightarrow> bool" ("_ \<turnstile> \<langle>_, _\<rangle> \<Rightarrow> \<langle>_, _\<rangle>" [80, 0, 0] 81)
and
  big_steps :: "'a environment \<Rightarrow> ('b, 't) expression list \<Rightarrow> ('b, 'm) state \<Rightarrow> ('b, 't) expression list \<Rightarrow> ('b, 'm) state \<Rightarrow> bool" ("_ \<turnstile> \<langle>_, _\<rangle> [\<Rightarrow>] \<langle>_, _\<rangle>" [80, 0, 0] 81)
for
	"\<Gamma>" :: "'a environment"
where
	Value: "\<Gamma> \<turnstile> \<langle>\<C> v, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> v, (l, m)\<rangle>" |
	Local: "l n = \<lfloor>v\<rfloor> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>\<V> n, (l, m)\<rangle> \<Rightarrow> \<langle>\<C> v, (l, m)\<rangle>" |
	Seq: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e\<^sub>1, s\<rangle> \<Rightarrow> \<langle>unit, s'\<rangle>; \<Gamma> \<turnstile> \<langle>e\<^sub>2, s'\<rangle> \<Rightarrow> \<langle>e\<^sub>2', s''\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e\<^sub>1;; e\<^sub>2, s\<rangle> \<Rightarrow> \<langle>e\<^sub>2', s''\<rangle>" |
	Assign: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>\<C> v, (l, m)\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>n ::= e, s\<rangle> \<Rightarrow> \<langle>unit, (l (n \<mapsto> v), m)\<rangle>" |
	"Create": "\<lbrakk>\<Gamma> n = \<lfloor>T\<rfloor>; instance m T = \<lfloor>(m', v)\<rfloor>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>create n, (l, m)\<rangle> \<Rightarrow> \<langle>unit, (l (n \<mapsto> v), m')\<rangle>" |
	"Create\<^bsub>fail\<^esub>": "\<lbrakk>\<Gamma> n = \<lfloor>T\<rfloor>; instance m T = (None::('m \<times> 'b value) option)\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>create n, (l, m)\<rangle> \<Rightarrow> \<langle>Exception, (l, m)\<rangle>" |
	Call: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>\<C> v, s\<^sub>e\<rangle>; v \<noteq> Void\<^sub>v; \<Gamma> \<turnstile> \<langle>es, s\<^sub>e\<rangle> [\<Rightarrow>] \<langle>map Value vs, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e \<^bsub>\<bullet>\<^esub> f (es), s\<rangle> \<Rightarrow> \<langle>unit, s'\<rangle>" |
	"If\<^bsub>true\<^esub>": "\<lbrakk>\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, s'\<rangle>; \<Gamma> \<turnstile> \<langle>e\<^sub>1, s'\<rangle> \<Rightarrow> \<langle>e\<^sub>1', s''\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>if b then e\<^sub>1 else e\<^sub>2 end, s\<rangle> \<Rightarrow> \<langle>e\<^sub>1', s''\<rangle>" |
	"If\<^bsub>false\<^esub>": "\<lbrakk>\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s'\<rangle>; \<Gamma> \<turnstile> \<langle>e\<^sub>2, s'\<rangle> \<Rightarrow> \<langle>e\<^sub>2', s''\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>if b then e\<^sub>1 else e\<^sub>2 end, s\<rangle> \<Rightarrow> \<langle>e\<^sub>2', s''\<rangle>" |
	"Loop\<^bsub>true\<^esub>": "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>until e loop b end, s\<rangle> \<Rightarrow> \<langle>unit, s'\<rangle>" |
	"Loop\<^bsub>false\<^esub>": "\<lbrakk>
	    \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle>;
	    \<Gamma> \<turnstile> \<langle>b, s\<^sub>e\<rangle> \<Rightarrow> \<langle>unit, s\<^sub>c\<rangle>;
	    \<Gamma> \<turnstile> \<langle>until e loop b end, s\<^sub>c\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>
	  \<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>until e loop b end, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>" |
	"Test\<^bsub>true\<^esub>": "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>\<C> v, (l, m)\<rangle>; v \<noteq> Void\<^sub>v \<and> v has_type T\<rbrakk> \<Longrightarrow>
	  \<Gamma> \<turnstile> \<langle>attached T e as n, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, (l (n \<mapsto> v), m)\<rangle>" |
	"Test\<^bsub>false\<^esub>": "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>\<C> v, (l, m)\<rangle>; \<not> (v \<noteq> Void\<^sub>v \<and> v has_type T)\<rbrakk> \<Longrightarrow>
	  \<Gamma> \<turnstile> \<langle>attached T e as n, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, (l, m)\<rangle>" |
	Nil: "\<Gamma> \<turnstile> \<langle>[], s\<rangle> [\<Rightarrow>] \<langle>[], s\<rangle>" |
	Cons: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>\<C> v, s\<^sub>e\<rangle>; \<Gamma> \<turnstile> \<langle>es, s\<^sub>e\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e # es, s\<rangle> [\<Rightarrow>] \<langle>(\<C> v) # es', s'\<rangle>" |
-- "Exception propagation"
	Exception: "\<Gamma> \<turnstile> \<langle>Exception, s\<rangle> \<Rightarrow> \<langle>Exception, s\<rangle>" |
	SeqEx: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e\<^sub>1, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e\<^sub>1;; e\<^sub>2, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	AssignEx: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>n ::= e, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	CallEx: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e \<^bsub>\<bullet>\<^esub> f (es), s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	CallArgEx: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>\<C> v, s\<^sub>e\<rangle>; \<Gamma> \<turnstile> \<langle>es, s\<^sub>e\<rangle> [\<Rightarrow>] \<langle>map Value vs @ Exception # es', s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e \<^bsub>\<bullet>\<^esub> f (es), s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	"IfEx": "\<lbrakk>\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>if b then e\<^sub>1 else e\<^sub>2 end, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	"LoopEx": "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>until e loop b end, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	"Loop\<^bsub>false\<^esub>Ex": "\<lbrakk>
	    \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle>;
	    \<Gamma> \<turnstile> \<langle>b, s\<^sub>e\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>
	  \<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>until e loop b end, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	TestEx: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>attached t e as n, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>" |
	ConsEx: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Exception, s'\<rangle>\<rbrakk> \<Longrightarrow> \<Gamma> \<turnstile> \<langle>e # es, s\<rangle> [\<Rightarrow>] \<langle>Exception # es, s'\<rangle>"

lemmas big_step_induct = "big_step_big_steps.inducts"[split_format(complete)]
declare "big_step_big_steps.intros"[simp,intro]
inductive_cases ValueE[elim]: "\<Gamma> \<turnstile> \<langle>\<C> v, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases LocalE[elim!]: "\<Gamma> \<turnstile> \<langle>\<V> n, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases SeqE[elim!]: "\<Gamma> \<turnstile> \<langle>c\<^sub>1;; c\<^sub>2, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases AssignE[elim!]: "\<Gamma> \<turnstile> \<langle>v::=x, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases CreateE[elim!]: "\<Gamma> \<turnstile> \<langle>create v, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases CallE[elim!]: "\<Gamma> \<turnstile> \<langle>v \<^bsub>\<bullet>\<^esub> f (a), s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases IfE[elim!]: "\<Gamma> \<turnstile> \<langle>if b then c1 else c2 end, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases LoopE[elim!]: "\<Gamma> \<turnstile> \<langle>until e loop c end, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases TestE[elim!]: "\<Gamma> \<turnstile> \<langle>attached t e as x, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
inductive_cases ExceptionE[elim!]: "\<Gamma> \<turnstile> \<langle>Exception, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>"
(*inductive_cases NilE[elim!]: "\<Gamma> \<turnstile> \<langle>[], s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle>"*)
inductive_cases ConsE[elim]: "\<Gamma> \<turnstile> \<langle>e # es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle>"

subsection "Final state"

definition "Final" :: "'a environment \<Rightarrow> ('b, 't) expression \<Rightarrow> ('l, 'm) state \<Rightarrow> bool" where
	"Final \<Gamma> c s \<longleftrightarrow> (\<exists> v. c = \<C> v) \<or> c = Exception"

definition
	"Finals es \<longleftrightarrow> (\<exists> vs. es = map Value vs) \<or> (\<exists> vs es' . es = map Value vs @ (Exception # es'))"

inductive final :: "'a environment \<Rightarrow> ('b, 't) expression \<Rightarrow> ('l, 'm) state \<Rightarrow> bool" where
  final_Value: "final \<Gamma> (\<C> v) s" |
  final_Exception: "final \<Gamma> Exception s"

inductive finals :: "('b, 't) expression list \<Rightarrow> bool" where
  finals_Value: "finals (map Value vs)" |
  finals_Exception: "finals (map Value vs @ (Exception # es'))"

lemma "Final \<Gamma> c s \<longleftrightarrow> final \<Gamma> c s"
  by (cases c, simp_all add: final.simps Final_def)

lemma "Finals es \<longleftrightarrow> finals es"
	by (induction es) (simp_all add: Finals_def finals.simps)

lemma
	fixes
		e e' :: "('b, 't) expression" and es es' :: "('b, 't) expression list"
	shows
		big_step_final:	"\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e', s'\<rangle> \<Longrightarrow> Final \<Gamma> e' s'" and
		big_step_finals: "\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es', s'\<rangle> \<Longrightarrow> Finals es'"
proof (induction rule: "big_step_big_steps.inducts")
	case Cons then show ?case using Finals_def append_Cons by (metis (no_types) list.simps(9))
next
	case ConsEx then show ?case using Finals_def map_append by blast
qed (simp_all add: Final_def Finals_def)

lemma no_progress:
	assumes "Final \<Gamma> c s"
	shows "\<Gamma> \<turnstile> \<langle>c, s\<rangle> \<Rightarrow> \<langle>c, s\<rangle>"
using assms
  by (metis Value Exception Final_def prod.collapse)

lemma big_step_final_value:
	assumes
	  HS: "\<Gamma> \<turnstile> \<langle>c, s\<rangle> \<Rightarrow> \<langle>c', s'\<rangle>" and
	  HE: "c' \<noteq> Exception"
	obtains v where "c' = \<C> v"
proof -
  from HS have "Final \<Gamma> c' s'" by (rule big_step_final)
  with HE have "\<exists> v. c' = \<C> v" by (simp add: Final_def)
  then show ?thesis using that by auto
qed
(*
lemma
	fixes
		e :: "('b, 't) expression" and es :: "('b, 't) expression list"
	shows
		big_step_deterministic: "\<lbrakk>\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e\<^sub>1, s\<^sub>1\<rangle>; \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle>\<rbrakk> \<Longrightarrow> e\<^sub>1 = e\<^sub>2 \<and> s\<^sub>1 = s\<^sub>2"
	and
		big_steps_deterministic: "\<lbrakk>\<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es\<^sub>1, s\<^sub>1\<rangle>; \<Gamma> \<turnstile> \<langle>es, s\<rangle> [\<Rightarrow>] \<langle>es\<^sub>2, s\<^sub>2\<rangle>\<rbrakk> \<Longrightarrow> es\<^sub>1 = es\<^sub>2 \<and> s\<^sub>1 = s\<^sub>2"
proof (induction arbitrary: e\<^sub>2 s\<^sub>2 and es\<^sub>2 s\<^sub>2 rule: "big_step_big_steps.inducts")
  case Value then show ?case by blast
next
  case Local then show ?case by auto
next
  case Seq then show ?case by (metis SeqE value_neq_exception)
next
  case Assign then show ?case by blast
next
  case Create then show ?case by auto
next
  case "Create\<^bsub>fail\<^esub>" then show ?case by (simp add: instance_def)
next
  case (Call e s v s\<^sub>e es es' s' n)
  then show ?case sorry
next
  case ("If\<^bsub>true\<^esub>" b s sb c1 c1' s1 c2)
  then obtain sb' where
    "\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, sb'\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c1, sb'\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle> \<or>
     \<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, sb'\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c2, sb'\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle>" by blast
  moreover have "\<not> \<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, sb'\<rangle>" using "If\<^bsub>true\<^esub>.IH" by blast
  ultimately have "\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, sb'\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c1, sb'\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle>" by simp
  with "If\<^bsub>true\<^esub>.IH" show ?case by blast
next
  case ("If\<^bsub>false\<^esub>" b s sb c2 c2' s2 c1)
  then obtain sb' where
    "\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, sb'\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c1, sb'\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle> \<or>
     \<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, sb'\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c2, sb'\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle>" by blast
  moreover have "\<not> \<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, sb'\<rangle>" using "If\<^bsub>false\<^esub>.IH" by blast
  ultimately have "\<Gamma> \<turnstile> \<langle>b, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, sb'\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c2, sb'\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle>" by simp
  with "If\<^bsub>false\<^esub>.IH" show ?case by blast
next
  case ("Loop\<^bsub>true\<^esub>" e s se) with LoopE value_neq_exception show ?case
    by (metis (full_types) expression.inject(1) object_value.inject(2) value.inject(1))
next
  case ("Loop\<^bsub>false\<^esub>"  e s se c sc cl sl)
  with LoopE obtain s\<^sub>e s\<^sub>c where
    hE: "e\<^sub>2 = unit \<and> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, s\<^sub>2\<rangle> \<or>
    \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c, s\<^sub>e\<rangle> \<Rightarrow> \<langle>unit, s\<^sub>c\<rangle> \<and> \<Gamma> \<turnstile> \<langle>until e loop c end, s\<^sub>c\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle> \<or>
    e\<^sub>2 = Exception \<and> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Exception, s\<^sub>2\<rangle> \<or>
    e\<^sub>2 = Exception \<and> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c, s\<^sub>e\<rangle> \<Rightarrow> \<langle>Exception, s\<^sub>2\<rangle>" by metis 
  with "Loop\<^bsub>false\<^esub>.IH" value_neq_exception show ?case
  proof -
    have f1: "\<forall>c p. \<not> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>c, p\<rangle> \<or> False\<^sub>c = c \<and> se = p"
      by (metis "Loop\<^bsub>false\<^esub>.IH"(2))
    then have f2: "\<not> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, s\<^sub>2\<rangle>"
      by blast
    have "e\<^sub>2 \<noteq> Exception \<or> \<not> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle> \<or> \<not> \<Gamma> \<turnstile> \<langle>c, s\<^sub>e\<rangle> \<Rightarrow> \<langle>Exception, s\<^sub>2\<rangle>"
      using f1 by (metis (no_types) "Loop\<^bsub>false\<^esub>.IH"(4) value_neq_exception)
    then have "\<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c, s\<^sub>e\<rangle> \<Rightarrow> \<langle>unit, s\<^sub>c\<rangle> \<and> \<Gamma> \<turnstile> \<langle>until e loop c end, s\<^sub>c\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle>"
      using f2 f1 by (metis (no_types) value_neq_exception hE)
    then have "\<Gamma> \<turnstile> \<langle>until e loop c end, sc\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle>"
      using f1 by (metis "Loop\<^bsub>false\<^esub>.IH"(4))
    then show ?thesis
      by (meson "Loop\<^bsub>false\<^esub>.IH"(6))
  qed
next
  case "Test\<^bsub>true\<^esub>" then show ?case by blast
next
  case "Test\<^bsub>false\<^esub>" then show ?case by blast
next
	case (Nil s s') then show ?case using big_steps.cases by blast
next
	case Cons then show ?case using value_neq_exception by blast
next
  case Exception then show ?case by blast
next
  case SeqEx then show ?case by blast 
next
  case AssignEx then show ?case by blast
next
  case CallEx then show ?case by blast 
next
	case CallArgEx then show ?case sorry
next
  case IfEx then show ?case by blast
next
  case LoopEx then show ?case by (metis LoopE value_neq_exception)
next
  case ("Loop\<^bsub>false\<^esub>Ex" e s se c sc)
  with LoopE obtain s\<^sub>e s\<^sub>c where
    hE: "e\<^sub>2 = unit \<and> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>True\<^sub>c, s\<^sub>2\<rangle> \<or>
    \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c, s\<^sub>e\<rangle> \<Rightarrow> \<langle>unit, s\<^sub>c\<rangle> \<and> \<Gamma> \<turnstile> \<langle>until e loop c end, s\<^sub>c\<rangle> \<Rightarrow> \<langle>e\<^sub>2, s\<^sub>2\<rangle> \<or>
    e\<^sub>2 = Exception \<and> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>Exception, s\<^sub>2\<rangle> \<or>
    e\<^sub>2 = Exception \<and> \<Gamma> \<turnstile> \<langle>e, s\<rangle> \<Rightarrow> \<langle>False\<^sub>c, s\<^sub>e\<rangle> \<and> \<Gamma> \<turnstile> \<langle>c, s\<^sub>e\<rangle> \<Rightarrow> \<langle>Exception, s\<^sub>2\<rangle>" by metis 
  with "Loop\<^bsub>false\<^esub>Ex" have ?case if "e\<^sub>2 = Exception" using that value_neq_exception
    by (metis)
  moreover from hE "Loop\<^bsub>false\<^esub>Ex" have ?case if "e\<^sub>2 = unit"
    using that value_neq_exception
      by (metis expression.inject(1) object_value.inject(2) value.inject(1))
  ultimately show ?case using hE "Loop\<^bsub>false\<^esub>Ex.IH"
    by (metis value_neq_exception) 
next
  case TestEx then show ?case by blast
next
	case ConsEx then show ?case using value_neq_exception by blast
qed
*)
end
